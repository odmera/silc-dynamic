import { FormularioOpcion } from './FormularioOpcion';
import { PreguntaResuelta } from './PreguntaResuelta';

export class FormularioPregunta {
	id?: string; //este id sera un guid generado por nosotros.
	idbloque:string;
	nombrePregunta:string;
	subtitulo?:string;
	orden:number;
	tipoPreguntaEnumId:string;
	mostrarnombre:boolean; //por defecto en true
	obligatoria: boolean; //por defecto en false y no se pone en la forma
	listaOpciones?:FormularioOpcion[] 
	habilitado: boolean;//por defecto en true
	PreguntaResuelta?: PreguntaResuelta; //solo se utiliza para enviarlo al pdf
}