export class Sincronizacion {
     id?: string;
    _rev: string;
    _id: string;
    idclub: string;
    nombreclub:string;
    nombreResponsable:string;
    firma:string;
    fechasincronizacion?:number;
    observacionsincronizacion?:string;
    correoelectronicoenvio?:string;
    idusuario:string;
    usuarioMail:string;
    fotosCapturadas?:object[] = [];
    urlImagenCampoJuego?:string;
    urlPdf?:string;
}
