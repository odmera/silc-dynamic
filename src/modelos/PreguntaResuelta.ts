import { FormularioOpcion } from './FormularioOpcion';
import { Foto } from './Foto';
export interface PreguntaResuelta {
	id?: string;
    _rev?: string; // rev PouchDb
    _id?: string; // id PouchDb
	idclub:string;
	nombreclub: string; 
	idformulario:string,
	nombreformulario:string;
	idbloque:string;
	idpregunta:string;
	nombrepregunta:string;
	listaOpcionesResueltas:FormularioOpcion[]; //por ahora solo va una opcion,pero se deja la lista por si a futuro nos piden respuestas multiple  
	respuestatexto:string;
	respuestanumerica:number;
	respuestalogica:boolean;
	respuestafecha:string;
	listImages: Array<Foto>; //solo para preguntas tipo fecha
}