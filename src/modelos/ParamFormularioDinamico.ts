import { Formulario } from "./Formulario";
import { Club } from './Club';
import { PreguntaResuelta } from './PreguntaResuelta';

export interface ParamFormularioDinamico {	
	formularios:Formulario; //lista de formularios a pintar 
	formulariolleno:boolean;
	club:Club;
	listPreguntasResueltas?:PreguntaResuelta[]
}