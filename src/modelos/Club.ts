import { TipoClubEnum } from "./enum/TipoClubEnum";
import { ImageProperties } from "./interface/imageinterface/ImageProperties";

export class Club {
    id?: string;
    nombreCompleto: string;
    tipoClub: TipoClubEnum; //01 Conmebol,02 Zona metropolitana(Nacional B),03 Zona interior(Nacional B)
    direccionSedeAdmin: string;
    direccionEstadio: string;
    direccionCampoDeportes:string;
    nombrePresidente:string;
    responsableLicencias:string;
    capacidadestadio:string; //se deja como string por un problema que se tuvo
    foto?: string; //url de la foto
    nombreFoto?: string;
    fotoFondo?: string; //url de la foto
    nombreFotoFondo?: string;
    habilitado: boolean;
    fecha:number;
    _id?:string; //identificador para pouchdb
    _rev?:string;//identificador para pouchdb
    // _attachments?: ImageProperties;
    _attachments?: {
        'att.txt':{
            content_type :string
            data: string
        },
        'attFondo.txt':{
            content_type :string
            data: string
        }        
    }

}



export enum TipoClub {
    CONMEBOL = <any>'01',
    ZONA_METROPOLITANA_B = <any>'02',
    ZONA_INTERIOR_B = <any>'03',
}