export class ModeloFirebase {
    public static readonly SINCRONIZACION = 'sincronizacion';
    public static readonly CLUB = "club";
    public static readonly USUARIOS = "usuario";
    public static readonly DIVISION = "division";
    public static readonly FORMULARIO = "formulario";
    public static readonly FORMULARIOBLOQUE = "formulariobloque";
    public static readonly FORMULARIORESPUESTA = "formulariorespuesta";
    public static readonly CAMPODEJUEGOMEDIDA = 'campodejuegomedida'; 
}