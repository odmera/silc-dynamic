import { QuestionBaseAPS } from "./QuestionBaseAPS";

export class ToggleQuestionAPS extends QuestionBaseAPS<string> {
  
  controlType = 'toggle';

  constructor(options: {} = {}) {
    super(options);
  }
}