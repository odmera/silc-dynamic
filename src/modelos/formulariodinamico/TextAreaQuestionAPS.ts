import { QuestionBaseAPS } from "./QuestionBaseAPS";

export class TextAreaQuestionAPS extends QuestionBaseAPS<string> {

  controlType = 'textarea';
  type: string;
  
  constructor(options: {} = {}) {
    super(options);
    this.type = options['type'] || '';
  }
}