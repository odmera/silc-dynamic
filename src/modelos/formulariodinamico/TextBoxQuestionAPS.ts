import { QuestionBaseAPS } from "./QuestionBaseAPS";

export class TextBoxQuestionAPS extends QuestionBaseAPS<string> {

  controlType = 'textbox';
  
  constructor(options: {} = {}) {
    super(options);
  }
}