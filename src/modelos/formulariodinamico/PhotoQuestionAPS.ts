import { QuestionBaseAPS } from "./QuestionBaseAPS";
import { Foto } from '../Foto';

export class PhotoQuestionAPS extends QuestionBaseAPS<string> {

  controlType = 'photo';
  listImages: Array<Foto> = [];

  constructor(options: {} = {}) {
    super(options);
    this.listImages = options['listImages'] || [];
  }
}