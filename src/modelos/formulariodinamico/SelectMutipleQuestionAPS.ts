import { QuestionBaseAPS } from "./QuestionBaseAPS";

export class SelectMutipleQuestionAPS extends QuestionBaseAPS<string> {

  controlType = 'selectmutiple';
  options: {key: string, value: string,selected:boolean}[] = [];
  //multiple:boolean=false;

  constructor(options: {} = {}) {
    super(options);
    this.options = options['options'] || [];
    //this.multiple = options['multiple'] || false;
  }
}