import { FormularioPregunta } from '../FormularioPregunta';
export class QuestionBaseAPS<T> {

  value: T;
  key: string;
  label: string;
  required: boolean;
  order: number;
  controlType: string;
  pregunta:FormularioPregunta
  showquestion :boolean;
  showlabel :boolean;
  subtitle:string
  
  constructor(
      options: {
      value?: T,
      key?: string,
      label?: string,
      required?: boolean,
      order?: number,
      controlType?: string,
      pregunta?: FormularioPregunta,
      showquestion?:boolean,
      showlabel? :boolean,
      subtitle?:string
    } = {}) {
    this.value = options.value;
    this.key = options.key || '';
    this.label = options.label || '';
    this.required = !!options.required;
    this.order = options.order === undefined ? 999999 : options.order;
    this.controlType = options.controlType || '';
    this.pregunta = options.pregunta || null;
    this.showquestion = options.showquestion == null ? true : options.showquestion;
    this.showlabel = options.showlabel == null ? false : options.showlabel;
    this.subtitle = options.subtitle || null;
  }
}