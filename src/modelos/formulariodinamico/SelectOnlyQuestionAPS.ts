import { QuestionBaseAPS } from "./QuestionBaseAPS";

export class SelectOnlyQuestionAPS extends QuestionBaseAPS<string> {

  controlType = 'selectonly';
  options: {key: string, value: string,selected:boolean}[] = [];

  constructor(options: {} = {}) {
    super(options);
    this.options = options['options'] || [];
  }
}