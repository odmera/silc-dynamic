import { QuestionBaseAPS } from "./QuestionBaseAPS";

export class RadioQuestionAPS extends QuestionBaseAPS<string> {

  controlType = 'radio';
  options: {key: string, value: string}[] = [];

  constructor(options: {} = {}) {
    super(options);
    this.options = options['options'] || [];
  }
}