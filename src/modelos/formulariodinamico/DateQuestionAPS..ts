import { QuestionBaseAPS } from "./QuestionBaseAPS";

export class DateQuestionAPS extends QuestionBaseAPS<string> {
  
  controlType = 'date';

  constructor(options: {} = {}) {
    super(options);
  }
}