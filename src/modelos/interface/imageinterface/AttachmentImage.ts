import { ImageProperties } from './ImageProperties';

export class AttachmentImage {
    name: number;
    imageProperties: ImageProperties;
}
