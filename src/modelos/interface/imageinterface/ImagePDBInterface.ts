import { AttachmentImage } from './AttachmentImage';

export class ImagePDBInterface {
    _id?: string;
    _attachments: AttachmentImage;
}
