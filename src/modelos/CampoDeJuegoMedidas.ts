export class CampoDeJuegoMedidas {
    id?: string;
    _rev?: string; // rev PouchDb
    _id?: string; // id PouchDb
    idclub:string;
    nombreclub: string; 
    modelnamesqlite: string;
    usuarioMail: string;
    fecha:number;  
    imagencampojuego:string;
    
    /////fuera de la cancha
    fueracancha1:number //5,50
    fueracancha2:number //5,50
    fueracancha3:number //5,50  
    fueracancha4:number //5,37
    fueracancha5:number //5,37
    fueracancha6:number //5,37
    fueracancha7:number //5,37
    fueracancha8:number //5,37
    fueracancha9:number //4,575
    fueracancha10:number //4,575
    fueracancha11:number //4,575
    fueracancha12:number //4,575
    fueracancha13:number //5,37
    fueracancha14:number //5,37
    fueracancha15:number //5,37
    fueracancha16:number //5,37
    fueracancha17:number //5,37
    fueracancha18:number //5,50
    fueracancha19:number //5,50
    fueracancha20:number //5,50
    fueracancha21:number //105.mts

    /////izquierda de la cancha
    izquierdacancha0:number //1
    izquierdacancha1:number
    izquierdacancha2:number
    izquierdacancha3:number
    izquierdacancha4:number
    izquierdacancha5:number
    izquierdacancha6:number
    izquierdacancha7:number
    izquierdacancha8:number
    izquierdacancha9:number
    izquierdacancha10:number
    izquierdacancha11:number
    izquierdacancha12:number
    izquierdacancha13:number
    izquierdacancha14:number
    izquierdacancha15:number
    izquierdacancha16:number
    izquierdacancha17:number
    izquierdacancha18:number
    izquierdacancha19:number

    /////centro  de la cancha
    centrocancha1:number
    centrocancha2:number
    centrocancha3:number

    //derecha de la cancha
    derechacancha1:number
    derechacancha2:number
    derechacancha3:number
    derechacancha4:number
    derechacancha5:number
    derechacancha6:number
    derechacancha7:number
    derechacancha8:number
    derechacancha9:number
    derechacancha10:number
    derechacancha11:number
    derechacancha12:number
    derechacancha13:number
    derechacancha14:number
    derechacancha15:number
    derechacancha16:number
    derechacancha17:number
    derechacancha18:number
    derechatipoesquinaarriba20:number
    derechatipoesquinaabajo20:number

    // ......................bancos suplente..............................
    bancosuplenteizquierda1:number
    bancosuplenteizquierda2:number
    bancosuplentederecha1:number
    bancosuplentederecha2:number
    bancosuplente105:number
    bancosuplente012:number


    // ......................arcos afuera del campo..............................
    arcoizquierda1:number
    arcoizquierda2:number
    arcoizquierda3:number
    arcoizquierda4:number
    arcoderecha1:number
    arcoderecha2:number
    arcoderecha3:number
    arcoderecha4:number


}