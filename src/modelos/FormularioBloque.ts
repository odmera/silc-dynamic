import { FormularioPregunta } from './FormularioPregunta';
import { QuestionBaseAPS } from './formulariodinamico/QuestionBaseAPS';
export class FormularioBloque {
	id?: string; //este id sera un guid generado por nosotros.
	idformulario:string;
	nombreBloque:string;
	orden:number;
	numerocolumnas:number;
	mostrarnombre:boolean;//por defecto en true
	listaPreguntas:FormularioPregunta[];
	preguntasdinamicas?:QuestionBaseAPS<any>[]  
	habilitado: boolean;//por defecto en true
}