import { FormularioBloque } from "./FormularioBloque";

export class Formulario {
	id?: string;
	nombreFormulario:string;
	descripcionFormulario?:string;
	agrupaFormulario: boolean; //determina si el formulario contiene otros formularios, cuando se checkea esta opción deben apareces el listado de formularios disponibles para asi poder hacer la asociación
	formulariosAgrupacion:Formulario[];//(lista de formularios que contiene un formulario, esto se llena cuando el campo agrupaFormulario es true)
	listaBloques:FormularioBloque[];
	habilitado: boolean; //por defecto en true
	agrupadoOtroFormulario?: boolean; //por defecto en true
	checked?: boolean; //determina si el formulario esta incluido en una division
}