import { CampoDeJuegoMedidas } from "./CampoDeJuegoMedidas";
import { Sincronizacion } from "./Sincronizacion";
import { Formulario } from "./Formulario";

export interface SincronizacionApi {
    campoDeJuegoMedidas:CampoDeJuegoMedidas;
    listFormularios:Formulario[];
    sincronizacion:Sincronizacion;
}
