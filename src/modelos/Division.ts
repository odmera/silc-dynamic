import { Formulario } from "./Formulario";

export class Division {
	id?: string;
    nombreDivision:string;
	habilitado: boolean;
	mostrarCancha: boolean;
	listaFormularios?:Formulario[];
}
