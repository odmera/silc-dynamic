export interface UsuarioDTO {
     id?: string;
     nombreCompleto: string; //aqui va la razon social para los clientes.
     correo: string;
     contrasena: string;
     telefono: string;
     perfil: string; 
     foto?: string;
     nombreFoto?: string;
     habilitado: boolean;
}


export enum PerfilUsuario {
    _pefil_admin = <any>'01',
    _pefil_usuario = <any>'02',
}