export enum ModelNamesSqlLiteEnum{
    SINCRONIZACION = <any>'sincronizacion.db',
    CLUB = <any>'club.db',
    DIVISION = <any>'division.db',
    FORMULARIO = <any>'formulario.db',
    FORMULARIORESPUESTA = <any>'formulariorespuesta.db',
    CAMPODEJUEGOMEDIDA = <any>'campodejuegomedida.db',  
}