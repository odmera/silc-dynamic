export const TipoPreguntaEnum = {
  OPCIONUNICARESPUESTA: {
    id: "1",
    name: "Pregunta con única respuesta"
  },
//   OPCIONMULTIPLERESPUESTA: {
//     id: "2",
//     name: "Pregunta con multiples respuestas"
//   },
  TEXTO: {
    id: "3",
    name: "Pregunta de tipo texto"
  },
  NUMERICA: {
    id: "4",
    name: "Pregunta de tipo numérica"
  },
  LOGICA: {
    id: "5",
    name: "Pregunta de tipo lógica"
  },
  FECHA: {
    id: "6",
    name: "Pregunta de tipo fecha"
  },
  FOTO: {
    id: "7",
    name: "Pregunta de tipo foto"
  }
};
