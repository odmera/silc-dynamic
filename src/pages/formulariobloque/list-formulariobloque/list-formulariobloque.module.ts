import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListFormularioBloquePage } from './list-formulariobloque';

@NgModule({
  declarations: [
    ListFormularioBloquePage,
  ],
  imports: [
    IonicPageModule.forChild(ListFormularioBloquePage),
  ],
})
export class ListFormularioBloquePageModule {}
