import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';
import { Formulario } from '../../../modelos/Formulario';
import { FormularioBloque } from '../../../modelos/FormularioBloque';
import { UtilidadesProvider } from '../../../providers/utilidades/utilidades';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { GenericFirestoreProvider } from '../../../providers/genericfirestore/genericfirestore';
import { ModeloFirebase } from '../../../modelos/ModeloFirebase';


@IonicPage()
@Component({
  selector: 'page-list-formulariobloque',
  templateUrl: 'list-formulariobloque.html',
})
export class ListFormularioBloquePage {

  listaFormulariosBloque: FormularioBloque[];
  private paramFormulario:Formulario;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController,
    public _util: UtilidadesProvider,
    private alertCtrl: AlertController,
    public _genericFirestoreProvider :GenericFirestoreProvider) {

      this.inicializarValores();
  }

  inicializarValores(){
    this.paramFormulario = this.navParams.get("paramformulario");
    this.listaFormulariosBloque = [];
  }

  ionViewDidLoad() {
    this.consultarFormularioBloque();
  }

  async consultarFormularioBloque() {

    let loader = this._util.presentarLoading("Cargando...");
    await loader.present();

    this.listaFormulariosBloque = [];

    if (this.paramFormulario.listaBloques != null && this.paramFormulario.listaBloques.length > 0) {
      this.listaFormulariosBloque = this.paramFormulario.listaBloques;
    } else {
      console.warn("Sin datos");
    }

    // Se ordena por campo orden
    this.listaFormulariosBloque.sort(function(a,b) {return (a.orden > b.orden) ? 1 : ((b.orden > a.orden) ? -1 : 0);} );
    loader.dismiss();
  }

  mostrarDialogo(tipoOperacion: number, item: FormularioBloque) {
  
    let miModal = this.modalCtrl.create('ModalFormularioBloquePage',
    {
        paramtipooperacion: tipoOperacion,
        paramformulariobloque: item,
        paramformulario: this.paramFormulario,
    });
    miModal.present();

    // Esta funcion se ejecuta cuando cierro el modal
    miModal.onDidDismiss(parametros => {
      if (parametros != null) {
        let loader = this._util.presentarLoading("Cargando...");
        loader.present().then(() => {
          this.consultarFormularioBloque().then(() => {
            loader.dismiss();
          });
        })
      }
    })
  }

  irAPreguntas(item: FormularioBloque) {
    this.navCtrl.push("ListFormularioPreguntaPage", {
      paramformulariobloque: item,
      paramformulario: this.paramFormulario
    });    
  }

  eliminarBloque(item: FormularioBloque){
    var index = this.listaFormulariosBloque.indexOf(item);
    if (index !== -1) this.listaFormulariosBloque.splice(index, 1);
    this.modificarFormulario();
  }

  presentConfirm(item: FormularioBloque) {
    let alert = this.alertCtrl.create({
      title: 'Esta seguro de realizar esta acción?',
      message: 'Presione OK para realizar la operación.',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Ok',
          handler: () => {
            //this.menu.open();
            try {
              this.eliminarBloque(item);
            } catch (error) {
              this._util.doAlert('','Ha ocurrido un error al eliminar. ' + error).present();;
            }
          }
        }
      ]
    });
    alert.present();
  }

  modificarFormulario() {
   
    let loader = this._util.presentarLoading("Grabando...");
    loader.present();

    this.paramFormulario.listaBloques = this.listaFormulariosBloque;

    this._genericFirestoreProvider.updateRecordFirestore(this.paramFormulario,ModeloFirebase.FORMULARIO).then(
      respuesta => {
        this._util.presentarToast("Registro actualizado.");
        this.viewCtrl.dismiss({ guardo: true });
        loader.dismiss();
      },
      error => {
        loader.dismiss();
        this._util.presentarToast(error);
        console.log(error);
      }
    );
  }


}
