import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalFormularioBloquePage } from './modal-formulariobloque';

@NgModule({
  declarations: [
    ModalFormularioBloquePage,
  ],
  imports: [
    IonicPageModule.forChild(ModalFormularioBloquePage),
  ],
})
export class ModalFormularioBloquePageModule {}
