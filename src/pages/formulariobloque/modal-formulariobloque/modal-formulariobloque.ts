import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Formulario } from '../../../modelos/Formulario';
import { FormularioBloque } from '../../../modelos/FormularioBloque';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { UtilidadesProvider } from '../../../providers/utilidades/utilidades';
import { GenericFirestoreProvider } from '../../../providers/genericfirestore/genericfirestore';
import { ModeloFirebase } from '../../../modelos/ModeloFirebase';

@IonicPage()
@Component({
  selector: 'page-modal-formulariobloque',
  templateUrl: 'modal-formulariobloque.html',
})
export class ModalFormularioBloquePage {

  private guardarFormulario: boolean;
  private formulario: FormGroup;

  private paramtipooperacion: number;
  private paramFormulario: Formulario;
  private paramFormularioBloque: FormularioBloque;
  private tituloDialogo: string;
  private estoyEditando: boolean;

  public CREAR: number = 1;
  public EDITAR: number = 2;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private formBuilder: FormBuilder,
    public _util: UtilidadesProvider,
    public _genericFirestoreProvider: GenericFirestoreProvider) {

    this.paramtipooperacion = this.navParams.get("paramtipooperacion");
    this.paramFormulario = this.navParams.get("paramformulario");
    this.paramFormularioBloque = this.navParams.get("paramformulariobloque");
   
    this.tituloDialogo =
      this.paramtipooperacion == this.CREAR
        ? "Nuevo bloque"
        : this.paramtipooperacion == this.EDITAR
          ? "Editar bloque"
          : null;

    this.estoyEditando =
      this.paramtipooperacion == this.CREAR
        ? false
        : this.paramtipooperacion == this.EDITAR
          ? true
          : false;

    this.guardarFormulario =
      this.paramtipooperacion == this.EDITAR ? true : false;

    this.formulario = formBuilder.group({
     

      txtNombre: [
        this.paramtipooperacion == this.CREAR
          ? ""
          : this.paramtipooperacion == this.EDITAR
            ? this.paramFormularioBloque.nombreBloque
            : null,
        Validators.required
      ],

      numOrden: [
        this.paramtipooperacion == this.CREAR
          ? ""
          : this.paramtipooperacion == this.EDITAR
            ? this.paramFormularioBloque.orden
            : null,
        Validators.required
      ],

      numColumnas: [
        this.paramtipooperacion == this.CREAR
          ? ""
          : this.paramtipooperacion == this.EDITAR
            ? this.paramFormularioBloque.numerocolumnas
            : null,
        Validators.required
      ],

      bolMostrarNombre: [
        this.paramtipooperacion == this.CREAR
          ? true
          : this.paramtipooperacion == this.EDITAR
            ? this.paramFormularioBloque.mostrarnombre
            : null
      ],

      bolHabilitada: [
        this.paramtipooperacion == this.CREAR
          ? true
          : this.paramtipooperacion == this.EDITAR
            ? this.paramFormularioBloque.habilitado
            : null
      ]
    });

    this.inicializarValores();

    this.formulario.valueChanges.subscribe(v => {
      this.guardarFormulario = this.formulario.valid;
    });
  }

  inicializarValores(){
    
  }

  guardar() {
    if (!this.formulario.valid) {
      return;
    }

    let loader = this._util.presentarLoading("Grabando...");
    loader.present();

    let formularioBloque: FormularioBloque = {
      id: this._util.guid(),
      idformulario: this.paramFormulario.id,
      nombreBloque: this.formulario.value.txtNombre,
      orden: Number(this.formulario.value.numOrden),
      numerocolumnas: Number(this.formulario.value.numColumnas),
      mostrarnombre: this.formulario.value.bolMostrarNombre,
      listaPreguntas: [],
      habilitado: this.formulario.value.bolHabilitada,
    };

    this.paramFormulario.listaBloques.push(formularioBloque);

    this._genericFirestoreProvider.updateRecordFirestore(this.paramFormulario,ModeloFirebase.FORMULARIO).then(
      respuesta => {
        this._util.presentarToast("Registro actualizado.");
        this.viewCtrl.dismiss({ guardo: true });
        loader.dismiss();
      },
      error => {
        loader.dismiss();
        this._util.presentarToast(error);
        console.log(error);
      }
    );

  }

  editar() {
    if (!this.formulario.valid) {
      return;
    }

    let loader = this._util.presentarLoading("Grabando...");
    loader.present();

    this.paramFormulario.listaBloques.find(x => x.id == this.paramFormularioBloque.id).nombreBloque = this.formulario.value.txtNombre;
    this.paramFormulario.listaBloques.find(x => x.id == this.paramFormularioBloque.id).orden = Number(this.formulario.value.numOrden);
    this.paramFormulario.listaBloques.find(x => x.id == this.paramFormularioBloque.id).numerocolumnas = Number(this.formulario.value.numColumnas);
    this.paramFormulario.listaBloques.find(x => x.id == this.paramFormularioBloque.id).mostrarnombre = this.formulario.value.bolMostrarNombre;
    this.paramFormulario.listaBloques.find(x => x.id == this.paramFormularioBloque.id).habilitado = this.formulario.value.bolHabilitada;

    this._genericFirestoreProvider.updateRecordFirestore(this.paramFormulario,ModeloFirebase.FORMULARIO).then(
      respuesta => {
        this._util.presentarToast("Registro actualizado.");
        this.viewCtrl.dismiss({ guardo: true });
        loader.dismiss();
      },
      error => {
        loader.dismiss();
        this._util.presentarToast(error);
        console.log(error);
      }
    );
  }

  cerralModal() {
    this.viewCtrl.dismiss();
  }



}
