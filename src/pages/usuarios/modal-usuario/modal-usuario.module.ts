import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalUsuarioPage } from './modal-usuario';

@NgModule({
  declarations: [
    ModalUsuarioPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalUsuarioPage),
  ],
})
export class ModalVendedorPageModule {}
