import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { UtilidadesProvider } from '../../../providers/utilidades/utilidades';
import { UsuarioDTO, PerfilUsuario } from '../../../modelos/UsuarioDTO';
import { UsuarioProvider } from '../../../providers/usuario/usuario';
import { Md5 } from 'ts-md5';

@IonicPage()
@Component({
  selector: 'page-modal-usuario',
  templateUrl: 'modal-usuario.html',
})
export class ModalUsuarioPage {

  private guardarFormulario: boolean;
  private formulario: FormGroup;

  private paramtipooperacion: number;
  private paramusuario: UsuarioDTO;
  private tituloDialogo: string;
  private estoyEditando: boolean;

  public CREAR: number = 1;
  public EDITAR: number = 2;
  public type = 'password';
  public showPass = false;


  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private formBuilder: FormBuilder,
    public _util: UtilidadesProvider,
    public _usuarioProvider: UsuarioProvider) {

    this.paramtipooperacion = this.navParams.get('paramtipooperacion')
    this.paramusuario = this.navParams.get('paramusuario')

    this.tituloDialogo = this.paramtipooperacion == this.CREAR ? 'Nuevo usuario' :
      this.paramtipooperacion == this.EDITAR ? 'Editar usuario' : null;

    this.estoyEditando = this.paramtipooperacion == this.CREAR ? false :
      this.paramtipooperacion == this.EDITAR ? true : false;

    this.guardarFormulario = this.paramtipooperacion == this.EDITAR ? true : false;

    this.formulario = formBuilder.group({
     
      txtNombreCompleto: [this.paramtipooperacion == this.CREAR ? '' :
        this.paramtipooperacion == this.EDITAR ? this.paramusuario.nombreCompleto : null,
      Validators.required],

      txtCorreo: [this.paramtipooperacion == this.CREAR ? '' :
        this.paramtipooperacion == this.EDITAR ? this.paramusuario.correo: null,
      Validators.required],

      txtClave: [this.paramtipooperacion == this.CREAR ? '' :
        this.paramtipooperacion == this.EDITAR ? Md5.hashAsciiStr(this.paramusuario.contrasena).toString() : null,
      Validators.required],

      txtTelefono: [this.paramtipooperacion == this.CREAR ? '' :
        this.paramtipooperacion == this.EDITAR ? this.paramusuario.telefono : null,
      Validators.required],

      txtHabilitada: [this.paramtipooperacion == this.CREAR ? true :
        this.paramtipooperacion == this.EDITAR ? this.paramusuario.habilitado : false],

    });
    this.formulario.valueChanges.subscribe((v) => {
      this.guardarFormulario = this.formulario.valid;
    }); 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalVendedorPage');
  }

  cerralModal() {
    this.viewCtrl.dismiss();
  }

  guardar() {

    if (!this.formulario.valid) { return; }

    let loader = this._util.presentarLoading("Grabando...");
    loader.present();

    let UsuarioVendedor: UsuarioDTO = {
      nombreCompleto: this.formulario.value.txtNombreCompleto,
      correo: this.formulario.value.txtCorreo,
      contrasena: this.formulario.value.txtClave,
      telefono: this.formulario.value.txtTelefono,
      perfil: PerfilUsuario._pefil_usuario.toString(),
      foto: null,
      nombreFoto: null,
      habilitado: true
    }

      this._usuarioProvider.crearUsuarioVendedor(UsuarioVendedor).then((respuesta) => {
      this._util.presentarToast("Registro almacenado.");
      this.viewCtrl.dismiss({ guardo: true });
      loader.dismiss();
    },
      (error) => {
        loader.dismiss();
        this._util.presentarToast(error);
        console.log(error);
      })
      
  }

  editar() {

    
    if (!this.formulario.valid) { return; }

    let loader = this._util.presentarLoading("Grabando...");
    loader.present();

   
    this.paramusuario.nombreCompleto = this.formulario.value.txtNombreCompleto;
    this.paramusuario.telefono = this.formulario.value.txtTelefono;

    
    this._usuarioProvider.actualizarUsuario(this.paramusuario).then((respuesta) => {
      this._util.presentarToast("Registro actualizado.");
      this.viewCtrl.dismiss({ guardo: true });
      loader.dismiss();
    },
      (error) => {
        loader.dismiss();
        this._util.presentarToast(error);
        console.log(error);
      })
      
  }

  showPassword() {
    this.showPass = !this.showPass;
    if (this.showPass) {
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }

}
