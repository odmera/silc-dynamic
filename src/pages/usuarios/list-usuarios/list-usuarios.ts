import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { UsuarioDTO } from '../../../modelos/UsuarioDTO';
import { UtilidadesProvider } from '../../../providers/utilidades/utilidades';
import { UsuarioProvider } from '../../../providers/usuario/usuario';

@IonicPage()
@Component({
  selector: 'page-list-usuarios',
  templateUrl: 'list-usuarios.html',
})
export class ListUsuariosPage {

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public _util: UtilidadesProvider,
              public _usuarioProvider: UsuarioProvider,
              public modalCtrl: ModalController) {
  }

  listaUsuarios: UsuarioDTO[] = [];

  ionViewDidEnter() {
    let loader = this._util.presentarLoading("Cargando...");
    loader.present().then(() => {
      this.consultarUsuarios();
      loader.dismiss();
    });
  }

  async consultarUsuarios() {
    this.listaUsuarios = [];

    let data = await this._usuarioProvider.consultarUsuarios()
    if (data != null && data.length > 0) {
      this.listaUsuarios = data;
    } else {
      console.warn("Sin datos");
    }
  }

  mostrarDialogo(tipoOperacion: number, item: UsuarioDTO) {

    let miModal = this.modalCtrl.create('ModalUsuarioPage',
    {
        paramtipooperacion: tipoOperacion,
        paramusuario: item
    });
    miModal.present();

    //esta funcion se ejecuta cuando cierro el modal
    miModal.onDidDismiss(parametros => {
      if (parametros != null) {
        let loader = this._util.presentarLoading("Cargando...");
        loader.present().then(() => {
          this.consultarUsuarios().then(() => {
            loader.dismiss();
          });
        })
      }

    })
  }

}
