import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';
import { Formulario } from '../../../modelos/Formulario';
import { FormularioPregunta } from '../../../modelos/FormularioPregunta';
import { UtilidadesProvider } from '../../../providers/utilidades/utilidades';
import { GenericFirestoreProvider } from '../../../providers/genericfirestore/genericfirestore';
import { FormularioBloque } from '../../../modelos/FormularioBloque';
import { TipoPreguntaEnum } from '../../../modelos/enum/TipoPreguntaEnum';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { ModeloFirebase } from '../../../modelos/ModeloFirebase';


@IonicPage()
@Component({
  selector: 'page-list-formulariopregunta',
  templateUrl: 'list-formulariopregunta.html',
})
export class ListFormularioPreguntaPage {

  listaFormulariosPregunta: FormularioPregunta[];
  private paramFormulario:Formulario;
  private paramFormularioBloque:FormularioBloque;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public _util: UtilidadesProvider,
    private alertCtrl: AlertController,
    public viewCtrl: ViewController,
    public _genericFirestoreProvider :GenericFirestoreProvider) {

      this.inicializarValores();
  }

  inicializarValores(){
    this.paramFormulario = this.navParams.get("paramformulario");
    this.paramFormularioBloque = this.navParams.get("paramformulariobloque");
    this.listaFormulariosPregunta = [];
  }

  ionViewDidLoad() {
    this.consultarFormulariosPregunta();
  }

  async consultarFormulariosPregunta() {

    let loader = this._util.presentarLoading("Cargando...");
    await loader.present();

    this.listaFormulariosPregunta = [];


    console.info(this.paramFormularioBloque);
    if (this.paramFormularioBloque != null && this.paramFormularioBloque.listaPreguntas != null && this.paramFormularioBloque.listaPreguntas.length > 0) {
      this.listaFormulariosPregunta = this.paramFormularioBloque.listaPreguntas;
    } else {
      console.warn("Sin datos");
    }

    // Se ordena por campo orden
    this.listaFormulariosPregunta.sort(function(a,b) {return (a.orden > b.orden) ? 1 : ((b.orden > a.orden) ? -1 : 0);} );
    loader.dismiss();
  }

  mostrarDialogo(tipoOperacion: number, item: FormularioPregunta) {
  
    let miModal = this.modalCtrl.create('ModalFormularioPreguntaPage',
    {
        paramtipooperacion: tipoOperacion,
        paramformulariopregunta: item,
        paramformulariobloque: this.paramFormularioBloque,
        paramformulario: this.paramFormulario,
    });
    miModal.present();

    // Esta funcion se ejecuta cuando cierro el modal
    miModal.onDidDismiss(parametros => {
      if (parametros != null) {
        let loader = this._util.presentarLoading("Cargando...");
        loader.present().then(() => {
          this.consultarFormulariosPregunta().then(() => {
            loader.dismiss();
          });
        })
      }
    })
  }

  consultarDescripcionTipoPregunta(idTipoPregunta){
    
    if(idTipoPregunta == TipoPreguntaEnum.TEXTO.id){
      return TipoPreguntaEnum.TEXTO.name;
    }
    if(idTipoPregunta == TipoPreguntaEnum.FECHA.id){
      return TipoPreguntaEnum.FECHA.name;
    }
    if(idTipoPregunta == TipoPreguntaEnum.NUMERICA.id){
      return TipoPreguntaEnum.NUMERICA.name;
    }
    if(idTipoPregunta == TipoPreguntaEnum.FOTO.id){
      return TipoPreguntaEnum.FOTO.name;
    }
    if(idTipoPregunta == TipoPreguntaEnum.OPCIONUNICARESPUESTA.id){
      return TipoPreguntaEnum.OPCIONUNICARESPUESTA.name;
    }
    if(idTipoPregunta == TipoPreguntaEnum.LOGICA.id){
      return TipoPreguntaEnum.LOGICA.name;
    }   

  }

  eliminarPregunta(item: FormularioPregunta){
    var index = this.listaFormulariosPregunta.indexOf(item);
    if (index !== -1) this.listaFormulariosPregunta.splice(index, 1);
    this.modificarFormulario();
  }

  presentConfirm(item: FormularioPregunta) {
    let alert = this.alertCtrl.create({
      title: 'Esta seguro de realizar esta acción?',
      message: 'Presione OK para realizar la operación.',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Ok',
          handler: () => {
            //this.menu.open();
            try {
              this.eliminarPregunta(item);
            } catch (error) {
              this._util.doAlert('','Ha ocurrido un error al eliminar. ' + error).present();;
            }
          }
        }
      ]
    });
    alert.present();
  }

  modificarFormulario() {
   
    let loader = this._util.presentarLoading("Grabando...");
    loader.present();

    this.paramFormulario.listaBloques.find(x => x.id == this.paramFormularioBloque.id).listaPreguntas = this.listaFormulariosPregunta;

    this._genericFirestoreProvider.updateRecordFirestore(this.paramFormulario,ModeloFirebase.FORMULARIO).then(
      respuesta => {
        this._util.presentarToast("Registro actualizado.");
        this.viewCtrl.dismiss({ guardo: true });
        loader.dismiss();
      },
      error => {
        loader.dismiss();
        this._util.presentarToast(error);
        console.log(error);
      }
    );
  }
 

}
