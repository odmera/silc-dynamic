import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListFormularioPreguntaPage } from './list-formulariopregunta';

@NgModule({
  declarations: [
    ListFormularioPreguntaPage,
  ],
  imports: [
    IonicPageModule.forChild(ListFormularioPreguntaPage),
  ],
})
export class ListFormularioPreguntaPageModule {}
