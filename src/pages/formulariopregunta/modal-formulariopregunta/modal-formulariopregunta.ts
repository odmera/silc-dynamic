import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Formulario } from '../../../modelos/Formulario';
import { FormularioBloque } from '../../../modelos/FormularioBloque';
import { FormularioPregunta } from '../../../modelos/FormularioPregunta';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { UtilidadesProvider } from '../../../providers/utilidades/utilidades';
import { GenericFirestoreProvider } from '../../../providers/genericfirestore/genericfirestore';
import { ModeloFirebase } from '../../../modelos/ModeloFirebase';
import { TipoPreguntaEnum } from '../../../modelos/enum/TipoPreguntaEnum';
import { FormularioOpcion } from '../../../modelos/FormularioOpcion';

@IonicPage()
@Component({
  selector: 'page-modal-formulariopregunta',
  templateUrl: 'modal-formulariopregunta.html',
})
export class ModalFormularioPreguntaPage {

  private guardarFormulario: boolean;
  private formulario: FormGroup;

  private paramtipooperacion: number;
  private paramFormulario: Formulario;
  private paramFormularioBloque: FormularioBloque;
  private paramFormularioPregunta: FormularioPregunta;
  private tituloDialogo: string;
  private estoyEditando: boolean;
  private colorPicker:string;
  private preguntaOpcion:boolean;
  private modificandoRespuesta:boolean;
  private respuestaEnModificacion:FormularioOpcion;

  public CREAR: number = 1;
  public EDITAR: number = 2;

  private color:string;
  private textoRespuesta:string;
  private listTipoPregunta;
  private listaOpciones:FormularioOpcion[];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private formBuilder: FormBuilder,
    public _util: UtilidadesProvider,
    public _genericFirestoreProvider: GenericFirestoreProvider) {

    this.paramtipooperacion = this.navParams.get("paramtipooperacion");
    this.paramFormulario = this.navParams.get("paramformulario");
    this.paramFormularioBloque = this.navParams.get("paramformulariobloque");
    this.paramFormularioPregunta = this.navParams.get("paramformulariopregunta");
   
    this.tituloDialogo =
      this.paramtipooperacion == this.CREAR
        ? "Nueva pregunta"
        : this.paramtipooperacion == this.EDITAR
          ? "Editar pregunta"
          : null;

    this.estoyEditando =
      this.paramtipooperacion == this.CREAR
        ? false
        : this.paramtipooperacion == this.EDITAR
          ? true
          : false;

    this.guardarFormulario =
      this.paramtipooperacion == this.EDITAR ? true : false;

    this.formulario = formBuilder.group({
     

      txtNombre: [
        this.paramtipooperacion == this.CREAR
          ? ""
          : this.paramtipooperacion == this.EDITAR
            ? this.paramFormularioPregunta.nombrePregunta
            : null,
        Validators.required
      ],

      txtSubtitulo: [
        this.paramtipooperacion == this.CREAR
          ? ""
          : this.paramtipooperacion == this.EDITAR
            ? this.paramFormularioPregunta.subtitulo
            : null
      ],

      numOrden: [
        this.paramtipooperacion == this.CREAR
          ? ""
          : this.paramtipooperacion == this.EDITAR
            ? this.paramFormularioPregunta.orden
            : null,
        Validators.required
      ],

      selTipoPregunta: [
        this.paramtipooperacion == this.CREAR
          ? ""
          : this.paramtipooperacion == this.EDITAR
            ? this.paramFormularioPregunta.tipoPreguntaEnumId
            : null,
        Validators.required
      ],

      bolMostrarNombre: [
        this.paramtipooperacion == this.CREAR
          ? true
          : this.paramtipooperacion == this.EDITAR
            ? this.paramFormularioPregunta.mostrarnombre
            : null
      ],

      bolHabilitada: [
        this.paramtipooperacion == this.CREAR
          ? true
          : this.paramtipooperacion == this.EDITAR
            ? this.paramFormularioPregunta.habilitado
            : null
      ],

      textoRespuesta: [""]
    });

    this.inicializarValores();

    this.formulario.valueChanges.subscribe(v => {
      this.guardarFormulario = this.formulario.valid;
    });
  }

  inicializarValores(){
    this.listTipoPregunta = [];
    this.listaOpciones = [];
    this.listTipoPregunta = this.buildTipoPreguntaEnumsArray();
    this.color = "";
    if(this.paramFormularioPregunta != null && this.paramFormularioPregunta.tipoPreguntaEnumId == "1"){
      this.preguntaOpcion = true;
      this.listaOpciones = this.paramFormularioPregunta.listaOpciones;
    } else {
      this.preguntaOpcion = false;
    }
    this.modificandoRespuesta = false;
  }

  guardar() {
    if(this.validarAntesDeGrabar()){
      return;
    }

    if (!this.formulario.valid) {
      return;
    }

    let loader = this._util.presentarLoading("Grabando...");
    loader.present();

    let formularioPregunta: FormularioPregunta = {
      id: this._util.guid(),
      idbloque: this.paramFormularioBloque.id,
      nombrePregunta: this.formulario.value.txtNombre,
      subtitulo: this.formulario.value.txtSubtitulo,
      orden: Number(this.formulario.value.numOrden),
      tipoPreguntaEnumId: this.formulario.value.selTipoPregunta,
      mostrarnombre: this.formulario.value.bolMostrarNombre,
      obligatoria: false,
      listaOpciones: this.formulario.value.selTipoPregunta == 1 ? this.listaOpciones : [],
      habilitado: this.formulario.value.bolHabilitada,
    };

    this.paramFormularioBloque.listaPreguntas.push(formularioPregunta);
    this.paramFormulario.listaBloques.find(x => x.id == this.paramFormularioBloque.id).listaPreguntas = this.paramFormularioBloque.listaPreguntas;

    this._genericFirestoreProvider.updateRecordFirestore(this.paramFormulario,ModeloFirebase.FORMULARIO).then(
      respuesta => {
        this._util.presentarToast("Registro actualizado.");
        this.viewCtrl.dismiss({ guardo: true });
        loader.dismiss();
      },
      error => {
        loader.dismiss();
        this._util.presentarToast(error);
        console.log(error);
      }
    );

  }

  editar() {
    if(this.validarAntesDeGrabar()){
      return;
    }

    if (!this.formulario.valid) {
      return;
    }

    let loader = this._util.presentarLoading("Grabando...");
    loader.present();

    this.paramFormularioBloque.listaPreguntas.find(x => x.id == this.paramFormularioPregunta.id).nombrePregunta = this.formulario.value.txtNombre;
    this.paramFormularioBloque.listaPreguntas.find(x => x.id == this.paramFormularioPregunta.id).subtitulo = this.formulario.value.txtSubtitulo;
    this.paramFormularioBloque.listaPreguntas.find(x => x.id == this.paramFormularioPregunta.id).orden = Number(this.formulario.value.numOrden);
    this.paramFormularioBloque.listaPreguntas.find(x => x.id == this.paramFormularioPregunta.id).tipoPreguntaEnumId = this.formulario.value.selTipoPregunta;
    this.paramFormularioBloque.listaPreguntas.find(x => x.id == this.paramFormularioPregunta.id).mostrarnombre = this.formulario.value.bolMostrarNombre;
    this.paramFormularioBloque.listaPreguntas.find(x => x.id == this.paramFormularioPregunta.id).listaOpciones = this.listaOpciones;
    this.paramFormularioBloque.listaPreguntas.find(x => x.id == this.paramFormularioPregunta.id).habilitado = this.formulario.value.bolHabilitada;

    this.paramFormulario.listaBloques.find(x => x.id == this.paramFormularioBloque.id).listaPreguntas = this.paramFormularioBloque.listaPreguntas;

    this._genericFirestoreProvider.updateRecordFirestore(this.paramFormulario,ModeloFirebase.FORMULARIO).then(
      respuesta => {
        this._util.presentarToast("Registro actualizado.");
        this.viewCtrl.dismiss({ guardo: true });
        loader.dismiss();
      },
      error => {
        loader.dismiss();
        this._util.presentarToast(error);
        console.log(error);
      }
    );
  }

  cerralModal() {
    this.viewCtrl.dismiss();
  }

  buildTipoPreguntaEnumsArray(): Object[] {
    var list = Object.keys(TipoPreguntaEnum).map(item => TipoPreguntaEnum[item]);
    return list;
  }

  agregarRespuesta(){
    if(this.validarRespuesta()){
      return;
    }
    let textoRespuesta = this.formulario.value.textoRespuesta;
    let listaOpcion:FormularioOpcion = {
      id: this._util.guid(),
      nombreOpcion: textoRespuesta,
      color: this.color
    }
    this.listaOpciones.push(listaOpcion);
    console.info(this.listaOpciones);
    this.color = "";
    this.formulario.controls["textoRespuesta"].setValue("");
  }

  validarRespuesta(){
    let textoRespuesta = this.formulario.value.textoRespuesta;

    if(textoRespuesta == null || textoRespuesta.length == 0){
      this._util.doAlert(
        "Validación al agregar respuesta",
        "El texto de la respuesta no puede ser vacio o nulo"
      ).present();
      return true;
    }
   
  }

  validarAntesDeGrabar(){
    let tipoPreguntaEnumId = this.formulario.value.selTipoPregunta;

    if(tipoPreguntaEnumId == 1 && (this.listaOpciones == null || this.listaOpciones.length == 0)){
      this._util.doAlert(
        "Validación pregunta unica respuesta",
        "El tipo de Pregunta con única respuesta debe contener al menos una respuesta"
      ).present();
      return true;
    }
   
  }

  validarTipoPregunta(item:any){
    if (item != null && item == 1) {
      this.preguntaOpcion = true;
    } else {
      this.preguntaOpcion = false;
    }
  }

  esconderBotonAgregarRespuesta(respuesta:FormularioOpcion){
    this.respuestaEnModificacion = respuesta;
    this.formulario.controls["textoRespuesta"].setValue(respuesta.nombreOpcion);
    this.color = respuesta.color;
    this.modificandoRespuesta = true;
  }

  modificarRespuesta(){
    if(this.validarRespuesta()){
      return;
    }
    let textoRespuesta = this.formulario.value.textoRespuesta;

    this.listaOpciones.find(x => x.id == this.respuestaEnModificacion.id).color = this.color;
    this.listaOpciones.find(x => x.id == this.respuestaEnModificacion.id).nombreOpcion = textoRespuesta;

    this.color = "";
    this.formulario.controls["textoRespuesta"].setValue("");
    this.modificandoRespuesta = false;
    this.respuestaEnModificacion = null;
  }

  eliminarRespuesta(respuesta:FormularioOpcion){
    var index = this.listaOpciones.indexOf(respuesta);
    if (index !== -1) this.listaOpciones.splice(index, 1);
  }

}
