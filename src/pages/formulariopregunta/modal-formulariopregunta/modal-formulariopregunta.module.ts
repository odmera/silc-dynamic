import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalFormularioPreguntaPage } from './modal-formulariopregunta';
import { ColorPickerModule } from 'ngx-color-picker';


@NgModule({
  declarations: [
    ModalFormularioPreguntaPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalFormularioPreguntaPage),
    ColorPickerModule
  ],
})
export class ModalFormularioBloquePageModule {}
