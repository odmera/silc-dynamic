import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenucriterioAgrupadoPage } from './menucriterioagrupado';


@NgModule({
  declarations: [
    MenucriterioAgrupadoPage,
  ],
  imports: [
    IonicPageModule.forChild(MenucriterioAgrupadoPage),
  ],
})
export class MenucriterioagrupadoPageModule {}
