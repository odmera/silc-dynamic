import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Club } from '../../modelos/Club';
import { UtilidadesProvider } from '../../providers/utilidades/utilidades';
import { Formulario } from '../../modelos/Formulario';
import { GenericpouchdbProvider } from '../../providers/genericpouchdb/genericpouchdb';
import { GenericFirestoreProvider } from '../../providers/genericfirestore/genericfirestore';
import { ModelNamesSqlLiteEnum } from '../../modelos/enum/ModelNamesSqlLiteEnum';
import { ModeloFirebase } from '../../modelos/ModeloFirebase';
import { ParamFormularioDinamico } from '../../modelos/ParamFormularioDinamico';


@IonicPage()
@Component({
  selector: 'page-menucriterioagrupado',
  templateUrl: 'menucriterioagrupado.html',
})
export class MenucriterioAgrupadoPage {

  private club:Club;
  public formulario:Formulario;
  public listaFormulariosMostrar: Formulario[]= [];

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public _util: UtilidadesProvider,
              public _genericPouchdbProvider: GenericpouchdbProvider,
              public _genericFirestoreProvider: GenericFirestoreProvider) {
    this.club = this.navParams.get('club');
    this.formulario = this.navParams.get('formulario');
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenucriterioAgrupadoPage');
    this.consultarFormularios();
  }

  // Funciones de redireccionamiento a formularios

  async openFormDynamic(formulario:Formulario){
    if(formulario.habilitado === false){
       this._util.doAlert("Mensaje","El formulario se encuentra inhabilitada").present();
       return;
    }
    if(formulario.agrupaFormulario === true){ // se envian a los formularios agrupados
      let loader = this._util.presentarLoading("Cargando formulario...");
      await loader.present();
      this.navCtrl.push("MenucriterioAgrupadoPage", { formulario: formulario,club:this.club });
      loader.dismiss();
    }else{ //se envia al formulario dinamico

      let paramFormDinamico:ParamFormularioDinamico = {
         formularios : formulario,
         formulariolleno: false,
         club:this.club
      }
      let loader = this._util.presentarLoading("Cargando formulario...");
      await loader.present();
      this.navCtrl.push("FormularioDinamicoPage", { paramFormDinamico: paramFormDinamico });
      loader.dismiss();
    }
  }


  // Funciones de redireccionamiento a formularios

  async consultarFormularios() {
    let loader = this._util.presentarLoading("Cargando formularios...");
    await loader.present();
    try {
      let listaFormularios:Formulario[] = [];
      let isApp = this._util.isApp();
      if(isApp == true){
        // Nombre de la db para el almacenamiento local
        await this._genericPouchdbProvider.constructorToObjectDb(ModelNamesSqlLiteEnum.FORMULARIO.toString());
        let data = await this._genericPouchdbProvider.findRecord()
        if (data != null && data.length > 0) {
          listaFormularios = data;
        } else {
          console.warn("Sin datos");
        }
      }
      else{
          let data = await this._genericFirestoreProvider.findRecordFirestore(ModeloFirebase.FORMULARIO);
          console.log("this.listaFormularios firebase " , data );
          if (data != null && data.length > 0) {
            listaFormularios = data;
          } else {
            console.warn("Sin datos");
          }
      }

      //se buscan los item del formulario agrupado y se agregan a la lista a mostrar
      if(this.formulario.formulariosAgrupacion != undefined && 
        this.formulario.formulariosAgrupacion != null && 
        this.formulario.formulariosAgrupacion.length > 0){
        for (let itemFormularioAgrupado of this.formulario.formulariosAgrupacion) {
          for (let itemFormulario of listaFormularios) {
            if(itemFormularioAgrupado.id === itemFormulario.id){
              if(itemFormularioAgrupado.checked ==true){
                this.listaFormulariosMostrar.push(itemFormulario);
              }
            }
          }
        }
      }
      //se ordena de manera alfabetica.
      this.listaFormulariosMostrar.sort(function(a,b) {return (a.nombreFormulario > b.nombreFormulario) ? 1 : ((b.nombreFormulario > a.nombreFormulario) ? -1 : 0);} );

    } catch (error) {
      this._util.doAlert('','Ocurrio algo obteniendo los formularios ' + error).present();
    }
    finally{
      loader.dismiss();
    }
  }

}
