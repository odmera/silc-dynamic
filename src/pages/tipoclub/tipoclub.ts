import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { UtilidadesProvider } from '../../providers/utilidades/utilidades';
import { GenericpouchdbProvider } from '../../providers/genericpouchdb/genericpouchdb';
import { ModelNamesSqlLiteEnum } from '../../modelos/enum/ModelNamesSqlLiteEnum';
import { Division } from '../../modelos/Division';
import { GenericFirestoreProvider } from '../../providers/genericfirestore/genericfirestore';
import { ModeloFirebase } from '../../modelos/ModeloFirebase';
import { Events } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-tipoclub',
  templateUrl: 'tipoclub.html',
})
export class TipoclubPage {

  public press: number = 0;
  public listaDivisiones: Division[] = [];
  
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public alertCtrl: AlertController,
              public _genericpouchdbProvider: GenericpouchdbProvider,
              public _genericFirestoreProvider: GenericFirestoreProvider,
              public _util: UtilidadesProvider,
              public events: Events) {

      //este evento se emite desde SincronizarDatosProvider metodo          
      events.subscribe('division:consulta', (valor) => { 
        this.consultarDivisiones();
      });
    

  }

  ionViewDidLoad() {
    
  }
  ionViewDidEnter() {
    this.consultarDivisiones();
  }

  irSeleccionClub(item:Division){
    /*if(item.habilitado === false){
      this._util.doAlert("Mensaje","La división se encuentra inhabilitada").present();
      return;
    }
    if(item.listaFormularios == null || item.listaFormularios.length == 0){
      this._util.doAlert("Mensaje","La división no tiene formularios asociados.").present();
      return;
    }*/

    this.navCtrl.push('SeleccionclubPage', { 'division': item } );
  }

  async consultarDivisiones() {

    let loader = this._util.presentarLoading("Cargando...");
    await loader.present();

    try {

      this.listaDivisiones = [];

      let isApp = this._util.isApp();
      if(isApp == true){
        // Nombre de la db para el almacenamiento local
        await this._genericpouchdbProvider.constructorToObjectDb(ModelNamesSqlLiteEnum.DIVISION.toString());
        let data = await this._genericpouchdbProvider.findRecord()
        if (data != null && data.length > 0) {
          this.listaDivisiones = data;
        } else {
          console.warn("Sin datos");
        }
      }
      else{
          let data = await this._genericFirestoreProvider.findRecordFirestore(ModeloFirebase.DIVISION);
          console.log("this.listaDivisiones firebase " , data );
          if (data != null && data.length > 0) {
            this.listaDivisiones = data;
          } else {
            console.warn("Sin datos");
          }
      }
      //se ordena de manera alfabetica.
      this.listaDivisiones.sort(function(a,b) {return (a.nombreDivision > b.nombreDivision) ? 1 : ((b.nombreDivision > a.nombreDivision) ? -1 : 0);} );
    } catch (error) {
      this._util.doAlert('','Ocurrio algo obteniendo las diviciones ' + error).present();
    }
    finally{
      loader.dismiss();
    }
    
  }


  pressEvent(e) {
    this.press++;
    console.info(this.press);
    if(this.press==10){
        let alert = this.alertCtrl.create({
          title: 'Felicitaciones boludo curioso...',
          subTitle: 'Desarrolladores: Julian Valencia, Oscar Mera',
          buttons: ['Ok']
        });
        alert.present();
    }
  }

}
