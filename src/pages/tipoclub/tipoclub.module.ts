import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TipoclubPage } from './tipoclub';

@NgModule({
  declarations: [
    TipoclubPage,
  ],
  imports: [
    IonicPageModule.forChild(TipoclubPage),
  ],
})
export class TipoclubPageModule {}
