import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListFormularioPage } from './list-formulario';

@NgModule({
  declarations: [
    ListFormularioPage,
  ],
  imports: [
    IonicPageModule.forChild(ListFormularioPage),
  ],
})
export class ListFormularioPageModule {}
