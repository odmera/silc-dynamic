import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController
} from "ionic-angular";
import { Formulario } from "../../../modelos/Formulario";
import { UtilidadesProvider } from "../../../providers/utilidades/utilidades";
import { GenericFirestoreProvider } from "../../../providers/genericfirestore/genericfirestore";
import { ModeloFirebase } from "../../../modelos/ModeloFirebase";

@IonicPage()
@Component({
  selector: "page-list-formulario",
  templateUrl: "list-formulario.html"
})
export class ListFormularioPage {
  listaFormularios: Formulario[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public _util: UtilidadesProvider,
    public _genericFirestoreProvider: GenericFirestoreProvider
  ) {
    this.inicializarValores();
  }

  inicializarValores() {
    this.listaFormularios = [];
  }

  ionViewDidLoad() {
    this.consultarFormularios();
  }

  async consultarFormularios() {
    let loader = this._util.presentarLoading("Cargando...");
    await loader.present();

    this.listaFormularios = [];

    let data = await this._genericFirestoreProvider.findRecordFirestore(
      ModeloFirebase.FORMULARIO
    );
    console.info(data);
    if (data != null && data.length > 0) {
      this.listaFormularios = data;
    } else {
      console.warn("Sin datos");
    }

    // Se ordena de manera alfabetica.
    this.listaFormularios.sort(function(a, b) {
      return a.nombreFormulario > b.nombreFormulario
        ? 1
        : b.nombreFormulario > a.nombreFormulario
          ? -1
          : 0;
    });
    loader.dismiss();
  }

  mostrarDialogo(tipoOperacion: number, item: Formulario) {
    let miModal = this.modalCtrl.create("ModalFormularioPage", {
      paramtipooperacion: tipoOperacion,
      paramformulario: item
    });
    miModal.present();

    // Esta funcion se ejecuta cuando cierro el modal
    miModal.onDidDismiss(parametros => {
      if (parametros != null) {
        let loader = this._util.presentarLoading("Cargando...");
        loader.present().then(() => {
          this.consultarFormularios().then(() => {
            loader.dismiss();
          });
        });
      }
    });
  }

  irABloques(item: Formulario) {
    this.navCtrl.push("ListFormularioBloquePage", {
      paramformulario: item
    });   
  }
}
