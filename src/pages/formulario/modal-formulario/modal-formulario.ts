import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController
} from "ionic-angular";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { Formulario } from "../../../modelos/Formulario";
import { ViewController } from "ionic-angular/navigation/view-controller";
import { UtilidadesProvider } from "../../../providers/utilidades/utilidades";
import { GenericFirestoreProvider } from "../../../providers/genericfirestore/genericfirestore";
import { ModeloFirebase } from "../../../modelos/ModeloFirebase";

@IonicPage()
@Component({
  selector: "page-modal-formulario",
  templateUrl: "modal-formulario.html"
})
export class ModalFormularioPage {
  private guardarFormulario: boolean;
  private formulario: FormGroup;

  private paramtipooperacion: number;
  private paramFormulario: Formulario;
  private tituloDialogo: string;
  private estoyEditando: boolean;
  private agrupaOtrosFormularios: boolean;

  private listFormulariosAgrupacion: Formulario[];

  public CREAR: number = 1;
  public EDITAR: number = 2;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private formBuilder: FormBuilder,
    public _util: UtilidadesProvider,
    public modalCtrl: ModalController,
    public _genericFirestoreProvider: GenericFirestoreProvider
  ) {
    this.paramtipooperacion = this.navParams.get("paramtipooperacion");
    this.paramFormulario = this.navParams.get("paramformulario");

    this.tituloDialogo =
      this.paramtipooperacion == this.CREAR
        ? "Nuevo formulario"
        : this.paramtipooperacion == this.EDITAR
          ? "Editar formulario"
          : null;

    this.estoyEditando =
      this.paramtipooperacion == this.CREAR
        ? false
        : this.paramtipooperacion == this.EDITAR
          ? true
          : false;

    this.guardarFormulario =
      this.paramtipooperacion == this.EDITAR ? true : false;

    this.formulario = formBuilder.group({
      txtNombre: [
        this.paramtipooperacion == this.CREAR
          ? ""
          : this.paramtipooperacion == this.EDITAR
            ? this.paramFormulario.nombreFormulario
            : null,
        Validators.required
      ],

      txtDescripcion: [
        this.paramtipooperacion == this.CREAR
          ? ""
          : this.paramtipooperacion == this.EDITAR
            ? this.paramFormulario.descripcionFormulario
            : null,
        Validators.required
      ],

      bolAgrupaFormulario: [
        this.paramtipooperacion == this.CREAR
          ? false
          : this.paramtipooperacion == this.EDITAR
            ? this.paramFormulario.agrupaFormulario
            : null
      ],

      bolHabilitada: [
        this.paramtipooperacion == this.CREAR
          ? true
          : this.paramtipooperacion == this.EDITAR
            ? this.paramFormulario.habilitado
            : null
      ]
    });

    this.inicializarValores();

    this.formulario.valueChanges.subscribe(v => {
      this.guardarFormulario = this.formulario.valid;
    });
  }

  inicializarValores() {
    this.listFormulariosAgrupacion = [];
    if (this.paramFormulario != null && this.paramFormulario.agrupaFormulario) {
      this.agrupaOtrosFormularios = true;
      this.listFormulariosAgrupacion = this.paramFormulario.formulariosAgrupacion;
    } else {
      this.agrupaOtrosFormularios = false;
    }
  }

  guardar() {
    if (this.validarFormulario()) {
      return;
    }

    if (!this.formulario.valid) {
      return;
    }

    let loader = this._util.presentarLoading("Grabando...");
    loader.present();

    let agrupaFormulario = this.formulario.value.bolAgrupaFormulario;

    if (agrupaFormulario == false) {
      this.listFormulariosAgrupacion = [];
    }

    let Formulario: Formulario = {
      nombreFormulario: this.formulario.value.txtNombre,
      descripcionFormulario: this.formulario.value.txtDescripcion,
      agrupaFormulario: this.formulario.value.bolAgrupaFormulario,
      formulariosAgrupacion: this.listFormulariosAgrupacion,
      listaBloques: [],
      habilitado: this.formulario.value.bolHabilitada
    };

    this._genericFirestoreProvider
      .addRecordFirestore(Formulario, ModeloFirebase.FORMULARIO)
      .then(
        respuesta => {
          this._util.presentarToast("Registro almacenado.");
          this.viewCtrl.dismiss({ guardo: true });
          loader.dismiss();
        },
        error => {
          loader.dismiss();
          this._util.presentarToast(error);
          console.log(error);
        }
      );
  }

  editar() {
    if (this.validarFormulario()) {
      return;
    }

    if (!this.formulario.valid) {
      return;
    }

    let loader = this._util.presentarLoading("Grabando...");
    loader.present();

    let agrupaFormulario = this.formulario.value.bolAgrupaFormulario;

    if (agrupaFormulario == false) {
      this.listFormulariosAgrupacion = [];
    } else {
      this.listFormulariosAgrupacion.forEach(element => {
        element.listaBloques = [];
      });
    }

    this.paramFormulario.nombreFormulario = this.formulario.value.txtNombre;
    this.paramFormulario.descripcionFormulario = this.formulario.value.txtDescripcion;
    this.paramFormulario.agrupaFormulario = this.formulario.value.bolAgrupaFormulario;
    this.paramFormulario.formulariosAgrupacion = this.listFormulariosAgrupacion;
    this.paramFormulario.habilitado = this.formulario.value.bolHabilitada;

    this._genericFirestoreProvider
      .updateRecordFirestore(this.paramFormulario, ModeloFirebase.FORMULARIO)
      .then(
        respuesta => {
          this._util.presentarToast("Registro actualizado.");
          this.viewCtrl.dismiss({ guardo: true });
          loader.dismiss();
        },
        error => {
          loader.dismiss();
          this._util.presentarToast(error);
          console.log(error);
        }
      );
  }

  cerralModal() {
    this.viewCtrl.dismiss();
  }

  mostrarBotonModalFormularios() {
    let agrupaFormulario = this.formulario.value.bolAgrupaFormulario;

    if (agrupaFormulario == true) {
      this.agrupaOtrosFormularios = true;
    } else {
      this.agrupaOtrosFormularios = false;
    }
  }

  mostrarModalFormularios() {
    let miModal = this.modalCtrl.create(
      "ModalFormularioAgrupaformulariosPage",
      { formularios: this.listFormulariosAgrupacion, idFormulario: this.paramFormulario == null ? null : this.paramFormulario.id  }
    );
    miModal.present();

    miModal.onDidDismiss(listFormulariosAgrupacion => {
      this.listFormulariosAgrupacion = listFormulariosAgrupacion;
      console.info(this.listFormulariosAgrupacion);
    });
  }

  validarFormulario() {
    let agrupaFormulario = this.formulario.value.bolAgrupaFormulario;

    if (
      agrupaFormulario == true &&
      (this.listFormulariosAgrupacion == null ||
        this.listFormulariosAgrupacion.length == 0)
    ) {
      this._util
        .doAlert(
          "Validación al crear formulario",
          "Se ha chequeado la opción Agrupa Formularios, pero no se ha seleccionado ninguno"
        )
        .present();
      return true;
    }
  }

  eliminarFormulario(formulario: Formulario) {
    var index = this.listFormulariosAgrupacion.indexOf(formulario);
    if (index !== -1) this.listFormulariosAgrupacion.splice(index, 1);
  }
}
