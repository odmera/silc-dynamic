import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Formulario } from '../../../modelos/Formulario';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { UtilidadesProvider } from '../../../providers/utilidades/utilidades';
import { GenericFirestoreProvider } from '../../../providers/genericfirestore/genericfirestore';
import { ModeloFirebase } from '../../../modelos/ModeloFirebase';

@IonicPage()
@Component({
  selector: 'page-modal-formularioagrupaformularios',
  templateUrl: 'modal-formulario-agrupaformularios.html',
})
export class ModalFormularioAgrupaformulariosPage {

  private guardarFormulario: boolean;

  private paramFormularios: Formulario[];
  private idFormularioPadre: string;

  private listFormulariosAgrupacion:Formulario[];

  public CREAR: number = 1;
  public EDITAR: number = 2;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private formBuilder: FormBuilder,
    public _util: UtilidadesProvider,
    public _genericFirestoreProvider: GenericFirestoreProvider) {

    this.paramFormularios = this.navParams.get("formularios");
    this.idFormularioPadre = this.navParams.get("idFormulario");
   
    this.inicializarValores();

  }

  inicializarValores(){
    this.listFormulariosAgrupacion = [];
    this.consultarFormularios();
  }

  async consultarFormularios() {
    let loader = this._util.presentarLoading("Cargando...");
    await loader.present();

    let data = await this._genericFirestoreProvider.findRecordFirestore(
      ModeloFirebase.FORMULARIO
    );
    console.info(data);
    if (data != null && data.length > 0) {
      data.forEach(element => {
        let formulario:Formulario = element;
        if(formulario.agrupaFormulario == false){
          if(this.idFormularioPadre == null || (this.idFormularioPadre != null && this.idFormularioPadre != formulario.id)){
            this.listFormulariosAgrupacion.push(formulario);
          }
        }
      });
    } else {
      console.warn("Sin datos");
    }

    if(this.paramFormularios != null && this.paramFormularios.length > 0){
      let newFormulariosList:Formulario[] = [];
      this.listFormulariosAgrupacion.forEach(element => {
        if((this.paramFormularios.find(x => x.id == element.id)) == null){
          console.info('Agregando este que falta: ',element.nombreFormulario);
          newFormulariosList.push(element);
        }
      }); 
      this.listFormulariosAgrupacion = newFormulariosList;
    }

    // Se ordena de manera alfabetica.
    this.listFormulariosAgrupacion.sort(function(a, b) {
      return a.nombreFormulario > b.nombreFormulario
        ? 1
        : b.nombreFormulario > a.nombreFormulario
          ? -1
          : 0;
    });
    loader.dismiss();
  }  

  cerralModal(){
    this.viewCtrl.dismiss(this.paramFormularios);
  }

  agregarFormulariosAgrupacion(){
    if (this.listFormulariosAgrupacion.length != 0) {
      this.listFormulariosAgrupacion.forEach(element => {
        let formularioChequeado: Formulario = element;
        if (formularioChequeado.agrupadoOtroFormulario == true) {
          formularioChequeado.listaBloques = [];
          this.paramFormularios.push(formularioChequeado);
        }
      });
    }
    this.viewCtrl.dismiss(this.paramFormularios);    
  }

  checkFormulario(event:any,formulario:Formulario){
    formulario.agrupadoOtroFormulario = event.checked;
  }


}
