import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalFormularioAgrupaformulariosPage } from './modal-formulario-agrupaformularios';

@NgModule({
  declarations: [
    ModalFormularioAgrupaformulariosPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalFormularioAgrupaformulariosPage),
  ],
})
export class ModalFormularioAgrupaformulariosPageModule {}
