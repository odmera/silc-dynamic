import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FirmaPage } from './firma';
import { SignaturePadModule } from 'angular2-signaturepad';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    FirmaPage
  ],
  imports: [
    IonicPageModule.forChild(FirmaPage),
    SignaturePadModule,
    ComponentsModule
  ],
})
export class FirmaPageModule {}
