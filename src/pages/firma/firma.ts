import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import {SignaturePad} from 'angular2-signaturepad/signature-pad'
import { Foto } from '../../modelos/Foto';
import { UtilidadesProvider } from '../../providers/utilidades/utilidades'
import { Club } from '../../modelos/Club';
import { SincronizarDatosProvider } from '../../providers/sincronizar-datos/sincronizar-datos';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlmacenamientoLocalProvider } from '../../providers/almacenamiento-local/almacenamiento-local';
import { GenericpouchdbProvider } from '../../providers/genericpouchdb/genericpouchdb';
import { Sincronizacion } from '../../modelos/Sincronizacion';
import { ModelNamesSqlLiteEnum } from '../../modelos/enum/ModelNamesSqlLiteEnum';
import { AttachmentImage } from '../../modelos/interface/imageinterface/AttachmentImage';
import { ApiProvider } from '../../providers/api/api';
import { CampoDeJuegoMedidas } from '../../modelos/CampoDeJuegoMedidas';
import { AlmacenamientoArchivosFirebaseProvider } from '../../providers/almacenamiento-archivos-firebase/almacenamiento-archivos-firebase';
import { GenericFirestoreProvider } from '../../providers/genericfirestore/genericfirestore';
import { App } from 'ionic-angular/components/app/app';
import { Division } from '../../modelos/Division';


@IonicPage()
@Component({
  selector: 'page-firma',
  templateUrl: 'firma.html',
})
export class FirmaPage {

  @ViewChild(SignaturePad) public signaturePad : SignaturePad;
  private listImagenes: Array<Foto> = [];
  private mostrarBtnSincronizar: boolean= false;
  public signaturePadOptions : Object = {
    'minWidth': 2,
    'canvasWidth': 950,
    'canvasHeight': 200
  };
  public signatureImage : string;

  private club: Club; 
  private division:Division;
  public formulario: FormGroup;
  private guardarFormulario: boolean;
  private _revPouchDb: string;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public _util: UtilidadesProvider,
              public _sincronizarDatosProvider:SincronizarDatosProvider,
              public formBuilder: FormBuilder,
              public _almacenamientoLocalProvider: AlmacenamientoLocalProvider,
              public _genericPouchdbProvider: GenericpouchdbProvider,
              public _apiProvider:ApiProvider,
              public _almacenamientoArchivosFirebaseProvider:AlmacenamientoArchivosFirebaseProvider,
              public _genericFirestoreProvider:GenericFirestoreProvider,
              public appCtrl: App) {

          this._revPouchDb = null;

          // Nombre de la db para el almacenamiento local
          this._genericPouchdbProvider.constructorToObjectDb(ModelNamesSqlLiteEnum.SINCRONIZACION.toString());


          this.formulario = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
          });

          this.formulario.valueChanges.subscribe((v) => {
            this.guardarFormulario = this.formulario.valid;
            console.log(this.guardarFormulario);
          }); 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FirmaPage');
    this.club = this.navParams.get('club');
    this.division = this.navParams.get("division");
    console.log(this.club);
    this.consultarSincronizacion();
  }

  async consultarSincronizacion() {
    try {
      let sincronizacion: Sincronizacion = null;

      // Consulta datos de Pdb
      if(this._util.isApp() == true) {
        sincronizacion = await this._genericPouchdbProvider.getRecord(this.club.id) as Sincronizacion;
      } 

      console.log("sincronizacion " , sincronizacion);
      // Se cargan los datos al formulario
      if (sincronizacion != null) {
        this._revPouchDb = sincronizacion._rev;
        this.signatureImage =  sincronizacion.firma;
        this.mostrarBtnSincronizar=true;
        if(this.signatureImage != null && this.signatureImage.length > 0){
          this.signaturePad.fromDataURL(this.signatureImage,this.signaturePadOptions);
        }
        this.listImagenes = this.obtenerImgsBase64Array(sincronizacion.fotosCapturadas);
      }
      else {
        console.info('No existen registros para este Equipo');
      }

    } catch (error) {
      console.info('No existen registros para este Equipo', error);
    }

  }


  ngAfterViewInit() {
    console.log("Reset Model Screen");
     this.signaturePad.clear();
     this.canvasResize();
  }
  
  canvasResize() {
    let canvas = document.querySelector('canvas');
    this.signaturePad.set('minWidth', 2);
    console.log(canvas.offsetWidth);
    this.signaturePad.set('canvasWidth', canvas.offsetWidth);
    this.signaturePad.set('canvasHeight', canvas.offsetHeight);
  }

  
  drawCancel() {
    this.viewCtrl.dismiss();
  }

  async drawComplete() {

    if(this.signaturePad.isEmpty()){
     this._util.doAlert("Mensaje", 'Debe establecer la firma.').present();
     return;      
    }

    let loader = this._util.presentarLoading("Guardando firma...");
    await loader.present();

    try {

      let datosAl = await this._almacenamientoLocalProvider.obtenerDatosSesion();
      if (datosAl != null && datosAl.idUsuario != null) {

        if (this._util.isApp() == true) {

            //obtengo la firma en base 64
            this.signatureImage = this.signaturePad.toDataURL();

            let sincronizacion: Sincronizacion = {
              _rev: this._revPouchDb,
              _id: this.club.id,
              idclub: this.club.id,
              nombreclub: this.club.nombreCompleto,
              nombreResponsable:this.club.responsableLicencias,
              firma:this.signatureImage,
              fechasincronizacion :null,
              observacionsincronizacion:"",
              correoelectronicoenvio:null,
              idusuario:datosAl.idUsuario,
              usuarioMail: datosAl.correoUsuario,
              fotosCapturadas:this._util.convertirArrayObjetoValidoPouch(this.listImagenes),
              urlImagenCampoJuego:null,
              urlPdf:null
            }

            let respuesta = await this._genericPouchdbProvider.putRecord(sincronizacion);
            this._revPouchDb = respuesta['rev'];
            this.mostrarBtnSincronizar = true;
            this._util.presentarToast("Firma almacenada.");
        }
    
      }
      
    } catch (error) {
      this._util.doAlert("Mensaje", 'Error ' + error).present();
      loader.dismiss();
      console.error(error);
    }
    finally{
      loader.dismiss();
    }
  }

  drawClear() {
    this.signaturePad.clear();
    this.mostrarBtnSincronizar = false;
  }

  async obtenerImgBase64(ev: string) {
    let loader = this._util.presentarLoading("Procesando...");
    await loader.present();
    try {

      //se arma objecto foto
      let fotoBase64: Foto = {
        foto: ev,
        nombreFoto: this._util.convertirFechaUnixtime(new Date),
        esBase64: true
      }
      this.listImagenes.push(fotoBase64);

      let fotosPB = this._util.convertirArrayObjetoValidoPouch(this.listImagenes);

      if(this._util.isApp() == true) {
        //se consulta el dato y se realiza la actualizacion
        let sincronizacion:Sincronizacion = await this._genericPouchdbProvider.getRecord(this.club.id) as Sincronizacion;
        sincronizacion.fotosCapturadas = fotosPB;
        await this._genericPouchdbProvider.putRecord(sincronizacion);
      } 
      
    } catch (error) {
      this._util.doAlert("Mensaje", 'Error ' + error).present();
      loader.dismiss();
      console.error(error);
    }
    finally{
      loader.dismiss();
    }
  }

   async sincronizarDatos(){
    
    let loading = this._util.presentarLoading("Sincronizando...");
    let loadingPdf = this._util.presentarLoading("Generando pdf en el servidor y enviando correos...");
    let loadingfileFirebase = this._util.presentarLoading("Enviando pdf a la nube...");
    try {
        await loading.present();

        if(this._util.isConect() == true){
           
            //se obtiene el dato de sincronizacion y se le setea le correo al cuando se le enviara el correo electronico
            let sincronizacion:Sincronizacion = await this._genericPouchdbProvider.getRecord(this.club.id) as Sincronizacion;
            sincronizacion.correoelectronicoenvio = this.formulario.value.email;
            sincronizacion.fechasincronizacion= this._util.convertirFechaUnixtime(new Date());

            //empieza la sincronizacion de los datos
            let respuesta = await this._sincronizarDatosProvider.sincronizarDatos(this.club.id,this.club._id,sincronizacion,this.division);
            if(respuesta.valido == true){
              try {
  
                await loadingPdf.present();
                let respuestaApi = await this._apiProvider.enviarDatosApiPdf(respuesta.datosPdf);
                //console.log("respuestaApi['state']" , respuestaApi['state']);
                if(respuestaApi['state'] == 'true' || respuestaApi['state'] == true){

                  await loadingfileFirebase.present();
                  //consulto medida del campo de juego para tomar la imagen del campo de juego y enviarla y firebase
                  await this._genericPouchdbProvider.constructorToObjectDb(ModelNamesSqlLiteEnum.CAMPODEJUEGOMEDIDA.toString());
                  
                  let DocumentoSincronizacion = respuesta.datosPdf.sincronizacion;
                  let base64ImagenCampoJuego = respuesta.datosPdf.campoDeJuegoMedidas != null && respuesta.datosPdf.campoDeJuegoMedidas.imagencampojuego != null? respuesta.datosPdf.campoDeJuegoMedidas.imagencampojuego:null;
                  let base64pdf = respuestaApi['pdf_image'];
        
                  //se envia la imagen de las medidas del campo de juegoa firebase
                  let urlImagenCampoJuego = null;
                  if(base64ImagenCampoJuego != null && base64ImagenCampoJuego.length > 0){
                     urlImagenCampoJuego= await this._almacenamientoArchivosFirebaseProvider.guardarFotoFirebase(base64ImagenCampoJuego,this._util.convertirFechaUnixtime(new Date()).toString());
                  }
                 
                  //se envia el pdf a firebase
                  let urlPdf = null;
                  if(base64pdf != null && base64pdf.length > 0){
                    urlPdf= await this._almacenamientoArchivosFirebaseProvider.guardarFotoFirebase(base64pdf,this._util.convertirFechaUnixtime(new Date()).toString(),"application/pdf");
                  }

                  //se actualiza el documento de firebase con las urls generadas.
                  DocumentoSincronizacion.urlImagenCampoJuego = urlImagenCampoJuego;
                  DocumentoSincronizacion.urlPdf = urlPdf;
                  await this._genericFirestoreProvider.updateRecordFirestore(DocumentoSincronizacion,"sincronizacion");
                   
                  await this._util.doAlert("Mensaje", "Datos enviados con éxito.").present();
                }
                else{
                  await this._util.doAlert("Mensaje", "Datos enviados con éxito, pero no fue posible enviar los correos.").present();
                } 
              } catch (error) {
                this._util.doAlert("Mensaje", 'Datos enviados con éxito, pero hubo un error.' + JSON.stringify(error)).present();
              }
              this.viewCtrl.dismiss().then(() => 
                 this.appCtrl.getRootNav().setRoot('TipoclubPage')
              );
            }
        }else{
          this._util.doAlert("Mensaje", 'Debe estar conectado a una red wifi para realizar la sincronización').present();
        }     
    } catch (error) {
      this._util.doAlert("Mensaje", 'No fue posible sincronizar los datos ' + error).present();
      console.error(error);
    }
    finally{
      loading.dismiss();
      loadingPdf.dismiss();
      loadingfileFirebase.dismiss();
    }

  }



  obtenerImgsBase64Array(arrayFotos: object[]) {

   let fotosObtenidas: Array<Foto> = [];
    if(arrayFotos != null && arrayFotos.length > 0){
      arrayFotos.forEach(element => {
        let imagenProperties: AttachmentImage = element['_attachments'];
        let fotoBase64: Foto = {
          foto: imagenProperties.imageProperties.data,
          nombreFoto: imagenProperties.name,
          esBase64: true
        }
        fotosObtenidas.push(fotoBase64);
      });
    }
    return fotosObtenidas;
  }


  
}
