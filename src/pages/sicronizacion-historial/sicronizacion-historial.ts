import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GenericFirestoreProvider } from '../../providers/genericfirestore/genericfirestore';
import { Club } from '../../modelos/Club';
import { UtilidadesProvider } from '../../providers/utilidades/utilidades';
import { Sincronizacion } from '../../modelos/Sincronizacion';
import { ModeloFirebase } from '../../modelos/ModeloFirebase';

/**
 * Generated class for the SicronizacionHistorialPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sicronizacion-historial',
  templateUrl: 'sicronizacion-historial.html',
})
export class SicronizacionHistorialPage {

  public club: Club; 
  public listaSicronizacion: Sincronizacion[] = [];

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public _genericFirestoreProvider:GenericFirestoreProvider,
              public _util: UtilidadesProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SicronizacionHistorialPage');
    this.club = this.navParams.get('paramclub');
    this.consultarHistorial();
  }

  async consultarHistorial() {

    let loader = this._util.presentarLoading("Cargando...");
    await loader.present();

    this.listaSicronizacion = [];
    console.log("this.club" , this.club);

    let data = await this._genericFirestoreProvider.findRecordPropertyFirestoreList('idclub',this.club.id,ModeloFirebase.SINCRONIZACION) as Sincronizacion[];
    if (data != null && data.length > 0) {
      this.listaSicronizacion = data;
      console.log(this.listaSicronizacion);
    } else {
      console.warn("Sin datos");
    }

    //se ordena por fecha.
   this.listaSicronizacion.sort(function (x, y) {
    return Number(y.fechasincronizacion) - Number(x.fechasincronizacion);
   });

   
   var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric',hour12: true ,hour:'numeric',minute:'numeric'};  

   //se castea la fecha
   for (let item of this.listaSicronizacion) {
    item['fechaMostrar'] = this._util.convertirUnixtimeFechaUnixtime(item.fechasincronizacion).toLocaleString("es-AR",options);
   }

   loader.dismiss();
  }


}
