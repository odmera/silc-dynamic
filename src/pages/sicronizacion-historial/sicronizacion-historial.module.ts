import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SicronizacionHistorialPage } from './sicronizacion-historial';

@NgModule({
  declarations: [
    SicronizacionHistorialPage,
  ],
  imports: [
    IonicPageModule.forChild(SicronizacionHistorialPage),
  ],
})
export class SicronizacionHistorialPageModule {}
