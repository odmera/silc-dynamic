import { Component } from '@angular/core';


import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage({
  name : 'tabPages'
})
@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tipoclub = 'TipoclubPage';
  buscar = 'BuscarPage';
  perfil = 'PerfilPage';

  constructor() {

  }
}
