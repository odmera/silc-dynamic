import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { Division } from '../../../modelos/Division';
import { Formulario } from '../../../modelos/Formulario';
import { GenericFirestoreProvider } from '../../../providers/genericfirestore/genericfirestore';
import { UtilidadesProvider } from '../../../providers/utilidades/utilidades';
import { ModeloFirebase } from '../../../modelos/ModeloFirebase';

/**
 * Generated class for the ModalFormulariosDivisionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-formularios-division',
  templateUrl: 'modal-formularios-division.html',
})
export class ModalFormulariosDivisionPage {

  private paramDivison: Division;
  public listaFormularios: Formulario[] = [];
  
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public _genericFirestoreProvider: GenericFirestoreProvider,
              public _util: UtilidadesProvider) {
     this.paramDivison = this.navParams.get("paramdivision");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalFormulariosDivisionPage');
    this.consultarFormularios();
  }


  async consultarFormularios() {
    let loader = this._util.presentarLoading("Cargando...");
    await loader.present();

    let listaFormulariosValidar:Formulario[] = [];

    let data = await this._genericFirestoreProvider.findRecordFirestore(ModeloFirebase.FORMULARIO);
    if (data != null && data.length > 0) {
      listaFormulariosValidar = data;
    } else {
      console.warn("Sin datos");
    }

    ///se ordenan los formularios
    for (let itemFormulario of listaFormulariosValidar) {
      if(itemFormulario.agrupaFormulario == true){ //agrupa otros formularios, se buscan sus formularios
        let nuevosFormulariosAgrupado:Formulario[] = [];
        for (let formAgrupado of itemFormulario.formulariosAgrupacion){          
          let formulaarioNuevoAgrupacion =  this.encontrarFormulario(listaFormulariosValidar,formAgrupado.id);
          nuevosFormulariosAgrupado.push(formulaarioNuevoAgrupacion);
        }
        //se reemplazo los formularios por los nuevos
        nuevosFormulariosAgrupado.sort(function(a, b) {return a.nombreFormulario > b.nombreFormulario? 1: b.nombreFormulario > a.nombreFormulario? -1: 0;});
        itemFormulario.formulariosAgrupacion= nuevosFormulariosAgrupado;
        this.listaFormularios.push(itemFormulario);
      }
      else{
        //se valida si el formulario esta agrupado en algun otro, si esta, no se agrega a la lista 
        let estaContenido:boolean = false
        for (let formularioValidacion of listaFormulariosValidar) {
          if(formularioValidacion.agrupaFormulario == true && 
            formularioValidacion.formulariosAgrupacion != null && 
            formularioValidacion.formulariosAgrupacion.length > 0){
            let formularioReEncontrado =  this.encontrarFormulario(formularioValidacion.formulariosAgrupacion,itemFormulario.id);
            if(formularioReEncontrado != null){
              estaContenido = true;
              break;
            }
          }
        }
        if(estaContenido == false){
          this.listaFormularios.push(itemFormulario);
        }
      }
    }

    await this.marcarFormularios(this.paramDivison.listaFormularios,this.listaFormularios);

    // Se ordena de manera alfabetica.
    this.listaFormularios.sort(function(a, b) {return a.nombreFormulario > b.nombreFormulario? 1: b.nombreFormulario > a.nombreFormulario? -1: 0;});

    loader.dismiss();

    console.info(this.listaFormularios);
  }


  async marcarFormularios(formulariosDivision:Formulario[],listaFormulariosMarcar:Formulario[]){

    //se marcan las formulario que ya se habian guardado
    if(formulariosDivision!= null && formulariosDivision != null){
      for (let itemFormulario of listaFormulariosMarcar) {
        if(itemFormulario.agrupaFormulario == true ){
          let itemFormularioDivisionAgrupa =  this.encontrarFormulario(formulariosDivision,itemFormulario.id);
          if(itemFormularioDivisionAgrupa != null){
            itemFormulario['visible'] = itemFormularioDivisionAgrupa['visible'];
            if(itemFormularioDivisionAgrupa.checked == true){
              itemFormulario.checked=true;
            }
          }

          if(itemFormulario.formulariosAgrupacion != null && itemFormulario.formulariosAgrupacion.length > 0){
            let formularioDivicionAgrupa =  this.encontrarFormulario(formulariosDivision,itemFormulario.id)
            if(formularioDivicionAgrupa != null && formularioDivicionAgrupa.agrupaFormulario ==true){
                await this.marcarFormularios(formularioDivicionAgrupa.formulariosAgrupacion,itemFormulario.formulariosAgrupacion);
            }
          }
        }
        else{
          let itemFormularioDivision =  this.encontrarFormulario(formulariosDivision,itemFormulario.id);
          if(itemFormularioDivision != null && itemFormularioDivision.checked == true){
            itemFormulario.checked=true;
          }
        }       
      }
    }

  }

  encontrarFormulario(dataFormularios:Formulario[],idformularioEncontrar:string){
    let formularioEncontrado:Formulario = null;
    if(dataFormularios != null && dataFormularios.length > 0){
      for (let item of dataFormularios) {
        if(item.id === idformularioEncontrar){
          formularioEncontrado = item
          break; 
        }
      }
    }
    return formularioEncontrado;
  }




  cerralModal() {
    this.viewCtrl.dismiss();
  }

  guardarFormularios() {
    let loader = this._util.presentarLoading("Grabando...");
    loader.present();

    if(this.listaFormularios != null && this.listaFormularios.length > 0){
      this.listaFormularios.forEach(elementPadre => {
        elementPadre.listaBloques = [];
        if(elementPadre.formulariosAgrupacion.length > 0){
          elementPadre.formulariosAgrupacion.forEach(elementHijo => {
            elementHijo.listaBloques = [];
            if(elementHijo.checked == true && elementPadre.checked == false){
              elementPadre.checked = true;
            }
          });
        }
      });
    }

    console.log( this.listaFormularios)

    this.paramDivison.listaFormularios = this.listaFormularios;

    this._genericFirestoreProvider.updateRecordFirestore(this.paramDivison,ModeloFirebase.DIVISION).then(
        respuesta => {
          this._util.presentarToast("Registro actualizado.");
          this.viewCtrl.dismiss({ guardo: true });
          loader.dismiss();
        },
        error => {
          loader.dismiss();
          this._util.presentarToast(error);
          console.log(error);
        }
      );
  }


}
