import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalFormulariosDivisionPage } from './modal-formularios-division';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  declarations: [
    ModalFormulariosDivisionPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalFormulariosDivisionPage),
    ComponentsModule
  ],
})
export class ModalFormulariosDivisionPageModule {}
