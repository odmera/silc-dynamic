import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Division } from '../../../modelos/Division';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { UtilidadesProvider } from '../../../providers/utilidades/utilidades';
import { GenericFirestoreProvider } from '../../../providers/genericfirestore/genericfirestore';
import { ModeloFirebase } from '../../../modelos/ModeloFirebase';
import { Formulario } from '../../../modelos/Formulario';
import { forEach } from '@firebase/util';

/**
 * Generated class for the ModalDivisionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-division',
  templateUrl: 'modal-division.html',
})
export class ModalDivisionPage {

  private guardarFormulario: boolean;
  private formulario: FormGroup;

  private paramtipooperacion: number;
  private paramDivison: Division;
  private tituloDialogo: string;
  private estoyEditando: boolean;

  public CREAR: number = 1;
  public EDITAR: number = 2;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private formBuilder: FormBuilder,
    public _util: UtilidadesProvider,
    public _genericFirestoreProvider: GenericFirestoreProvider) {

    this.paramtipooperacion = this.navParams.get("paramtipooperacion");
    this.paramDivison = this.navParams.get("paramdivision");
   
    this.tituloDialogo =
      this.paramtipooperacion == this.CREAR
        ? "Nueva división"
        : this.paramtipooperacion == this.EDITAR
          ? "Editar división"
          : null;


    this.estoyEditando =
      this.paramtipooperacion == this.CREAR
        ? false
        : this.paramtipooperacion == this.EDITAR
          ? true
          : false;

    this.guardarFormulario =
      this.paramtipooperacion == this.EDITAR ? true : false;

    this.formulario = formBuilder.group({
     

      txtNombre: [
        this.paramtipooperacion == this.CREAR
          ? ""
          : this.paramtipooperacion == this.EDITAR
            ? this.paramDivison.nombreDivision
            : null,
        Validators.required
      ],

      txtHabilitada: [
        this.paramtipooperacion == this.CREAR
          ? true
          : this.paramtipooperacion == this.EDITAR
            ? this.paramDivison.habilitado
            : null
      ],

      txtMostrarCancha: [
        this.paramtipooperacion == this.CREAR
          ? true
          : this.paramtipooperacion == this.EDITAR
            ? this.paramDivison.mostrarCancha
            : null
      ]

    });

    this.formulario.valueChanges.subscribe(v => {
      this.guardarFormulario = this.formulario.valid;
    });

  }

  ionViewDidLoad() {
  }

  

  guardar() {
    if (!this.formulario.valid) {
      return;
    }

    let loader = this._util.presentarLoading("Grabando...");
    loader.present();

    let Division: Division = {
      nombreDivision: this.formulario.value.txtNombre,
      habilitado: this.formulario.value.txtHabilitada,
      mostrarCancha: this.formulario.value.txtMostrarCancha,
      listaFormularios: null
    };

    this._genericFirestoreProvider.addRecordFirestore(Division,ModeloFirebase.DIVISION)
      .then(
        respuesta => {
          this._util.presentarToast("Registro almacenado.");
          this.viewCtrl.dismiss({ guardo: true });
          loader.dismiss();
        },
        error => {
          loader.dismiss();
          this._util.presentarToast(error);
          console.log(error);
        }
      );
  }

  editar() {
    if (!this.formulario.valid) {
      return;
    }

    let loader = this._util.presentarLoading("Grabando...");
    loader.present();

    this.paramDivison.nombreDivision = this.formulario.value.txtNombre;
    this.paramDivison.habilitado = this.formulario.value.txtHabilitada;
    this.paramDivison.mostrarCancha = this.formulario.value.txtMostrarCancha;

    this._genericFirestoreProvider.updateRecordFirestore(this.paramDivison,ModeloFirebase.DIVISION).then(
        respuesta => {
          this._util.presentarToast("Registro actualizado.");
          this.viewCtrl.dismiss({ guardo: true });
          loader.dismiss();
        },
        error => {
          loader.dismiss();
          this._util.presentarToast(error);
          console.log(error);
        }
      );
  }

  cerralModal() {
    this.viewCtrl.dismiss();
  }



}
