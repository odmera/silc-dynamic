import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalDivisionPage } from './modal-division';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  declarations: [
    ModalDivisionPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalDivisionPage)
  ],
})
export class ModalDivisionPageModule {}
