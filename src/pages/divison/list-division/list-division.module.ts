import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListDivisionPage } from './list-division';

@NgModule({
  declarations: [
    ListDivisionPage,
  ],
  imports: [
    IonicPageModule.forChild(ListDivisionPage),
  ],
})
export class ListDivisionPageModule {}
