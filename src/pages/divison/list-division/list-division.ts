import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { Division } from '../../../modelos/Division';
import { UtilidadesProvider } from '../../../providers/utilidades/utilidades';
import { GenericFirestoreProvider } from '../../../providers/genericfirestore/genericfirestore';
import { ModeloFirebase } from '../../../modelos/ModeloFirebase';


@IonicPage()
@Component({
  selector: 'page-list-division',
  templateUrl: 'list-division.html',
})
export class ListDivisionPage {

  listaDivisiones: Division[] = [];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public _util: UtilidadesProvider,
    public _genericFirestoreProvider :GenericFirestoreProvider) {
  }

  ionViewDidLoad() {
    this.consultarDivisiones();
  }

  async consultarDivisiones() {

    let loader = this._util.presentarLoading("Cargando...");
    await loader.present();

    this.listaDivisiones = [];

    let data = await this._genericFirestoreProvider.findRecordFirestore(ModeloFirebase.DIVISION);
    if (data != null && data.length > 0) {
      this.listaDivisiones = data;
    } else {
      console.warn("Sin datos");
    }

    //se ordena de manera alfabetica.
    this.listaDivisiones.sort(function(a,b) {return (a.nombreDivision > b.nombreDivision) ? 1 : ((b.nombreDivision > a.nombreDivision) ? -1 : 0);} );
    loader.dismiss();
  }

  mostrarDialogo(tipoOperacion: number, item: Division) {
  
    let miModal = this.modalCtrl.create('ModalDivisionPage',
    {
        paramtipooperacion: tipoOperacion,
        paramdivision: item
    });
    miModal.present();

    //esta funcion se ejecuta cuando cierro el modal
    miModal.onDidDismiss(parametros => {
      if (parametros != null) {
        let loader = this._util.presentarLoading("Cargando...");
        loader.present().then(() => {
          this.consultarDivisiones().then(() => {
            loader.dismiss();
          });
        })
      }
    })
  }


  mostrarFormulariosDinamicos(item: Division){
    let miModal = this.modalCtrl.create('ModalFormulariosDivisionPage',
    {
        paramdivision: item
    });
    miModal.present();

    //esta funcion se ejecuta cuando cierro el modal
    miModal.onDidDismiss(parametros => {
      if (parametros != null) {
        let loader = this._util.presentarLoading("Cargando...");
        loader.present().then(() => {
          this.consultarDivisiones().then(() => {
            loader.dismiss();
          });
        })
      }
    })
  }

}
