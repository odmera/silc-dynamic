import {Component} from "@angular/core";
import {NavController, AlertController, ToastController, MenuController, IonicPage, Events} from "ionic-angular";
import {HomePage} from "../home/home";
import { UtilidadesProvider } from "../../providers/utilidades/utilidades";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuntenticacionFirebaseProvider } from "../../providers/auntenticacion-firebase/auntenticacion-firebase";

import 'rxjs/add/operator/first';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { datosAL } from "../../modelos/datosAL";
import { UsuarioProvider } from "../../providers/usuario/usuario";
import { AlmacenamientoLocalProvider } from "../../providers/almacenamiento-local/almacenamiento-local";
import { PerfilUsuario } from "../../modelos/UsuarioDTO";
import { SincronizarDatosProvider } from "../../providers/sincronizar-datos/sincronizar-datos";


@IonicPage() 
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  formularioLogin: FormGroup;

  constructor(public nav: NavController, 
              public forgotCtrl: AlertController, 
              public menu: MenuController, 
              public toastCtrl: ToastController,
              public formBuilder: FormBuilder,
              public _auntenticacionFirebaseProvider: AuntenticacionFirebaseProvider,
              public _util: UtilidadesProvider,
              public _usuarioProvider: UsuarioProvider,
              public _almacenamientoLocalProvider: AlmacenamientoLocalProvider,
              private _sincronizarDatosProvider: SincronizarDatosProvider,
              public events: Events) {
    this.menu.swipeEnable(false);

    this.formularioLogin = this.formBuilder.group({
      txtCorreo: ['', Validators.compose([Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'), Validators.required])],
      txtClave: ['', Validators.compose([Validators.required])],
    });
  }

 
  login() {

    let correo: string = this.formularioLogin.value.txtCorreo;
    let clave: string = this.formularioLogin.value.txtClave;

    let loader = this._util.presentarLoading("Iniciando...");
    loader.present();

    this.iniciarSesionConEmail(correo, clave).then(() => {
      loader.dismiss();
    },
      (error) => {
        loader.dismiss();
        this._util.doAlert("Mensaje", error).present()
        console.error(error);
      })

  }

  async iniciarSesionConEmail(correo: string, clave: string): Promise<void> {

    try {
      //se autentica el usuario en firebase con email
      let sesionCorrecta: boolean = await this._auntenticacionFirebaseProvider.iniciarSesionConEmail(correo, clave);

      if (sesionCorrecta == true){
        //consulto el usario autenticado para guardar los datos en la sesion.
        let itemUsuario = await this._usuarioProvider.consultarUsuarioPorEmail(correo).first().toPromise();
        if (itemUsuario == null || itemUsuario.length == 0) {
          throw ("No se encontro el usuario");
        }

        let usuarioLogin = itemUsuario[0];
        //guardo los datos en la sesion
        let datosal = new datosAL()
        datosal.idUsuario = usuarioLogin.id;
        datosal.correoUsuario = usuarioLogin.correo;
        datosal.perfilUsuario = usuarioLogin.perfil;
        datosal.nombreCompleto = usuarioLogin.nombreCompleto;
        let almacenamientoCorrecto: boolean = await this._almacenamientoLocalProvider.guardarDatosAL(datosal);

        if (!almacenamientoCorrecto) {
          throw ("Con el almacenamiento local");
        }
      
        if (usuarioLogin.perfil == PerfilUsuario._pefil_usuario.toString()) {
          this.nav.setRoot('TipoclubPage');
        }
        else if (usuarioLogin.perfil == PerfilUsuario._pefil_admin.toString()) {
          this.nav.setRoot('TipoclubPage');
        }
       
        if(this._util.isApp() == true){
          try {
            await this._sincronizarDatosProvider.obtenerDatosDelServidor();
            //se emite el evento para que se realice la consulta justo despues de tener las diviciones listas en PB
            this.events.publish('division:consulta',true);
          } catch (error) {
            this._util.doAlert("Mensaje","No se obtuvieron los datos del servidor " + error).present()
          }
        }

      }
      
    } catch (error) {
      throw new Error(error);
    }

  }

  forgotPass() {
    let forgot = this.forgotCtrl.create({
      title: '¿Se te olvidó tu contraseña?',
      message: "Ingrese su dirección de correo electrónico para enviar un enlace.",
      inputs: [
        {
          name: 'correo',
          placeholder: 'Correo Electrónico',
          type: 'email'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Enviar',
          handler: data => {

            this._auntenticacionFirebaseProvider.restrablecerContrasena(data.correo).
            then((res : boolean) =>{
              if(res == true){
                this._util.presentarToast("El correo electrónico fue enviado con éxito.");
              }
            }).catch(() =>{
              this._util.presentarToast("Error, verifique el correo electrónico.");
            });  
          }
        }
      ]
    });
    forgot.present();
  }

}
