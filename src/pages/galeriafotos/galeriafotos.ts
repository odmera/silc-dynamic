import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Foto } from '../../modelos/Foto';
import { ImageViewerController } from 'ionic-img-viewer';
import { UtilidadesProvider } from '../../providers/utilidades/utilidades';

/**
 * Generated class for the GaleriafotosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-galeriafotos',
  templateUrl: 'galeriafotos.html',
})
export class GaleriafotosPage {

  public listImagen: Array<Foto> = [];
  public grid = true;
  public nombrePregunta:string = null;
  _imageViewerCtrl: ImageViewerController;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public imageViewerCtrl: ImageViewerController,
              public viewCtrl: ViewController,
              public _util: UtilidadesProvider) {

      this._imageViewerCtrl = imageViewerCtrl;

      this.listImagen = this.navParams.get('imagenes');
      this.nombrePregunta = this.navParams.get('nombrePregunta');
      console.log( this.nombrePregunta)

      for (let item of this.listImagen ) {
        let base64data = 'data:image/jpeg;base64,' + item.foto;
        let bigSize = this._util.getImageSize(base64data);
        //alert("tamaño " + bigSize + " MB");
      }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GaleriafotosPage');
  }

  changeGrid(){
    this.grid = !this.grid;
  }

  mostrarImagenPantallaCompleta(myImage){
    const imageViewer = this._imageViewerCtrl.create(myImage);
    imageViewer.present();
  }

  cerralModal() {
    this.viewCtrl.dismiss();
  }

  eliminarImagen(idx){
    this.listImagen.splice(idx, 1); //cuando se elimina aca se elimina en el formulario
    this.viewCtrl.dismiss({ index: idx });
  }

}
