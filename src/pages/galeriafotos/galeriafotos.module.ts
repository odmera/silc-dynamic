import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GaleriafotosPage } from './galeriafotos';

@NgModule({
  declarations: [
    GaleriafotosPage,
  ],
  imports: [
    IonicPageModule.forChild(GaleriafotosPage),
  ],
})
export class GaleriafotosPageModule {}
