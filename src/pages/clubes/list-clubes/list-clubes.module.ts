import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListClubesPage } from './list-clubes';

@NgModule({
  declarations: [
    ListClubesPage
  ],
  imports: [
    IonicPageModule.forChild(ListClubesPage),
  ],
})
export class ListClubesPageModule {}
