import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { UtilidadesProvider } from '../../../providers/utilidades/utilidades';
import { Club } from '../../../modelos/Club';
import { ClubProvider } from '../../../providers/club/club';

@IonicPage()
@Component({
  selector: 'page-list-clubes',
  templateUrl: 'list-clubes.html',
})
export class ListClubesPage {

  listaClubes: Club[] = [];
  listaClubesSearchBar: Club[];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public _util: UtilidadesProvider,
    public _clubProvider: ClubProvider) {
  }

  ionViewDidLoad() {
    
  }
  ionViewDidEnter() {
    this.consultarClubes();
  }

  initializeItems() {
    this.listaClubesSearchBar = this.listaClubes;
  }


  getItems(searchbar) {

    this.initializeItems();
    let equipo = searchbar.target.value;

    if (equipo && equipo.trim() != '') {
      this.listaClubesSearchBar = this.listaClubesSearchBar.filter((item) => {
        return (item.nombreCompleto.toLowerCase().indexOf(equipo.toLowerCase()) > -1);
      })
    } 
  }

  async consultarClubes() {

    let loader = this._util.presentarLoading("Cargando...");
    await loader.present();

    this.listaClubes = [];

    let data = await this._clubProvider.consultarClubes()
    if (data != null && data.length > 0) {
      this.listaClubes = data;
    } else {
      console.warn("Sin datos");
    }

    //se ordena de manera alfabetica.
   this.listaClubes.sort(function(a,b) {return (a.nombreCompleto > b.nombreCompleto) ? 1 : ((b.nombreCompleto > a.nombreCompleto) ? -1 : 0);} );
   this.initializeItems();

   loader.dismiss();
  }

  mostrarDialogo(tipoOperacion: number, item: Club) {
    console.log(Club);

    let miModal = this.modalCtrl.create('ModalClubPage',
    {
        paramtipooperacion: tipoOperacion,
        paramclub: item
    });
    miModal.present();

    //esta funcion se ejecuta cuando cierro el modal
    miModal.onDidDismiss(parametros => {
      if (parametros != null) {
        let loader = this._util.presentarLoading("Cargando...");
        loader.present().then(() => {
          this.consultarClubes().then(() => {
            loader.dismiss();
          });
        })
      }
    })
  }


  mostrarHistorial(item: Club){
    this.navCtrl.push('SicronizacionHistorialPage',{paramclub: item}) 
  }

}
