import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalClubPage } from './modal-club';

@NgModule({
  declarations: [
    ModalClubPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalClubPage),
  ],
})
export class ModalClubPageModule {}
