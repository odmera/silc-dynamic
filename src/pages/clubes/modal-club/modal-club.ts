import { Component, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Club } from "../../../modelos/Club";
import { UtilidadesProvider } from "../../../providers/utilidades/utilidades";
import { ClubProvider } from "../../../providers/club/club";
import { Division } from "../../../modelos/Division";
import { DivisionProvider } from "../../../providers/division/division";
import { Ng2ImgMaxService } from 'ng2-img-max';
import { DomSanitizer } from '@angular/platform-browser';

@IonicPage()
@Component({
  selector: "page-modal-club",
  templateUrl: "modal-club.html"
})
export class ModalClubPage {
  private guardarFormulario: boolean;
  private formularioClub: FormGroup;

  private imagenFirebase: string = null;
  private imagenFondoFirebase: string = null;

  @ViewChild("fileInput") fileInput;
  @ViewChild("fileInputFondo") fileInputFondo;

  private paramtipooperacion: number;
  private paramClub: Club;
  private tituloDialogo: string;
  private estoyEditando: boolean;

  public CREAR: number = 1;
  public EDITAR: number = 2;

  public lstDivisiones: Division[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private formBuilder: FormBuilder,
    public _util: UtilidadesProvider,
    public _clubProvider: ClubProvider,
    public _divisionProvider: DivisionProvider,
    private ng2ImgMax: Ng2ImgMaxService,
    public sanitizer: DomSanitizer
  ) {
    this.paramtipooperacion = this.navParams.get("paramtipooperacion");
    this.paramClub = this.navParams.get("paramclub");

    this.tituloDialogo =
      this.paramtipooperacion == this.CREAR
        ? "Nuevo club"
        : this.paramtipooperacion == this.EDITAR
          ? "Editar club"
          : null;

    this.estoyEditando =
      this.paramtipooperacion == this.CREAR
        ? false
        : this.paramtipooperacion == this.EDITAR
          ? true
          : false;

    this.guardarFormulario =
      this.paramtipooperacion == this.EDITAR ? true : false;

    this.formularioClub = formBuilder.group({
      foto: [
        this.paramtipooperacion == this.CREAR
          ? ""
          : this.paramtipooperacion == this.EDITAR
            ? this.paramClub.foto
            : null
      ],

      fotoFondo: [
        this.paramtipooperacion == this.CREAR
          ? ""
          : this.paramtipooperacion == this.EDITAR
            ? this.paramClub.fotoFondo
            : null
      ],

      txtNombre: [
        this.paramtipooperacion == this.CREAR
          ? ""
          : this.paramtipooperacion == this.EDITAR
            ? this.paramClub.nombreCompleto
            : null,
        Validators.required
      ],

      txtLiga: [
        this.paramtipooperacion == this.CREAR
          ? ""
          : this.paramtipooperacion == this.EDITAR
            ? this.paramClub.tipoClub
            : null,
        Validators.required
      ],

      txtdireccionSedeAdmin: [
        this.paramtipooperacion == this.CREAR
          ? ""
          : this.paramtipooperacion == this.EDITAR
            ? this.paramClub.direccionSedeAdmin
            : null,
        Validators.required
      ],

      txtdireccionEstadio: [
        this.paramtipooperacion == this.CREAR
          ? ""
          : this.paramtipooperacion == this.EDITAR
            ? this.paramClub.direccionEstadio
            : null,
        Validators.required
      ],

      txtdireccionCampoDeportes: [
        this.paramtipooperacion == this.CREAR
          ? ""
          : this.paramtipooperacion == this.EDITAR
            ? this.paramClub.direccionCampoDeportes
            : null,
        Validators.required
      ],

      txtnombrePresidente: [
        this.paramtipooperacion == this.CREAR
          ? ""
          : this.paramtipooperacion == this.EDITAR
            ? this.paramClub.nombrePresidente
            : null,
        Validators.required
      ],

      txtresponsableLicencias: [
        this.paramtipooperacion == this.CREAR
          ? ""
          : this.paramtipooperacion == this.EDITAR
            ? this.paramClub.responsableLicencias
            : null,
        Validators.required
      ],

      txtcapacidadestadio: [
        this.paramtipooperacion == this.CREAR
          ? null
          : this.paramtipooperacion == this.EDITAR
            ? this.paramClub.capacidadestadio
            : null,
        Validators.required
      ],

      txtHabilitada: [
        this.paramtipooperacion == this.CREAR
          ? true
          : this.paramtipooperacion == this.EDITAR
            ? this.paramClub.habilitado
            : null
      ]
    });

    this.formularioClub.valueChanges.subscribe(v => {
      this.guardarFormulario = this.formularioClub.valid;
    });
  }

  ionViewDidLoad() {
    let loader = this._util.presentarLoading("Cargando...");
    loader.present().then(() => {
      this.consultarDivisiones();
      loader.dismiss();
    });
  }

  guardar() {
    if (!this.formularioClub.valid) {
      return;
    }

    if (this.imagenFirebase == null) {
      this._util.presentarToast("La foto del club es obligatoria.");
      return;
    }

    let loader = this._util.presentarLoading("Grabando...");
    loader.present();

    let Club: Club = {
      nombreCompleto: this.formularioClub.value.txtNombre,
      tipoClub: this.formularioClub.value.txtLiga,
      habilitado: this.formularioClub.value.txtHabilitada,
      direccionSedeAdmin: this.formularioClub.value.txtdireccionSedeAdmin,
      direccionEstadio: this.formularioClub.value.txtdireccionEstadio,
      direccionCampoDeportes: this.formularioClub.value
        .txtdireccionCampoDeportes,
      nombrePresidente: this.formularioClub.value.txtnombrePresidente,
      responsableLicencias: this.formularioClub.value.txtresponsableLicencias,
      capacidadestadio:this.formularioClub.value.txtcapacidadestadio,
      foto: null,
      nombreFoto: null,
      fotoFondo: null,
      nombreFotoFondo: null,
      fecha: Number(this._util.convertirFechaUnixtime(new Date())),
      _attachments: {
        "att.txt": {
          content_type: "text/plain",
          data: this.imagenFirebase
        },
        "attFondo.txt": {
          content_type: "text/plain",
          data: this.imagenFondoFirebase === null || this.imagenFondoFirebase.length == 0 ? "" :
          this.imagenFondoFirebase //si esta null se debe guardar vacio
        }
      }
    };

    this._clubProvider
      .crearActualizarClub(Club, this.imagenFirebase, this.imagenFondoFirebase)
      .then(
        respuesta => {
          this._util.presentarToast("Registro almacenado.");
          this.viewCtrl.dismiss({ guardo: true });
          loader.dismiss();
        },
        error => {
          loader.dismiss();
          this._util.presentarToast(error);
          console.log(error);
        }
      );
  }

  editar() {
    if (!this.formularioClub.valid) {
      return;
    }

    let loader = this._util.presentarLoading("Grabando...");
    loader.present();

    this.paramClub.nombreCompleto = this.formularioClub.value.txtNombre;
    this.paramClub.habilitado = this.formularioClub.value.txtHabilitada;
    this.paramClub.tipoClub = this.formularioClub.value.txtLiga;
    this.paramClub.direccionSedeAdmin = this.formularioClub.value.txtdireccionSedeAdmin;
    this.paramClub.direccionEstadio = this.formularioClub.value.txtdireccionEstadio;
    (this.paramClub.direccionCampoDeportes = this.formularioClub.value.txtdireccionCampoDeportes),
      (this.paramClub.nombrePresidente = this.formularioClub.value.txtnombrePresidente);
    this.paramClub.responsableLicencias = this.formularioClub.value.txtresponsableLicencias;
    this.paramClub.capacidadestadio = this.formularioClub.value.txtcapacidadestadio;
    this.paramClub.fecha = Number(this._util.convertirFechaUnixtime(new Date()) );

    if (this.imagenFirebase != null) {
        this.paramClub.nombreFoto = this.paramClub.nombreFoto == undefined || 
        this.paramClub.nombreFoto == null ||
        this.paramClub.nombreFoto == "" ? 
        this.paramClub.id : this.paramClub.nombreFoto;
        this.paramClub._attachments['att.txt'].data = this.imagenFirebase; 
        this.paramClub._attachments['att.txt'].content_type = 'text/plain';         
    }
    if (this.imagenFondoFirebase != null) {
        this.paramClub.nombreFotoFondo = this.paramClub.nombreFotoFondo == undefined || 
                                    this.paramClub.nombreFotoFondo == null ||
                                    this.paramClub.nombreFotoFondo == "" ? 
                                    this.paramClub.id : this.paramClub.nombreFotoFondo;                   
        this.paramClub._attachments['attFondo.txt'].data = this.imagenFondoFirebase; 
        this.paramClub._attachments['attFondo.txt'].content_type = 'text/plain'; 
    }


    this._clubProvider
      .crearActualizarClub(
        this.paramClub,
        this.imagenFirebase,
        this.imagenFondoFirebase
      )
      .then(
        respuesta => {
          this._util.presentarToast("Registro actualizado.");
          this.viewCtrl.dismiss({ guardo: true });
          loader.dismiss();
        },
        error => {
          loader.dismiss();
          this._util.presentarToast(error);
          console.log(error);
        }
      );
  }

  cerralModal() {
    this.viewCtrl.dismiss();
  }

  seleccionarImagenGallery(tipoImagen:number) {
    console.info(tipoImagen);
    if(tipoImagen == 1){
      this.fileInput.nativeElement.click();
    } else {
      this.fileInputFondo.nativeElement.click();
    }
  }

  async processWebImage(file, tipoImagen: number) {

    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = readerEvent => {
      let loader = this._util.presentarLoading("");
      loader.present().then(() => {
        // 1. Logo, 2. Fondo
        if (tipoImagen == 1) {
          this.imagenFirebase = reader.result.split(",")[1]; //quito la cadena data:image/jpg;base64,
          let imageData = (readerEvent.target as any).result; // bien con la cadena data:image/jpg;base64,
          this.formularioClub.patchValue({ foto: imageData});
        } else {
          this.imagenFondoFirebase = reader.result.split(",")[1]; //quito la cadena data:image/jpg;base64,
          let imageFondoData = (readerEvent.target as any).result; // bien con la cadena data:image/jpg;base64,
          this.formularioClub.patchValue({ fotoFondo: imageFondoData });
        }
        loader.dismiss();
      });
    };
  }

  getProfileImageStyle(tipoImagen: number) {
    if (tipoImagen == 1) {
      return this.sanitizer.bypassSecurityTrustStyle(`url('${this.formularioClub.controls["foto"].value}')`);
    } else {
      return this.sanitizer.bypassSecurityTrustStyle(`url('${this.formularioClub.controls["fotoFondo"].value}')`);
    }
  }

  async consultarDivisiones() {
    this.lstDivisiones = [];
    let data = await this._divisionProvider.consultarDivisiones();
    if (data != null && data.length > 0) {
      this.lstDivisiones = data;
      this.lstDivisiones.sort(function(a,b) {return (a.nombreDivision > b.nombreDivision) ? 1 : ((b.nombreDivision > a.nombreDivision) ? -1 : 0);} );
    } else {
      console.warn("No hay categorias");
    }
  }


  onImageChange(event,tipoImagen: number) {
    
      let image = event.target.files[0];
      console.log("image" , image);
        if(image != null){
          let loader = this._util.presentarLoading("Ajustando imagen...");
          loader.present().then(() => {
            this.ng2ImgMax.resizeImage(image, 900, 900).subscribe(
              result => {
                let uploadedImage = new File([result], result.name);
                loader.dismiss();
                this.processWebImage(uploadedImage,tipoImagen);
              },
              error => {
                loader.dismiss();
                console.log('😢 Oh no!', error);
              }
            );
          })
        }
      
  }

  getImagePreview(file: File) {
    const reader: FileReader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = readerEvent => {
      //this.imagenFirebase = reader.result;

      //this.imagenFirebase = readerEvent.result.split(",")[1];
      let imageFondoData = (readerEvent.target as any).result;
      this.formularioClub.patchValue({ foto: imageFondoData});


      let bigSize = this._util.getImageSize(imageFondoData);
      console.log(bigSize);
      this._util.doAlert("Detalle de imagen ","tamaño imagen " + bigSize + "MB").present();
    };
  }

}
