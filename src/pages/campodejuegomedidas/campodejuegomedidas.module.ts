import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CampodejuegomedidasPage } from './campodejuegomedidas';

@NgModule({
  declarations: [
    CampodejuegomedidasPage,
  ],
  imports: [
    IonicPageModule.forChild(CampodejuegomedidasPage),
  ],
})
export class CampodejuegomedidasPageModule {}
