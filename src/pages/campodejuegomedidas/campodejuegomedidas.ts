import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CampoDeJuegoMedidas } from '../../modelos/CampoDeJuegoMedidas';
import { AlmacenamientoLocalProvider } from '../../providers/almacenamiento-local/almacenamiento-local';
import { GenericpouchdbProvider } from '../../providers/genericpouchdb/genericpouchdb';
import { GenericFirestoreProvider } from '../../providers/genericfirestore/genericfirestore';
import { Club } from '../../modelos/Club';
import { ModelNamesSqlLiteEnum } from '../../modelos/enum/ModelNamesSqlLiteEnum';
import { UtilidadesProvider } from '../../providers/utilidades/utilidades';
import { ModeloFirebase } from '../../modelos/ModeloFirebase';

import * as html2canvas from "html2canvas"

/**
 * Generated class for the CampodejuegomedidasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-campodejuegomedidas',
  templateUrl: 'campodejuegomedidas.html',
})
export class CampodejuegomedidasPage {

  campoDeJuegoMedidas={} as CampoDeJuegoMedidas;
  private club: Club;
  // Id usado por PouchDb
  private _revPouchDb: string;
  private idfirestore: string = null;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public _almacenamientoLocalProvider: AlmacenamientoLocalProvider,
              public _genericPouchdbProvider: GenericpouchdbProvider,
              public _genericFirestoreProvider: GenericFirestoreProvider,
              public _util: UtilidadesProvider) {

    this.club = this.navParams.get('club');
    this._revPouchDb = null;
    console.log("this.club " , this.club);

    // Nombre de la db para el almacenamiento local
    this._genericPouchdbProvider.constructorToObjectDb(ModelNamesSqlLiteEnum.CAMPODEJUEGOMEDIDA.toString());

  }

   // Almacenar informacion de las medidas del campo de juego
   async guardarMedidasCampoJuego() {
  
    let loader = this._util.presentarLoading("Grabando...");
    await loader.present();

    try {

      let datosAl = await this._almacenamientoLocalProvider.obtenerDatosSesion();
      if (datosAl != null && datosAl.idUsuario != null) {

        this.campoDeJuegoMedidas.id=this.idfirestore;
        this.campoDeJuegoMedidas._rev=this._revPouchDb;
        this.campoDeJuegoMedidas._id=this.club.id;
        this.campoDeJuegoMedidas.idclub=this.club.id;
        this.campoDeJuegoMedidas.nombreclub= this.club.nombreCompleto;
        this.campoDeJuegoMedidas.modelnamesqlite= ModelNamesSqlLiteEnum.CAMPODEJUEGOMEDIDA.toString(),
        this.campoDeJuegoMedidas.usuarioMail= datosAl.correoUsuario,
        this.campoDeJuegoMedidas.fecha= this._util.convertirFechaUnixtime(new Date());

        if (this._util.isApp() == true) {
          //solo se guarda la imagen del campo de juego en el movil
          this.campoDeJuegoMedidas.imagencampojuego =  await this.capturarImagenCampoJuego();
          let respuesta = await this._genericPouchdbProvider.putRecord(this.campoDeJuegoMedidas);
          this._revPouchDb = respuesta['rev'];
        }
        else {
          this.idfirestore = await this._genericFirestoreProvider.addOrUpdateRecordFirestore(this.campoDeJuegoMedidas, ModeloFirebase.CAMPODEJUEGOMEDIDA);
        }

        await this._util.doAlert("Mensaje", "Registro almacenado.").present();
        setTimeout(() => {
          this.navCtrl.pop();
        }, 3000);
      }
      else {
        this._util.presentarToast("Los datos del usuario no se han encontrado.");
      }
    } catch (error) {
      this._util.doAlert("Mensaje", error).present();
      console.log(error);
    } finally {
      loader.dismiss();
    }

  }

 async capturarImagenCampoJuego(){

    //se recojen todos los inputs de la vista y ajustan algunas propiedades 
    let inputs = document.querySelectorAll("ion-input input");
    [].forEach.call(inputs, function(input) {
       input.setAttribute("style", "padding-top: 8px;");
       input.setAttribute("placeholder", ""); 
    });
   
    //generando la captura
    let  canvas = await html2canvas(document.getElementById("campojuego"));
    return canvas.toDataURL();

    /*var image = new Image();
    image.src = canvas.toDataURL();
    var w = window.open("");
    w.document.write(image.outerHTML)*/
    
 
  }


  async consultarMedidasCamposJuego() {
    try {
     
      // Consulta datos de Pdb
      if (this._util.isApp() == true) {
        this.campoDeJuegoMedidas = await this._genericPouchdbProvider.getRecord(this.club.id) as CampoDeJuegoMedidas;
      } else {
        // Consulta datos de Firebase
        this.campoDeJuegoMedidas = await this._genericFirestoreProvider.findRecordPropertyFirestore("idclub", this.club.id, ModeloFirebase.CAMPODEJUEGOMEDIDA);
      }

      // Se cargan los datos al formulario
      if (this.campoDeJuegoMedidas != null) {
        this._revPouchDb = this.campoDeJuegoMedidas._rev;
        this.idfirestore = this.campoDeJuegoMedidas.id;
      }
      else {
        this.campoDeJuegoMedidas={} as CampoDeJuegoMedidas;
        console.info('No existen registros para este Equipo');
      }

    } catch (error) {
      console.info('No existen registros para este Equipo', error);
    }

  }

  ionViewDidLoad() {
    this.consultarMedidasCamposJuego();
  }

}
