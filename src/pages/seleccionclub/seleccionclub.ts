import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Club } from '../../modelos/Club';
import { ModelNamesSqlLiteEnum } from '../../modelos/enum/ModelNamesSqlLiteEnum';
import { UtilidadesProvider } from '../../providers/utilidades/utilidades';
import { GenericpouchdbProvider } from '../../providers/genericpouchdb/genericpouchdb';
import { Division } from '../../modelos/Division';
import { GenericFirestoreProvider } from '../../providers/genericfirestore/genericfirestore';
import { ModeloFirebase } from '../../modelos/ModeloFirebase';

/**
 * Generated class for the SeleccionclubPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-seleccionclub',
  templateUrl: 'seleccionclub.html',
})
export class SeleccionclubPage {

  listaClubesSearchBar: Club[];
  private listaCublesPorTipo: Club[]= [];
  private division:Division;
  

  //Se debe de cambiar este metodo para luego cargue los equipos dependiendo su division
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public _util: UtilidadesProvider,
              public _genericPouchdbProvider: GenericpouchdbProvider,
              public _genericFirestoreProvider: GenericFirestoreProvider,) {

    this.division = this.navParams.get('division');
    console.log("division " , this.division );
  }

  ionViewDidLoad() {
      if(this.division != null && this.division.id != null){
        this.consultarClubes();
      } 
  }

  initializeItems() {
    this.listaClubesSearchBar = this.listaCublesPorTipo;
  }

  getItems(searchbar) {

    this.initializeItems();

    let equipo = searchbar.target.value;

    if (equipo && equipo.trim() != '') {
      this.listaClubesSearchBar = this.listaClubesSearchBar.filter((item) => {
        return (item.nombreCompleto.toLowerCase().indexOf(equipo.toLowerCase()) > -1);
      })
    } 

  }

  menuClub(club:Club){
    console.log('Menu Club de Zona division para el equipo: '+club.nombreCompleto);
    this.navCtrl.push('MenucriterioPage', { 'club': club,'division': this.division } );
  }

  async consultarClubes(){

   let loader = this._util.presentarLoading("Cargando...");
   await loader.present();
   try {
      let isApp = this._util.isApp();
      let listaCubles: Club[]= [];
      if(isApp == true){

        // Nombre de la db para el almacenamiento local
        await this._genericPouchdbProvider.constructorToObjectDb(ModelNamesSqlLiteEnum.CLUB.toString());

        //se crea el indice ['tipoClub']
        let indexclubtipoclub = await this._genericPouchdbProvider.createIndex('indexclubtipoclub',['tipoClub']);
        console.log("indexclubtipoclub " , indexclubtipoclub);

        let datos = await this._genericPouchdbProvider.findRecordPropertys({tipoClub: this.division.id},'indexclubtipoclub')
        listaCubles = datos as Club[];
    
        if(listaCubles != null && listaCubles.length >0){
          for (let item of listaCubles ) {
            //if(item._attachments != null && item._attachments["att.txt"] != null && item._attachments["att.txt"].data != null){
              //item['imagenmostrar'] = "data:image/png;base64,"+ item._attachments["att.txt"].data;
              item['imagenmostrar'] = item.foto;
              //console.log("Imagen " + item['imagenmostrar']);
            //}
          }
        }
        console.log("this.listaCubles PB " , listaCubles );
      }
      else{
        listaCubles = await this._genericFirestoreProvider.findRecordPropertyFirestoreList('tipoClub',this.division.id,ModeloFirebase.CLUB) as Club[];
        console.log("this.listaCubles firebase " , listaCubles );
        if(listaCubles != null && listaCubles.length >0){
          for (let item of listaCubles ) {
            item['imagenmostrar'] = item.foto;
          }
        }
      }
  
      //se filtran los clubles por tipo
      this.listaCublesPorTipo =listaCubles;

      if(this.listaCublesPorTipo != null && this.listaCublesPorTipo.length > 0){
        //se ordena de manera alfabetica.
        this.listaCublesPorTipo.sort(function(a,b) {return (a.nombreCompleto > b.nombreCompleto) ? 1 : ((b.nombreCompleto > a.nombreCompleto) ? -1 : 0);} );
        console.log("this.listaCublesPorTipo " , this.listaCublesPorTipo);
      }
      this.initializeItems();
  
     
   } catch (error) {
     this._util.doAlert('','Ocurrio algo obteniendo los cubles ' + error).present();
   }
   finally{
    loader.dismiss();
   }
  
  }

}
