import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SeleccionclubPage } from './seleccionclub';

@NgModule({
  declarations: [
    SeleccionclubPage,
  ],
  imports: [
    IonicPageModule.forChild(SeleccionclubPage),
  ],
})
export class SeleccionclubPageModule {}
