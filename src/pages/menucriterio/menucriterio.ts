import { Component, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  AlertController
} from "ionic-angular";

// Paginas de criterios
import { Club } from "../../modelos/Club";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { UtilidadesProvider } from "../../providers/utilidades/utilidades";
import { AlmacenamientoLocalProvider } from "../../providers/almacenamiento-local/almacenamiento-local";
import { GenericpouchdbProvider } from "../../providers/genericpouchdb/genericpouchdb";
import { GenericFirestoreProvider } from "../../providers/genericfirestore/genericfirestore";
import { ModelNamesSqlLiteEnum } from "../../modelos/enum/ModelNamesSqlLiteEnum";
import { SincronizarDatosProvider } from "../../providers/sincronizar-datos/sincronizar-datos";
import { Formulario } from '../../modelos/Formulario';
import { ModeloFirebase } from '../../modelos/ModeloFirebase';
import { ParamFormularioDinamico } from '../../modelos/ParamFormularioDinamico';
import { Division } from "../../modelos/Division";

@IonicPage()
@Component({
  selector: "page-menucriterio",
  templateUrl: "menucriterio.html"
})
export class MenucriterioPage {
  private club = {} as Club; //se inicializa el club vacio
  public division:Division;
  private nombreEquipo: string;
  private formularioDatosClub: FormGroup;
  public listaFormulariosMarcados: Formulario[]=[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public _util: UtilidadesProvider,
    public modalController: ModalController,
    public _almacenamientoLocalProvider: AlmacenamientoLocalProvider,
    public _genericPouchdbProvider: GenericpouchdbProvider,
    public _genericFirestoreProvider: GenericFirestoreProvider,
    public _sincronizarDatosProvider: SincronizarDatosProvider,
    private alertCtrl: AlertController
  ) {
    this.club = this.navParams.get("club");
    this.division = this.navParams.get("division");


    console.info(document.getElementById("content"));

    if (this.club != undefined && this.club.nombreCompleto && this.division != undefined && this.division.nombreDivision) {
      this.nombreEquipo = this.club.nombreCompleto.toUpperCase();

      this.formularioDatosClub = this.formBuilder.group({
        direccionSedeAdmin: [
          this._util.validarString(this.club.direccionSedeAdmin),
          Validators.compose([Validators.required])
        ],
        direccionEstadio: [
          this._util.validarString(this.club.direccionEstadio),
          Validators.compose([Validators.required])
        ],
        direccionCampoDeportes: [
          this._util.validarString(this.club.direccionCampoDeportes),
          Validators.compose([Validators.required])
        ],
        nombrePresidente: [
          this._util.validarString(this.club.nombrePresidente),
          Validators.compose([Validators.required])
        ],
        responsableLicencias: [
          this._util.validarString(this.club.responsableLicencias),
          Validators.compose([Validators.required])
        ],
        capacidadestadio: [this.club.capacidadestadio,
          Validators.compose([Validators.required])
        ]
      });
    } else {
      this.formularioDatosClub = this.formBuilder.group({
        direccionSedeAdmin: [""],
        direccionEstadio: [""],
        direccionCampoDeportes: [""],
        nombrePresidente: [""],
        responsableLicencias: [""],
        capacidadestadio: []
      });
    }
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad MenucriterioPage");
    this.consultarFormularios();
  }

  ionViewDidEnter(){}


  async consultarFormularios() {
    let loader = this._util.presentarLoading("Cargando formularios...");
    await loader.present();
    try {
      let listaFormularios:Formulario[] = [];
      let isApp = this._util.isApp();
      if(isApp == true){
        // Nombre de la db para el almacenamiento local
        await this._genericPouchdbProvider.constructorToObjectDb(ModelNamesSqlLiteEnum.FORMULARIO.toString());
        let data = await this._genericPouchdbProvider.findRecord()
        if (data != null && data.length > 0) {
          listaFormularios = data;
        } else {
          console.warn("Sin datos");
        }
      }
      else{
          let data = await this._genericFirestoreProvider.findRecordFirestore(ModeloFirebase.FORMULARIO);
          console.log("this.listaFormularios firebase " , data );
          if (data != null && data.length > 0) {
            listaFormularios = data;
          } else {
            console.warn("Sin datos");
          }
      }

      if(listaFormularios != null && listaFormularios.length >0){
        //se ordena de manera alfabetica.
        listaFormularios.sort(function(a,b) {return (a.nombreFormulario > b.nombreFormulario) ? 1 : ((b.nombreFormulario > a.nombreFormulario) ? -1 : 0);} );
        if(this.division != null && this.division.listaFormularios){ //lista de formulario de la divicion
          for (let itemFormulario of listaFormularios) {
            for (let itemFormularioDivicion of this.division.listaFormularios) {           
              if(itemFormulario.id == itemFormularioDivicion.id){
                if(itemFormularioDivicion.checked == true){
                  this.listaFormulariosMarcados.push(itemFormulario);
                  break;
                }
              }
            }
          } 
        }
      }
    } catch (error) {
      this._util.doAlert('','Ocurrio algo obteniendo los formularios ' + error).present();
    }
    finally{
      loader.dismiss();
    }
  }


  setBackgroundStyle() {
    if (this.club.fotoFondo != null) {
      let style = {
        "background": "url('" + this.club.fotoFondo + "')",
        "background-position": "center",
        "background-repeat": "no-repeat",
        "background-size": "cover",
        "text-align": "center"
      };
      return style;
    } else {
      let style = {
        "background-position": "center",
        "background-repeat": "no-repeat",
        "background-size": "cover",
        "text-align": "center"
      };
      return style;
    }
  }


  async openFormDynamic(formulario:Formulario){
    if(formulario.habilitado === false){
       this._util.doAlert("Mensaje","El formulario se encuentra inhabilitada").present();
       return;
    }
    if(formulario.agrupaFormulario === true){ // se envian a los formularios agrupados
      let loader = this._util.presentarLoading("Cargando formulario...");
      await loader.present();

      //se marcan los formulario chekeados
      if(this.division != null && this.division.listaFormularios != null && this.division.listaFormularios.length > 0){
        for (let itemFomularioDivicion of this.division.listaFormularios) {
          if(itemFomularioDivicion.agrupaFormulario ==true){
            if(itemFomularioDivicion.id == formulario.id){
              if(itemFomularioDivicion.formulariosAgrupacion != null && itemFomularioDivicion.formulariosAgrupacion.length >0){
               for (let itemForAgrupado of itemFomularioDivicion.formulariosAgrupacion) {
                 if(itemForAgrupado.checked == true){
                  if(formulario.formulariosAgrupacion != null && formulario.formulariosAgrupacion.length >0){
                    for (let itemForAgrupadoSelec of formulario.formulariosAgrupacion) {
                      if(itemForAgrupado.id ==itemForAgrupadoSelec.id){
                        itemForAgrupadoSelec.checked = true;
                        console.log(itemForAgrupadoSelec.nombreFormulario);
                        break;
                      }
                    }
                  }
                 }
               }
              }
            }
          }
        }
      }
      this.navCtrl.push("MenucriterioAgrupadoPage", { formulario: formulario,club:this.club });
      loader.dismiss();
    }else{ //se envia al formulario dinamico
      let paramFormDinamico:ParamFormularioDinamico = {
         formularios : formulario,
         formulariolleno: false,
         club:this.club
      }
      let loader = this._util.presentarLoading("Cargando formulario...");
      await loader.present();
      this.navCtrl.push("FormularioDinamicoPage", { paramFormDinamico: paramFormDinamico });
      loader.dismiss();
    }
  }


  async openFormMedidasCampoJuego() {
    let loader = this._util.presentarLoading("Cargando formulario...");
    await loader.present();
    await this.navCtrl.push("CampodejuegomedidasPage", { club: this.club });
    loader.dismiss();
  }

  async guardarDatosClub() {
    if (!this.formularioDatosClub.valid) {
      return;
    }

    let loader = this._util.presentarLoading("Grabando...");
    await loader.present();

    try {
      let datosAl = await this._almacenamientoLocalProvider.obtenerDatosSesion();
      if (datosAl != null && datosAl.idUsuario != null) {
        if (this._util.isApp() == true) {
          //solo se permite hacer esta accion desde la app
          await this._genericPouchdbProvider.constructorToObjectDb(ModelNamesSqlLiteEnum.CLUB.toString());
          let clubConsultado: Club = await this._genericPouchdbProvider.getRecord(this.club._id) as Club;
      
          if (clubConsultado != null) {
            clubConsultado.direccionSedeAdmin = this.formularioDatosClub.value.direccionSedeAdmin;
            clubConsultado.direccionEstadio = this.formularioDatosClub.value.direccionEstadio;
            (clubConsultado.direccionCampoDeportes = this.formularioDatosClub.value.direccionCampoDeportes),
              (clubConsultado.nombrePresidente = this.formularioDatosClub.value.nombrePresidente);
            clubConsultado.responsableLicencias = this.formularioDatosClub.value.responsableLicencias;
            clubConsultado.capacidadestadio =this.formularioDatosClub.value.capacidadestadio;
            clubConsultado.fecha = Number(this._util.convertirFechaUnixtime(new Date()));
            let guardado = await this._genericPouchdbProvider.putRecord(clubConsultado );
            console.log("Club guardo ", guardado);
            this._util.presentarToast("Se guardarón los datos.");
          }
          else{
            this._util.doAlert("Mensaje","No fue posible guardar los datos.").present();
          }
        }
       
      } else {
        this._util.doAlert("Mensaje","Los datos del usuario no se han encontrado.").present();
      }
    } catch (error) {
      this._util.doAlert("Mensaje",error).present();
      console.log(error);
    } finally {
      loader.dismiss();
    }
  }

  async abrirCapturaFirma() {
    let modal = this.modalController.create("FirmaPage", { club: this.club ,division:this.division });
    modal.present();
  }

  async obtenerDatosFormularisoResueltosClub() {

    let loader = this._util.presentarLoading( "Obteniendo datos de formularios del servidor...");
    await loader.present();
    try {
      if (this._util.isConect() == true) {
        let respuesta = await this._sincronizarDatosProvider.obtenerDatosFormularisoResueltosClub(this.club.id);
        if (respuesta == true) {
          this._util.doAlert("Mensaje", "Datos obtenidos con éxito.").present();
        }
      } else {
        this._util.doAlert( "Mensaje","Debe estar conectado a una red wifi para realizar la sincronización").present();
      }
    } catch (error) {
      this._util.doAlert("Mensaje", error).present();
    } finally {
      loader.dismiss();
    }
  }



  presentConfirmDeleteForm() {
    let alert = this.alertCtrl.create({
      title: 'Esta seguro de realizar esta accion.',
      message: 'Presione OK para realizar la operación',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Ok',
          handler: () => {
            try {
                this.eliminarFormularisoResueltosClub();
            } catch (error) {
              this._util.doAlert('','No se fue posible realizar la operación' + error).present();;
            }
          }
        }
      ]
    });
    alert.present();
  }

  async eliminarFormularisoResueltosClub(){

    let loader = this._util.presentarLoading( "Eliminado datos de formularios...");
    await loader.present();
    try {
        let respuesta = await this._sincronizarDatosProvider.eliminarFormularisoResueltosClub( this.club.id);
        if (respuesta == true) {
          this._util.doAlert("Mensaje", "Datos eliminados con éxito.").present();
        }
    } catch (error) {
      this._util.doAlert("Mensaje", error).present();
    } finally {
      loader.dismiss();
    }
  }

}
