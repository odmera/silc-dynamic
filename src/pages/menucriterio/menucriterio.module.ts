import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenucriterioPage } from './menucriterio';

@NgModule({
  declarations: [
    MenucriterioPage,
  ],
  imports: [
    IonicPageModule.forChild(MenucriterioPage),
  ],
})
export class MenucriterioPageModule {}
