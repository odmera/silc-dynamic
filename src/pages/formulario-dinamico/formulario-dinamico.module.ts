import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormularioDinamicoPage } from './formulario-dinamico';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    FormularioDinamicoPage,
  ],
  imports: [
    IonicPageModule.forChild(FormularioDinamicoPage),
    ComponentsModule
  ],
})

export class FormularioDinamicoPageModule {}
