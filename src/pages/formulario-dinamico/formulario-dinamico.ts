import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, AlertController, ToastController } from 'ionic-angular';
import { UtilidadesProvider } from '../../providers/utilidades/utilidades';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormdynamicProvider } from '../../providers/formdynamic/formdynamic';
import { QuestionBaseAPS } from '../../modelos/formulariodinamico/QuestionBaseAPS';
import { PreguntaResuelta } from '../../modelos/PreguntaResuelta';
import { Formulario } from '../../modelos/Formulario';
import { ParamFormularioDinamico } from '../../modelos/ParamFormularioDinamico';
import { TipoPreguntaEnum } from '../../modelos/enum/TipoPreguntaEnum';
import { FormularioPregunta } from '../../modelos/FormularioPregunta';
import { FormularioOpcion } from '../../modelos/FormularioOpcion';
import { Foto } from '../../modelos/Foto';
import { PhotoQuestionAPS } from '../../modelos/formulariodinamico/PhotoQuestionAPS';
import { GenericpouchdbProvider } from '../../providers/genericpouchdb/genericpouchdb';
import { GenericFirestoreProvider } from '../../providers/genericfirestore/genericfirestore';
import { ModeloFirebase } from '../../modelos/ModeloFirebase';
import { ModelNamesSqlLiteEnum } from '../../modelos/enum/ModelNamesSqlLiteEnum';



@IonicPage()
@Component({
  selector: 'page-formulario-dinamico',
  templateUrl: 'formulario-dinamico.html',
})
export class FormularioDinamicoPage {

  mostrarBorde: boolean = false;
  disabledBtnGuardar: boolean = false;
  formularioDinamico: FormGroup;
  formularioDinamicoModelo: Formulario;
  paramFormDinamico: ParamFormularioDinamico;

  questionsForm: QuestionBaseAPS<any>[] = [];
  listPreguntasResueltasForm: PreguntaResuelta[] = [];

  customPickerOption = {
    buttons: [{
      text: 'Cancelar',
      handler: (data: any) => {
        return true;
      }
    }]
  }

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public formdynamicProvider: FormdynamicProvider,
    public _util: UtilidadesProvider,
    public platform: Platform,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    public _genericPouchdbProvider: GenericpouchdbProvider,
    public _genericFirestoreProvider: GenericFirestoreProvider) {
   
    this.paramFormDinamico = navParams.get('paramFormDinamico');

    if(this.paramFormDinamico != null){
      this.armarFormulario();
    }
    
  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FormularioDinamicoPage');
  }


  /**
   * Metodo que arma formulario dinamico
   * @author OM
   */
  async armarFormulario() {

    let loader = this._util.presentarLoading("Cargando formulario...");
    loader.present();

    try {

      let listPreguntasResueltas: PreguntaResuelta[];
      if(this._util.isApp() == true){
        // Nombre de la db para el almacenamiento local
        await this._genericPouchdbProvider.constructorToObjectDb(ModelNamesSqlLiteEnum.FORMULARIORESPUESTA.toString());

        //se crea el indice ['idclub', 'idformulario']
        let indexformulariorespuestaidclubidformulario = await this._genericPouchdbProvider.createIndex('indexformulariorespuestaidclubidformulario',['idclub', 'idformulario']);
        console.log("indexformulariorespuestaidclubidformulario " , indexformulariorespuestaidclubidformulario);

        let datos = await this._genericPouchdbProvider.findRecordPropertys({
          idclub: this.paramFormDinamico.club.id,
          idformulario: this.paramFormDinamico.formularios.id
        },'indexformulariorespuestaidclubidformulario')

        listPreguntasResueltas=datos as PreguntaResuelta[]; 
  
      }
      else{
        listPreguntasResueltas = await this._genericFirestoreProvider.cargarPreguntasResueltas(this.paramFormDinamico.club.id,this.paramFormDinamico.formularios.id)
      }

      if(listPreguntasResueltas != null && listPreguntasResueltas.length > 0){ //si el formulario ya se ha llenado en el pasado
        this.paramFormDinamico.formulariolleno = true;
        this.paramFormDinamico.listPreguntasResueltas = listPreguntasResueltas;
      }

      this.formularioDinamicoModelo = await this.formdynamicProvider.consultarDatosFormulario(this.paramFormDinamico);
      let questions: QuestionBaseAPS<any>[] = [];
     

      if(this.formularioDinamicoModelo != null && this.formularioDinamicoModelo.id != null){ 
        for (let itemBloque of this.formularioDinamicoModelo.listaBloques) {
  
          //se gestiona el numero de columnas de los bloques de acuerdo a los 
          //dispositvos donde se esta usando la aplicacion
          //se cambia el valor de las columnas de los bloques
          if (this.platform.is('tablet')) {
            if (itemBloque.numerocolumnas > 3) {
              itemBloque.numerocolumnas = 3;
            }
          }

          else if (this.platform.is('mobile') || this.platform.is('mobileweb')) {
            if (itemBloque.numerocolumnas > 1) {
              itemBloque.numerocolumnas = 1;
            }
          }

          //se guardan las preguntas en la lista de preguntas dinamicas globales
          questions.push(...itemBloque.preguntasdinamicas);
        }
      }

      this.questionsForm = questions;
      this.listPreguntasResueltasForm = listPreguntasResueltas;
      this.formularioDinamico = this.toFormGroup(questions);      

    } catch (error) {
      this._util.doAlert("Ocurrió algo con el formularo dinamico", JSON.stringify(error)).present();
    }
    finally {
      loader.dismiss();
    }
  }

  toFormGroup(questions: QuestionBaseAPS<any>[]) {
    let group: any = {};
    questions.forEach(question => {
      
      if(question.controlType !== 'photo'){
        group[question.key] = question.required ? new FormControl(question.value || null, Validators.required)
        : new FormControl(question.value || null);
      }
      else{
        //console.log("Pregunta foto " , JSON.stringify(question));
      }
    });
    return new FormGroup(group);
  }

  mostrarBordeFormulario() {
    this.mostrarBorde = !this.mostrarBorde;
  }

  /*--------------------- grabacion formulario dinamico --------------------*/

  /**
   * metodo que permite guardar el formulario dinamico
   */
   
  terminar() {

    /*let preguntasNoDiligenciadas =  this.validarPreguntasObligatorias();
    if(preguntasNoDiligenciadas !== null && preguntasNoDiligenciadas !== '' && preguntasNoDiligenciadas.length > 0){
      this.presentarToast(preguntasNoDiligenciadas);
      return;
    }*/

    if (this.paramFormDinamico.formulariolleno === true) { //edicion
      this.editarDatosPreguntasDinamicas();
    }
    else {
      this.guardarDatosPreguntasDinamicas();
    }
  }

  async guardarDatosPreguntasDinamicas() {
    let loader = this._util.presentarLoading("Procesando...");
    await loader.present();

    try {
      //se el formulario a un objeto map (es decir llave y valor)
      let preguntasFormularioAPSMap = this._util.objToStrMap(this.formularioDinamico.value);

      let listPreguntasRespuestas: PreguntaResuelta[] = [];

      for (let questionForm of this.questionsForm) {

        ////se obtiene el valor de la pregunta.
        questionForm.value = preguntasFormularioAPSMap.get(questionForm.key);

        let tipoPregunta: string = questionForm.pregunta.tipoPreguntaEnumId;//(texto,fecha, multipleselecion etc) 
        let formularioPregunta: FormularioPregunta = questionForm.pregunta;

        if (tipoPregunta === TipoPreguntaEnum.OPCIONUNICARESPUESTA.id) { //unica
          let respuestaUnica = this.armarObjectoPreguntaUnicaResuelta(questionForm);
          listPreguntasRespuestas.push(respuestaUnica);
        }
        else if (tipoPregunta === TipoPreguntaEnum.TEXTO.id) { //texto
          let respuestaTexto = this.crearObjetoPreguntaResuelta(formularioPregunta);
          respuestaTexto.respuestatexto = this.obtenerValueResultadoTexto(questionForm.value);
          listPreguntasRespuestas.push(respuestaTexto);
        }
        else if (tipoPregunta === TipoPreguntaEnum.NUMERICA.id) { //numerica
          let respuestaNumerica = this.crearObjetoPreguntaResuelta(formularioPregunta);
          respuestaNumerica.respuestanumerica = this.obtenerValueResultadoNumerico(questionForm.value);
          listPreguntasRespuestas.push(respuestaNumerica);
        }
        else if (tipoPregunta === TipoPreguntaEnum.LOGICA.id) { //logica
          let respuestaLogica = this.crearObjetoPreguntaResuelta(formularioPregunta);
          respuestaLogica.respuestalogica = questionForm.value == null ? false : Boolean(questionForm.value);
          listPreguntasRespuestas.push(respuestaLogica);
        }
        else if (tipoPregunta === TipoPreguntaEnum.FECHA.id) { //fecha
          let respuestaFecha = this.crearObjetoPreguntaResuelta(formularioPregunta);
          respuestaFecha.respuestafecha = questionForm.value;
          listPreguntasRespuestas.push(respuestaFecha);
        }

        else if (tipoPregunta === TipoPreguntaEnum.FOTO.id) { //foto
          let respuestaFoto = this.crearObjetoPreguntaResuelta(formularioPregunta);
          respuestaFoto.listImages = this.obtenerValueResultadoFoto(questionForm);
          listPreguntasRespuestas.push(respuestaFoto);
        }
      }

      if (this.paramFormDinamico == null || this.paramFormDinamico.club == null) {
        this._util.doAlert("", "No se encontro el parametros del tipo del formulario").present();
        return;
      }
      
      if(listPreguntasRespuestas != null && listPreguntasRespuestas.length > 0){
        if (this._util.isApp() == true) {
          let respuestaPB = await this._genericPouchdbProvider.putRecordMultiple(listPreguntasRespuestas);
          console.log("respuestaPB " , respuestaPB);
        }
        else {
          let respuestaFirebase = await this._genericFirestoreProvider.addOrUpdateRecordFirestoreMultiple(listPreguntasRespuestas, ModeloFirebase.FORMULARIORESPUESTA);
          console.log("respuestaFirebase " , respuestaFirebase);
        }
        this._util.presentarToast("Registro almacenado con exito...!!");
        setTimeout(() => {
          this.navCtrl.pop();
        }, 2000);
      }
      else{
          this._util.doAlert("", "No se encontrarón preguntas para almacenar. ").present();
      }

    } catch (error) {
        this._util.doAlert("", "No fue posible almacenar el registro. " + JSON.stringify(error)).present();
    }
    finally{
      loader.dismiss();
    }
  
  }


 async editarDatosPreguntasDinamicas() {

    let loader = this._util.presentarLoading("Procesando...");
    await loader.present();

    try {
      //se el formulario a un objeto map (es decir llave y valor)
      let preguntasFormularioAPSMap = this._util.objToStrMap(this.formularioDinamico.value);
      let listPreguntasRespuestasEdicion: PreguntaResuelta[] = [];

      for (let questionForm of this.questionsForm) {

        ////se obtiene el valor de la pregunta.
        questionForm.value = preguntasFormularioAPSMap.get(questionForm.key);

        let tipoPregunta: string = questionForm.pregunta.tipoPreguntaEnumId;//(texto,fecha, multipleselecion etc) 

        if (tipoPregunta === TipoPreguntaEnum.OPCIONUNICARESPUESTA.id) { //unica
            let preguntaUnicaResuelta = this.edicionObjetoPreguntaResueltaUnica(questionForm);
            if(preguntaUnicaResuelta != null){
              listPreguntasRespuestasEdicion.push(preguntaUnicaResuelta); 
            }
            else{
              let respuestaUnica = this.armarObjectoPreguntaUnicaResuelta(questionForm);
              listPreguntasRespuestasEdicion.push(respuestaUnica);
            } 
        }
        else if (tipoPregunta === TipoPreguntaEnum.TEXTO.id) { //texto
          this.edicionObjetoPreguntaResueltaTexto(questionForm,listPreguntasRespuestasEdicion);
        }
        else if (tipoPregunta === TipoPreguntaEnum.NUMERICA.id) { //numerica
          this.edicionObjetoPreguntaResueltaNumerica(questionForm,listPreguntasRespuestasEdicion);
        }
        else if (tipoPregunta === TipoPreguntaEnum.LOGICA.id) { //logica
          this.edicionObjetoPreguntaResueltaLogica(questionForm,listPreguntasRespuestasEdicion);
        }
        else if (tipoPregunta === TipoPreguntaEnum.FECHA.id) { //fecha
          this.edicionObjetoPreguntaResueltaFecha(questionForm,listPreguntasRespuestasEdicion);
        }
        else if (tipoPregunta === TipoPreguntaEnum.FOTO.id) { //foto
          this.edicionObjetoPreguntaResueltaFoto(questionForm,listPreguntasRespuestasEdicion)
        }
      }

      if (this.paramFormDinamico == null || this.paramFormDinamico.club == null) {
        this._util.doAlert("", "No se encontro el parametro del tipo del formulario").present()
        return;
      }

      if(listPreguntasRespuestasEdicion != null && listPreguntasRespuestasEdicion.length > 0){
        if (this._util.isApp() == true) {
          let respuestaPB = await this._genericPouchdbProvider.putRecordMultiple(listPreguntasRespuestasEdicion);
          console.log("respuestaPB " , respuestaPB);
        }
        else {
          let respuestaFirebase = await this._genericFirestoreProvider.addOrUpdateRecordFirestoreMultiple(listPreguntasRespuestasEdicion, ModeloFirebase.FORMULARIORESPUESTA);
          console.log("respuestaFirebase " , respuestaFirebase);
        }
        this._util.presentarToast("Registro almacenado con exito...!!");
        setTimeout(() => {
          this.navCtrl.pop();
        }, 2000);
      }
      else{
          this._util.doAlert("", "No se encontrarón preguntas para almacenar. ").present();
      }
      
    } catch (error) {
      this._util.doAlert("", "No fue posible almacenar el registro. " + JSON.stringify(error)).present();
    }
    finally{
      loader.dismiss();
    }

  }


  validarPreguntasObligatorias(){
    let preguntasNoDiligenciadas:string = ''; 

    let preguntasFormularioAPSMap = this._util.objToStrMap(this.formularioDinamico.value);

    for (let questionForm of this.questionsForm) {
      //son preguntas que estas visibles en el formulario y son obligatorias
      if(questionForm.pregunta != null){
        if(questionForm.showquestion === true && questionForm.required === true){
          let preguntaResuelta = preguntasFormularioAPSMap.get(questionForm.key);
          if(preguntaResuelta === null || preguntaResuelta === 'null' || preguntaResuelta === ''){
            preguntasNoDiligenciadas += `La pregunta "${questionForm.pregunta.nombrePregunta}" es obligatoria \n`
          }
        }
      }
    }
    return preguntasNoDiligenciadas;
  }


  /**
   * metodo que arma el objecto para la grabacion de las preguntas
   * @param preguntaForm
   * @author OM 
   */

   
  crearObjetoPreguntaResuelta(preguntaForm: FormularioPregunta): PreguntaResuelta {

    let preguntaResuelta: PreguntaResuelta = {
      id:null,
      _rev:null,
      _id:this._util.armarIdentificadorPouchdb(this.paramFormDinamico.club.id,this.paramFormDinamico.formularios.id,preguntaForm.id),
      idclub:this.paramFormDinamico.club.id,
	    nombreclub: this.paramFormDinamico.club.nombreCompleto,
      idformulario: this.paramFormDinamico.formularios.id, //se saca el primir formulario, es decir el unico formulario que se pinta
      nombreformulario: this.paramFormDinamico.formularios.nombreFormulario,
      idbloque: preguntaForm.idbloque,
      idpregunta: preguntaForm.id,
      nombrepregunta:preguntaForm.nombrePregunta,
      listaOpcionesResueltas : null,
      respuestatexto: null,
      respuestanumerica: null,
      respuestalogica: null,
      respuestafecha: null,
      listImages:null
    }
    return preguntaResuelta;
  }

  /**
   * metodo que arma el objecto para la grabacion de las preguntas unica respuestas
   * @param formularioPreguntaUnica  
   * @param questionForm 
   * @author OM
   */

  
  armarObjectoPreguntaUnicaResuelta( questionForm: QuestionBaseAPS<any>): PreguntaResuelta {
    let formularioPreguntaUnica = questionForm.pregunta;
    let respuestaUnica = this.crearObjetoPreguntaResuelta(formularioPreguntaUnica);
    let valorRespuestaUnica = questionForm.value;
  
    if (valorRespuestaUnica != null) {
      for (let respuestapregunta of formularioPreguntaUnica.listaOpciones) {
        if (valorRespuestaUnica.trim() === respuestapregunta.id.trim()) {

          //si arma la opcion marcada por el usuario
          let Opcion :FormularioOpcion ={
           id:respuestapregunta.id,
           nombreOpcion:respuestapregunta.nombreOpcion,
           color:respuestapregunta.color,
          }

          let listaOpciones :FormularioOpcion[] =[];
          listaOpciones.push(Opcion);

          respuestaUnica.listaOpcionesResueltas = listaOpciones;
          break;
        }
      }
    }
    return respuestaUnica;
  }

  

  edicionObjetoPreguntaResueltaUnica(questionForm: QuestionBaseAPS<any>){
    let formularioPreguntaUnica  = questionForm.pregunta;
    for (let preguntaUnicaResuelta of this.listPreguntasResueltasForm) {
      if (preguntaUnicaResuelta.idpregunta === questionForm.pregunta.id) {
        let valorRespuestaUnica = questionForm.value;
        if (valorRespuestaUnica != null) {
          for (let respuestapregunta of formularioPreguntaUnica.listaOpciones) {
            if (valorRespuestaUnica.trim() === respuestapregunta.id.trim()) {
              //si arma la opcion marcada por el usuario
              let Opcion :FormularioOpcion ={
                id:respuestapregunta.id,
                nombreOpcion:respuestapregunta.nombreOpcion,
                color:respuestapregunta.color,
              }
              let listaOpciones :FormularioOpcion[] =[];
              listaOpciones.push(Opcion);
              preguntaUnicaResuelta.listaOpcionesResueltas = listaOpciones;
              return preguntaUnicaResuelta;
            }
          }
        }
      }
    }
  }

  edicionObjetoPreguntaResueltaTexto(questionForm: QuestionBaseAPS<any>,listPreguntasRespuestasEdicion: PreguntaResuelta[]){
    let existe:boolean=false;
    for (let preguntaTextoResuelta of this.listPreguntasResueltasForm) {
      if (questionForm.pregunta.id === preguntaTextoResuelta.idpregunta) {
        preguntaTextoResuelta.respuestatexto = this.obtenerValueResultadoTexto(questionForm.value);
        listPreguntasRespuestasEdicion.push(preguntaTextoResuelta);
        existe = true
        break;
      }
    }
    if(existe === false){ //es una pregunta que agrego al formulario despues de que estaba grabado.
      let respuestaTexto = this.crearObjetoPreguntaResuelta(questionForm.pregunta);
      respuestaTexto.respuestatexto = this.obtenerValueResultadoTexto(questionForm.value);
      listPreguntasRespuestasEdicion.push(respuestaTexto);
    }
  }

  edicionObjetoPreguntaResueltaNumerica(questionForm: QuestionBaseAPS<any>,listPreguntasRespuestasEdicion: PreguntaResuelta[]){
    let existe:boolean=false;
    for (let preguntaNumericaResuelta of this.listPreguntasResueltasForm) {
      if (preguntaNumericaResuelta.idpregunta === questionForm.pregunta.id) {
        preguntaNumericaResuelta.respuestanumerica = this.obtenerValueResultadoNumerico(questionForm.value);;
        listPreguntasRespuestasEdicion.push(preguntaNumericaResuelta);
        existe = true
        break;
      }
    }
    if(existe === false){ //es una pregunta que agrego al formulario despues de que estaba grabado.
      let respuestaNumerica = this.crearObjetoPreguntaResuelta(questionForm.pregunta);
      respuestaNumerica.respuestanumerica = this.obtenerValueResultadoNumerico(questionForm.value);
      listPreguntasRespuestasEdicion.push(respuestaNumerica);
    }
  }

  edicionObjetoPreguntaResueltaLogica(questionForm: QuestionBaseAPS<any>,listPreguntasRespuestasEdicion: PreguntaResuelta[]){
    let existe:boolean=false;
    for (let preguntaLogicaResuelta of this.listPreguntasResueltasForm){
      if (preguntaLogicaResuelta.idpregunta === questionForm.pregunta.id) {
        preguntaLogicaResuelta.respuestalogica = this.obtenerValueResultadoLogico(questionForm.value);
        listPreguntasRespuestasEdicion.push(preguntaLogicaResuelta);
        existe = true
        break;
      }
    }

    if(existe === false){ //es una pregunta que agrego al formulario despues de que estaba grabado.
      let respuestaLogica = this.crearObjetoPreguntaResuelta(questionForm.pregunta);
      respuestaLogica.respuestalogica = this.obtenerValueResultadoLogico(questionForm.value);
      listPreguntasRespuestasEdicion.push(respuestaLogica);
    }
  }

  edicionObjetoPreguntaResueltaFecha(questionForm: QuestionBaseAPS<any>,listPreguntasRespuestasEdicion: PreguntaResuelta[]){
    let existe:boolean=false;
    for (let preguntaFechaResuelta of this.listPreguntasResueltasForm) {
      if(preguntaFechaResuelta.idpregunta === questionForm.pregunta.id) {
        preguntaFechaResuelta.respuestafecha = questionForm.value;
        listPreguntasRespuestasEdicion.push(preguntaFechaResuelta);
        existe = true
        break;
      }
    }
    if(existe === false){ //es una pregunta que agrego al formulario despues de que estaba grabado.
      let respuestaFecha = this.crearObjetoPreguntaResuelta(questionForm.pregunta);
      respuestaFecha.respuestafecha = questionForm.value;
      listPreguntasRespuestasEdicion.push(respuestaFecha);
    }
  }

  edicionObjetoPreguntaResueltaFoto(questionForm: QuestionBaseAPS<any>,listPreguntasRespuestasEdicion: PreguntaResuelta[]){
    let existe:boolean=false;
    for (let preguntaFotoResuelta of this.listPreguntasResueltasForm) {
      if(preguntaFotoResuelta.idpregunta === questionForm.pregunta.id) {
        preguntaFotoResuelta.listImages = this.obtenerValueResultadoFoto(questionForm);
        listPreguntasRespuestasEdicion.push(preguntaFotoResuelta);
        existe = true
        break;
      }
    }
    if(existe === false){ //es una pregunta que agrego al formulario despues de que estaba grabado.
      let respuestaFoto = this.crearObjetoPreguntaResuelta(questionForm.pregunta);
      respuestaFoto.listImages = this.obtenerValueResultadoFoto(questionForm);
      listPreguntasRespuestasEdicion.push(respuestaFoto);
    }
  }

  presentarToast(mensaje) {
    let toast = this.toastCtrl.create({
      message: mensaje,
      position: "middle",
      showCloseButton:true,
      closeButtonText:"Aceptar",
      cssClass: "toastPreline"
    });
    toast.present();
    return toast;
  }


  obtenerValueResultadoTexto(value:any){
   let resultadoTexto:string = value === null || value === "null" ? null : String(value);
   return resultadoTexto;
  }

  obtenerValueResultadoNumerico(value:any){
    let resultadoNumerico:number = value == null ? null : Number(value);
    return resultadoNumerico;
  }

  obtenerValueResultadoLogico(value:any){
    let resultadoLogico:boolean = value == null ? false : Boolean(value);
    return resultadoLogico;
  }

  obtenerValueResultadoFecha(value:any){
    let resultadoFecha:boolean = value == null ? false : Boolean(value);
    return resultadoFecha;
  }

  obtenerValueResultadoFoto(questionForm: QuestionBaseAPS<any>){
    let resultadoFoto= questionForm['listImages'] != undefined && questionForm['listImages'] != null ? questionForm['listImages']:null;
    return resultadoFoto;
  }


  


  /*-------------------- termina grabacion formulario dinamico----------------*/




  /*------------------------- eventos de controles dinamicos -----------------*/

  /**
   * evento para el boton limpiar de los componente fecha y fecha hora
   * @param preguntaDinamicaActual 
   * @author OM
   */
  eventClearDate(preguntaDinamicaActual: QuestionBaseAPS<any>) {
    this.formularioDinamico.controls[preguntaDinamicaActual.key].setValue(null)
  }

  getColor(opcion:FormularioOpcion) {
    //console.log("opcion.color " , opcion.color);
    //console.log("opcion " , opcion)
    return opcion.color != undefined && opcion.color != null ? opcion.color : null
  }

  obtenerImgBase64(ev: string, preguntaDinamicaActualFoto: PhotoQuestionAPS) {
    // Aqui se puede crear una lista he ir almacenando las imagenes capturadas
    console.log("Mi cadena en base 64 ", ev);
    let fotoBase64: Foto = {
      foto: ev,
      nombreFoto: this._util.convertirFechaUnixtime(new Date),
      esBase64: true
    }
    this.addFotoToArray(fotoBase64, preguntaDinamicaActualFoto);
  }

  addFotoToArray(fotoBase64: Foto, preguntaDinamicaActualFoto: PhotoQuestionAPS) {
    if(preguntaDinamicaActualFoto != null && preguntaDinamicaActualFoto.listImages != null){
       preguntaDinamicaActualFoto.listImages.push(fotoBase64);
    }   
  }
  /*----------------------termina eventos de controles dinamicos -----------------*/

  

}








