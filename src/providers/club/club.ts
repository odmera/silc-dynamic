import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from 'angularfire2/firestore';
import { Club } from '../../modelos/Club';
import { UtilidadesProvider } from '../utilidades/utilidades';
import { AlmacenamientoArchivosFirebaseProvider } from '../almacenamiento-archivos-firebase/almacenamiento-archivos-firebase';
import { ModeloFirebase } from '../../modelos/ModeloFirebase';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ClubProvider {

  collectionClub: AngularFirestoreCollection<Club>;


  constructor(public _util: UtilidadesProvider,
              public _almacenamientoFirebaseProvider: AlmacenamientoArchivosFirebaseProvider,
              public afDB: AngularFirestore) {
  
    this.collectionClub = this.afDB.collection(`/${ModeloFirebase.CLUB}`);
  }

  obtenerRef(id) {
    return this.afDB.doc<Club>(`${ModeloFirebase.CLUB}/${id}`);
  }

  consultarClubes(): Promise<Club[]> {
    return new Promise((resolve, reject) => {

      return this.afDB.collection(`${ModeloFirebase.CLUB}`).snapshotChanges().map(actions => {
          return actions.map(a => {
            return { id: a.payload.doc.id, ...a.payload.doc.data() } as Club;
          });
        }).subscribe(datos => {
          resolve(datos)
        });
    });
  }

  async crearActualizarClub(Club: Club,imagenFirebase:string, imagenFondoFirebase:string): Promise<Club> {
    if (Club.id == null) { //se guarda
      let documento = await this.collectionClub.add(Club);
      Club.id = documento.id;
      await this.obtenerRef(Club.id).update(Club);
    }
    else { //se actualiza
      await this.obtenerRef(Club.id).update(Club);
    }

    //se estable el nombre de la imagen
    let nombreArchivo = Club.id;

    if (imagenFirebase != null){
      let urlFoto: string = await this._almacenamientoFirebaseProvider.
        guardarFotoFirebase(imagenFirebase, nombreArchivo);

      if (urlFoto != null) {
        Club.foto = urlFoto;
        Club.nombreFoto = nombreArchivo;
        await this.obtenerRef(Club.id).update(Club);
      }
    }
    // Fondo
    if (imagenFondoFirebase != null){
      let urlFoto: string = await this._almacenamientoFirebaseProvider.
        guardarFotoFirebase(imagenFondoFirebase, nombreArchivo+"Fondo");

      if (urlFoto != null) {
        Club.fotoFondo = urlFoto;
        Club.nombreFotoFondo = nombreArchivo+"Fondo";
        await this.obtenerRef(Club.id).update(Club);
      }
    }
    return Club;
  }

  consultarClubPorid(id: string): Observable<Club[]> {
    return this.afDB.collection(`${ModeloFirebase.CLUB}`, ref => 
     ref.where('id', '==', id)).snapshotChanges().map(actions => {
      return actions.map(a => {
        return { id: a.payload.doc.id, ...a.payload.doc.data() } as Club;
      });
    });
  }

}
