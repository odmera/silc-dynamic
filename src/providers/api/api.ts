import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SincronizacionApi } from '../../modelos/SincronizacionApi';

let apiUrl = 'http://licenciasdeclubes.com/silc-afa-apirest/public/api/pdf_dynamic';


@Injectable()
export class ApiProvider {

  constructor(public http: HttpClient) {
    console.log('Hello ApiProvider Provider');
  }



  /**
   * metodo que envia los datos a la api que genera el pdf y envia los datos.
   * @param sincronizacionApi
   */
  enviarDatosApiPdf(sincronizacionApi :SincronizacionApi): Promise<any> {

    return new Promise((resolve, reject) => {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          //'responseType': 'text'
        }),
      };
      //console.log("URL API " , apiUrl);
      //console.log("datosPdf " , JSON.stringify(sincronizacionApi));
      var url = apiUrl;
      this.http.post(url, JSON.stringify(sincronizacionApi),httpOptions)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          console.log(err);
          reject(err);
        });
    });
  }


}
