import { Injectable } from '@angular/core';
import { UtilidadesProvider } from '../utilidades/utilidades';
import { Formulario } from '../../modelos/Formulario';
import { ParamFormularioDinamico } from '../../modelos/ParamFormularioDinamico';
import { PreguntaResuelta } from '../../modelos/PreguntaResuelta';
import { TipoPreguntaEnum } from '../../modelos/enum/TipoPreguntaEnum';
import { FormularioPregunta } from '../../modelos/FormularioPregunta';
import { SelectOnlyQuestionAPS } from '../../modelos/formulariodinamico/SelectOnlyQuestionAPS';
import { QuestionBaseAPS } from '../../modelos/formulariodinamico/QuestionBaseAPS';
import { FormularioOpcion } from '../../modelos/FormularioOpcion';
import { TextAreaQuestionAPS } from '../../modelos/formulariodinamico/TextAreaQuestionAPS';
import { TextBoxQuestionAPS } from '../../modelos/formulariodinamico/TextBoxQuestionAPS';
import { ToggleQuestionAPS } from '../../modelos/formulariodinamico/ToggleQuestionAPS';
import { DateQuestionAPS } from '../../modelos/formulariodinamico/DateQuestionAPS.';
import { PhotoQuestionAPS } from '../../modelos/formulariodinamico/PhotoQuestionAPS';

/*Tipo de pregunta 
  1=Mutuamente excluyente,
  2=Opciónmúltiple,
  3=Abierta/Texto,
  4=Abierta/ Numérica,
  5=Lógica,
  6=Abierta/Fecha,
  7=foto*/

@Injectable()
export class FormdynamicProvider {


  paramFormDinamicoGlobal: ParamFormularioDinamico;

  constructor(/*private _formularioResueltoProvider: FormularioResueltoProvider,
              private _formularioBloqueProvider: FormularioBloqueProvider,
              private _formularioPreguntaProvider: FormularioPreguntaProvider,
              private _formularioRespuestaProvider: FormularioOpcionProvider,*/
              private _util: UtilidadesProvider) {
  }


  /**
   * metodo que se encarga de armar los objectos para armar el formulario dinamico
   * @param paramFormDinamico
   * @author OM
   */
  async consultarDatosFormulario(paramFormDinamico: ParamFormularioDinamico): Promise<Formulario> {
    try {

      if(paramFormDinamico == null ||
          paramFormDinamico.formularios == null) {
          throw new Error("No hay formularios para mostrar");
      }

      this.paramFormDinamicoGlobal = paramFormDinamico;

      let itemFormulario = paramFormDinamico.formularios; //siempre sera un solo formulario

      if(itemFormulario != null && itemFormulario.id != null){
        //se consultan los formularios resueltos
        let listPreguntasResueltas: PreguntaResuelta[] = paramFormDinamico.listPreguntasResueltas;
        
        let listBloques = itemFormulario.listaBloques;

        if (listBloques != null && listBloques.length > 0) {

          //se ordenan los bloques
          listBloques.sort((a, b) => a.orden - b.orden);

          for (let itemBloque of listBloques) {

            if(itemBloque.habilitado === false){ //solo se pintan los bloques habilitadas
              continue;
            }

            let listPreguntas = itemBloque.listaPreguntas;
            let questions: QuestionBaseAPS<any>[] = [];

            if (listPreguntas != null && listPreguntas.length > 0) {

              for (let pregunta of listPreguntas) {

                if(pregunta.habilitado === false){ //solo se pintan las preguntas habilitadas
                  continue;
                }

                if (pregunta.tipoPreguntaEnumId === TipoPreguntaEnum.OPCIONUNICARESPUESTA.id) { //1 Mutuamente excluyente

                  //se consultan las opciones de la pregunta
                  let listaOpciones = pregunta.listaOpciones;

                  let preguntaUnicaRespuesta = this.armarPreguntaUnicaRespuesta(pregunta, listaOpciones, listPreguntasResueltas);
                  questions.push(preguntaUnicaRespuesta);
                }
                else if (pregunta.tipoPreguntaEnumId === TipoPreguntaEnum.TEXTO.id) { //3=Abierta/Texto,
                  let preguntaTexto = this.armarPreguntaTexto(pregunta, listPreguntasResueltas);
                  questions.push(preguntaTexto);
                }
                else if (pregunta.tipoPreguntaEnumId === TipoPreguntaEnum.NUMERICA.id) {//4=Abierta/ Numérica,
                  let preguntaNumerica = this.armarPreguntaNumerica(pregunta, listPreguntasResueltas);
                  questions.push(preguntaNumerica);
                }
                else if (pregunta.tipoPreguntaEnumId === TipoPreguntaEnum.LOGICA.id) {//5=Lógica,
                  let preguntaLogica = this.armarPreguntaLogica(pregunta, listPreguntasResueltas);
                  questions.push(preguntaLogica);
                }
                else if (pregunta.tipoPreguntaEnumId === TipoPreguntaEnum.FECHA.id) { //6=Abierta/Fecha,
                  let preguntaFecha = this.armarPreguntaFecha(pregunta, listPreguntasResueltas);
                  questions.push(preguntaFecha);
                }
                else if (pregunta.tipoPreguntaEnumId === TipoPreguntaEnum.FOTO.id) { //7=Foto,
                  let preguntaFoto = this.armarPreguntaFoto(pregunta, listPreguntasResueltas);
                  questions.push(preguntaFoto);
                }
              }
              //se ordenan las preguntas
              questions.sort((a, b) => a.order - b.order);
              itemBloque.preguntasdinamicas = questions;
            }
          }
        }
      }

      return itemFormulario;

    } catch (error) {
      alert(JSON.stringify(error));
      throw new Error(error);
    }
  }


  /**
   * Metodo que arma pregunta dinamica tipo texto
   * @param pregunta
   * @param listPreguntasResueltas 
   * @author OM
   */
  armarPreguntaTexto(pregunta: FormularioPregunta, listPreguntasResueltas: PreguntaResuelta[]): TextAreaQuestionAPS {
    try {
      let valuePregunta: string = null;

      if (this.paramFormDinamicoGlobal != null &&
        this.paramFormDinamicoGlobal.formulariolleno === true) { //el formulario esta lleno
        for (const itemPreguntaResuelta of listPreguntasResueltas) {
          if (itemPreguntaResuelta.idpregunta === pregunta.id) {
            valuePregunta = itemPreguntaResuelta.respuestatexto;
            break;
          }
        }
      }
      else { //formulario nuevo
        //valuePregunta = pregunta.valorpred != null && pregunta.valorpredeterminado.trim().length > 0 ? pregunta.valorpredeterminado : null
      }

      let preguntaTexto = new TextAreaQuestionAPS({
        key: pregunta.id,
        label: pregunta.nombrePregunta.trim(),
        value: valuePregunta,
        required: pregunta.obligatoria,
        order: pregunta.orden,
        pregunta: pregunta,
        showquestion: true,
        showlabel: pregunta.mostrarnombre,
        subtitle:pregunta.subtitulo 
      });
      return preguntaTexto;
    } catch (error) {
      alert("Error armando pregunta unica texto. " + error);
    }

  }


  /**
   * Metodo que arma pregunta dinamica tipo unica respuesta
   * @param pregunta 
   * @param listaOpciones
   * @param listPreguntasResueltas
   * @author OM 
   */
  armarPreguntaUnicaRespuesta(pregunta: FormularioPregunta, listaOpciones: FormularioOpcion[],
                              listPreguntasResueltas: PreguntaResuelta[]): SelectOnlyQuestionAPS {

    try {
      let opcionSeleccionada: string[] = [];

      if (this.paramFormDinamicoGlobal != null &&
        this.paramFormDinamicoGlobal.formulariolleno === true) { //el formulario esta lleno
        for (const itemPreguntaResuelta of listPreguntasResueltas) {
          if (itemPreguntaResuelta.idpregunta === pregunta.id) {
            if(itemPreguntaResuelta.listaOpcionesResueltas !== null && itemPreguntaResuelta.listaOpcionesResueltas.length > 0){
              opcionSeleccionada.push(itemPreguntaResuelta.listaOpcionesResueltas[0].id);
              break;
            }
          }
        }
      }

      let preguntaUnicaRespuesta = new SelectOnlyQuestionAPS({
        key: pregunta.id,
        label: pregunta.nombrePregunta.trim(),
        value:opcionSeleccionada != null && opcionSeleccionada.length > 0 ? opcionSeleccionada[0]:null,
        options: this.obtenerRespuestasPregunta(listaOpciones, opcionSeleccionada),
        required: pregunta.obligatoria,
        order: pregunta.orden,
        pregunta: pregunta,
        showquestion: true,
        showlabel: pregunta.mostrarnombre,
        subtitle:pregunta.subtitulo
      });
      return preguntaUnicaRespuesta;

    } catch (error) {
      alert("Error armando pregunta unica respuesta. " + error);
    }

  }

  /**
   * Metodo que arma pregunta dinamica tipo numerica
   * @param pregunta
   * @param listPreguntasResueltas  
   * @author OM 
   */
  armarPreguntaNumerica(pregunta: FormularioPregunta, listPreguntasResueltas: PreguntaResuelta[]): TextBoxQuestionAPS {

    try {
      let valuePregunta: number = null;

      if (this.paramFormDinamicoGlobal != null &&
        this.paramFormDinamicoGlobal.formulariolleno === true) { //el formulario esta lleno
        for (const itemPreguntaResuelta of listPreguntasResueltas) {
          if (itemPreguntaResuelta.idpregunta === pregunta.id) {
            valuePregunta = itemPreguntaResuelta.respuestanumerica;
            break;
          }
        }
      }
      else { //formulario nuevo
        //valuePregunta = pregunta.valorpredeterminado != null && pregunta.valorpredeterminado.trim().length > 0 ? Number(pregunta.valorpredeterminado.trim()) : null;
      }

      let preguntaNumerica = new TextBoxQuestionAPS({
        key: pregunta.id,
        label: pregunta.nombrePregunta.trim(),
        value: valuePregunta,
        required: pregunta.obligatoria,
        order: pregunta.orden,
        pregunta: pregunta,
        showquestion: true,
        showlabel: pregunta.mostrarnombre,
        subtitle:pregunta.subtitulo
      });
      return preguntaNumerica;
    } catch (error) {
      alert("Error armando pregunta numerica. " + error);
    }
  }

  /**
   * Metodo que arma pregunta dinamica tipo logica
   * @param pregunta 
   * @param listPreguntasResueltas 
   * @author OM 
   */
  armarPreguntaLogica(pregunta: FormularioPregunta, listPreguntasResueltas: PreguntaResuelta[]): ToggleQuestionAPS {

    try {
      let valuePregunta: boolean = false ;
      if (this.paramFormDinamicoGlobal != null &&
        this.paramFormDinamicoGlobal.formulariolleno === true) { //el formulario esta lleno
        for (const itemPreguntaResuelta of listPreguntasResueltas) {
          if (itemPreguntaResuelta.idpregunta === pregunta.id) {
            valuePregunta = itemPreguntaResuelta.respuestalogica;
            break;
          }
        }
      }

      let preguntaLogica = new ToggleQuestionAPS({
        key: pregunta.id,
        label: pregunta.nombrePregunta.trim(),
        value: valuePregunta,
        required: pregunta.obligatoria,
        order: pregunta.orden,
        pregunta: pregunta,
        showquestion: true,
        showlabel: pregunta.mostrarnombre,
        subtitle:pregunta.subtitulo 
      });
      return preguntaLogica;
    } catch (error) {
      alert("Error armando pregunta logica. " + error);
    }

  }

  /**
   * Metodo que arma pregunta dinamica tipo fecha
   * @param pregunta
   * @param listPreguntasResueltas  
   * @author OM 
   */
  armarPreguntaFecha(pregunta: FormularioPregunta, listPreguntasResueltas: PreguntaResuelta[]): DateQuestionAPS {

    try {
      let valuePregunta: string = null;
      if (this.paramFormDinamicoGlobal != null &&
          this.paramFormDinamicoGlobal.formulariolleno === true) { //el formulario esta lleno
        
        for (const itemPreguntaResuelta of listPreguntasResueltas) {
          if (itemPreguntaResuelta.idpregunta === pregunta.id) {
            valuePregunta = itemPreguntaResuelta.respuestafecha;
            break;
          }
        }
      }
      let preguntaFecha = new DateQuestionAPS({
        key: pregunta.id,
        label: pregunta.nombrePregunta.trim(),
        value: valuePregunta,
        required: pregunta.obligatoria,
        order: pregunta.orden,
        pregunta: pregunta,
        showquestion: true,
        showlabel: pregunta.mostrarnombre,
        subtitle:pregunta.subtitulo
      });
      return preguntaFecha;
    } catch (error) {
      alert("Error armando pregunta fecha. " + error);
    }

  }



  /**
   * Metodo que arma pregunta dinamica tipo foto
   * @param pregunta 
   * @param listPreguntasResueltas 
   * @author OM 
   */
  armarPreguntaFoto(pregunta: FormularioPregunta, listPreguntasResueltas: PreguntaResuelta[]): PhotoQuestionAPS {

    try {
      let valuePregunta;
      if (this.paramFormDinamicoGlobal != null &&
        this.paramFormDinamicoGlobal.formulariolleno === true) { //el formulario esta lleno
        for (const itemPreguntaResuelta of listPreguntasResueltas) {
          if (itemPreguntaResuelta.idpregunta === pregunta.id) {
            valuePregunta = itemPreguntaResuelta.listImages;
            break;
          }
        }
      }

      let preguntaFoto = new PhotoQuestionAPS({
        key: pregunta.id,
        label: pregunta.nombrePregunta.trim(),
        //value: valuePregunta,
        required: pregunta.obligatoria,
        order: pregunta.orden,
        pregunta: pregunta,
        showquestion: true,
        showlabel: pregunta.mostrarnombre,
        subtitle:pregunta.subtitulo,
        listImages:valuePregunta
      });
      return preguntaFoto;
    } catch (error) {
      alert("Error armando pregunta foto. " + error);
    }

  }


  /**
   * Metodo que se encarga de obtener las respuestas (FormularioOpcion) y 
   * las convierte a un objecto tipo (key,value,selected)
   * @param listaOpciones
   * @param opcionesSeleccionadas  
   * @author OM 
   */
  obtenerRespuestasPregunta(listaOpciones: FormularioOpcion[], opcionesSeleccionadas: string[]) {

    try {
      let options: { key: string, value: string, selected: boolean }[] = [];
      if(listaOpciones != null  && listaOpciones.length > 0){
        for (let itemRespuesta of listaOpciones) {
          let seleccionada: boolean = false;
          if (opcionesSeleccionadas != undefined && opcionesSeleccionadas != null && opcionesSeleccionadas.length > 0) {//se verifican las opcion seleccionada
            for (let opcionSelec of opcionesSeleccionadas) {
              if (opcionSelec.trim() === itemRespuesta.id.trim()) {
                seleccionada = true;
              }
            }
          }
          let opcion = {
            key: itemRespuesta.id.trim(),
            value: itemRespuesta.nombreOpcion.trim(),
            color:itemRespuesta.color,
            selected: seleccionada
          }
          options.push(opcion);
        }
        //se ordenan en orden alfabetico.
        options.sort(function (a, b) { return (a.key > b.key) ? 1 : ((b.key > a.key) ? -1 : 0); });
      }
      return options;
    } catch (error) {
      alert("Error obteniendo respuestas preguntas. " + error);
    }
  }

}




