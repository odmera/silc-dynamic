import { Injectable } from '@angular/core';
import { UsuarioDTO, PerfilUsuario } from '../../modelos/UsuarioDTO';
import { AuntenticacionFirebaseProvider } from '../auntenticacion-firebase/auntenticacion-firebase';
import { AlmacenamientoLocalProvider } from '../almacenamiento-local/almacenamiento-local';
import { datosAL } from '../../modelos/datosAL';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { ModeloFirebase } from '../../modelos/ModeloFirebase';
import { Md5 } from 'ts-md5/dist/md5';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UsuarioProvider {

  collection: AngularFirestoreCollection<UsuarioDTO>;

  constructor(
    public _autenticacionFirebaseProvider: AuntenticacionFirebaseProvider,
    public _almacenamientoLocalProvider: AlmacenamientoLocalProvider,
    public afDB: AngularFirestore) {
    this.collection = this.afDB.collection(`/${ModeloFirebase.USUARIOS}`);
  }

  obtenerRef(id) {
    return this.afDB.doc<UsuarioDTO>(`${ModeloFirebase.USUARIOS}/${id}`);
  }

  async registrarUsuario(usuario: UsuarioDTO): Promise<boolean> {

    try {
      //se registra el usuario con autenticacion en firebase
      let registroUsuarioCorrecto: boolean =
        await this._autenticacionFirebaseProvider.crearUsuarioConEmail(usuario.correo, usuario.contrasena);

      if (registroUsuarioCorrecto == true){
        //se registra el usuario
        let usuarioGuarado = await this.crearUsuario(usuario);

        //guardo los datos en el almacenamiento local
        let datosal = new datosAL();
        datosal.idUsuario = usuarioGuarado.id;
        datosal.correoUsuario = usuarioGuarado.correo;
        datosal.perfilUsuario = PerfilUsuario._pefil_usuario.toString(); //perfil de cliente
        datosal.nombreCompleto = usuarioGuarado.nombreCompleto;
        await this._almacenamientoLocalProvider.guardarDatosAL(datosal);

        return true;
      }
      
    } catch (err) {
      throw new Error(err);
    }
  }

  async crearUsuario(usuario: UsuarioDTO): Promise<UsuarioDTO> {
    usuario.contrasena = Md5.hashStr(usuario.contrasena).toString();
    let documento = await this.collection.add(usuario);
    usuario.id = documento.id;
    await this.actualizarUsuario(usuario);
    return usuario;
  }

  async actualizarUsuario(usuario: UsuarioDTO){
    await this.obtenerRef(usuario.id).update(usuario);
  }

  consultarUsuarioPorEmail(correo: string): Observable<UsuarioDTO[]> {
    return this.afDB.collection(`${ModeloFirebase.USUARIOS}`, ref => ref.where('correo', '==', correo)).snapshotChanges().map(actions => {
      return actions.map(a => {
        return { id: a.payload.doc.id, ...a.payload.doc.data() } as UsuarioDTO;
      });
    });
  }

  /**
   * metodo que permite consultar las solicitudes de los clientes 
   * (sucede cuando un cliente se registra)
   */
  obtenerSolicitudes() {
    return this.afDB.collection(`${ModeloFirebase.USUARIOS}`, ref => 
      ref.where('perfil', '==', PerfilUsuario._pefil_usuario)
         .where('numeroCliente', '==', null))
       
      .snapshotChanges().map(actions => {
        return actions.map(a => {
          return { id: a.payload.doc.id, ...a.payload.doc.data() } as UsuarioDTO;
      })
    })
  }

  /**
   * metodo que consulta los usuario  
   */
  consultarUsuarios(): Promise<UsuarioDTO[]> {
    return new Promise((resolve, reject) => {
      return this.afDB.collection(ModeloFirebase.USUARIOS, ref =>
        ref.where('perfil', '==', PerfilUsuario._pefil_usuario)).snapshotChanges().map(actions => {
          return actions.map(a => {
            return { id: a.payload.doc.id, ...a.payload.doc.data() } as UsuarioDTO;
          });
        }).subscribe(datos => {
          resolve(datos)
        });
    });
  }

  /**
   * Consultar Clientes Aprodados
   */
  consultarClientesAprobados(): Promise<UsuarioDTO[]> {
    return new Promise((resolve, reject) => {
      return this.afDB.collection(ModeloFirebase.USUARIOS, ref => 
        ref.where('perfil', '==', PerfilUsuario._pefil_usuario)
           .where('numeroCliente', '>', 0)).snapshotChanges().map(actions => {
          return actions.map(a => {
            return { id: a.payload.doc.id, ...a.payload.doc.data() } as UsuarioDTO;
          });
        }).subscribe(datos => {
          resolve(datos)
        });
    });
  }

  async crearUsuarioVendedor(usuario: UsuarioDTO): Promise<boolean> {

    try {
      //se registra el usuario con autenticacion en firebase
      let registroUsuarioCorrecto: boolean =
        await this._autenticacionFirebaseProvider.crearUsuarioConEmail(usuario.correo, usuario.contrasena);

      if (registroUsuarioCorrecto == true) {
        //se registra el usuario
        let usuarioGuarado = await this.crearUsuario(usuario);

        //guardo los datos en el almacenamiento local
        return true;
      }

    } catch (err) {
      throw new Error(err);
    }
  }

  /**
   * se registra un usuario apartir de las visitas online de los vendedores
   */
  async registrarUsuarioDesdeVisitas(usuario: UsuarioDTO): Promise<boolean> {

    try {
      //se registra el usuario con autenticacion en firebase
      let registroUsuarioCorrecto: boolean =
        await this._autenticacionFirebaseProvider.crearUsuarioConEmail(usuario.correo, usuario.contrasena);

      if (registroUsuarioCorrecto == true) {
        //se registra el usuario
        let usuarioGuarado = await this.crearUsuario(usuario);

        //se envia correo al usuario para que cambie su contraseña.
        await this._autenticacionFirebaseProvider.restrablecerContrasena(usuario.correo);
        
        return true;
      }

    } catch (err) {
      throw new Error(err);
    }
  }
}
