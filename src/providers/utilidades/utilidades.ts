import { Injectable } from '@angular/core';
import { AlertController, LoadingController, ToastController, Platform } from 'ionic-angular';

// Manejo de imagenes
import { ImagePDBInterface } from '../../modelos/interface/imageinterface/ImagePDBInterface';
import { AttachmentImage } from '../../modelos/interface/imageinterface/AttachmentImage';
import { ImageProperties } from '../../modelos/interface/imageinterface/ImageProperties';
import { Foto } from '../../modelos/Foto';
import { Network } from '@ionic-native/network';
// Manejo de imagenes


@Injectable()
export class UtilidadesProvider {

  constructor(public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public platform:Platform,
    private network: Network) {
  }

  doAlert(title, mensaje) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: mensaje,
      buttons: ['Ok']
    });
    return alert;
  }

  presentarLoading(mensaje: string) {
    let loading = this.loadingCtrl.create({
      content: mensaje
    });
    return loading;
  }

  presentarToast(mensaje) {
    let toast = this.toastCtrl.create({
      message: mensaje,
      duration: 3000,
      cssClass: 'dark-trans',
      closeButtonText: 'OK',
      showCloseButton: true
    });
    toast.present();
    return toast;
  }

  guid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
  }

  download(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }

  public convertirFechaUnixtime(fecha: Date): number {
    return Number(Math.floor(fecha.getTime() / 1000).toString());
  }

  public convertirUnixtimeFechaUnixtime(unixtime: number): Date {
    return new Date(unixtime * 1000);
  }

  public validarBoolean(booleanObject: any) {
    let objRes: boolean = false;
    if (booleanObject != null && booleanObject !== undefined) {
      if (typeof (booleanObject) === "boolean") {
        objRes = booleanObject;
        return objRes;
      } else {
        console.error('utilidades/validarBoolean: object is not boolean');
      }
    } else if (booleanObject == null) {
      return objRes;
    } else {
      console.error('utilidades/validarBoolean: undefined object');
    }

  }

  public validarString(stringObject: any) {
    let objRes: string = '';
    if (stringObject != null && stringObject !== undefined) {
      if (typeof (stringObject) === "string") {
        objRes = stringObject;
        return objRes;
      } else {
        console.error('utilidades/validarString: object is not string');
      }
    } else if (stringObject == null) {
      return objRes;
    } else {
      console.error('utilidades/validarString: undefined object');
    }

  }

  public validarNumber(numberObject: any) {
    let objRes: number = 0;
    if (numberObject != null && numberObject !== undefined) {
      if (typeof (numberObject) === "number") {
        objRes = numberObject;
        return objRes;
      } else {
        console.error('utilidades/validarNumber: object is not number');
      }
    } else if (numberObject == null) {
      return objRes;
    } else {
      console.error('utilidades/validarNumber: undefined object');
    }

  }

  // Convierte un array de imagenes a un objeto válido para Pouch
  convertirArrayObjetoValidoPouch(arrayFotos: Array<Foto>) {
    
    let fotosCapturadas:object[] = [];
    arrayFotos.forEach(element => {

      let imageProperties: ImageProperties={
        contentType : 'image/png',
        data : element.foto,
      }

      let attachImage: AttachmentImage ={
        name : element.nombreFoto,
        imageProperties:imageProperties
      }
      
      let objectImage: ImagePDBInterface = {
        _id : element.nombreFoto.toString(),
        _attachments:attachImage
      }
      console.info(objectImage);
    
      fotosCapturadas.push(objectImage);
    });
    return fotosCapturadas;
    
  }


  
  /**
   * metodo que permite validar si estoy en la app o en el navegador
   */
  isApp():boolean{
    let isApp = true;
    if(this.platform.is('core') || this.platform.is('mobileweb')) {
      isApp = false;
    } else {
      isApp = true;
    }
    return isApp;
  }

  /**
   * metodo que verifca si el telefono tiene internet
   * @author OM
   */
  isConect(): boolean {
    if (this.platform.is('cordova')) {
      let conntype = this.network.type;
      let conectado = conntype && conntype !== 'unknown' && conntype !== 'none'

      if(conectado == true && this.network.type === 'wifi'){
        return true;
      }
      else{
        return false;
      }
    }
    else{ //en el navegador.
      return navigator.onLine;
    }
  }

  isValidUrl(string){
    try {
      new URL(string);
      return true;
    } catch (_) {
      return false;  
    }
  }

  getImageSize(data_url) {
    var head = 'data:image/jpeg;base64,';
    return ((data_url.length - head.length) * 3 / 4 / (1024*1024)).toFixed(4);
  }

  objToStrMap(obj) {
    let strMap = new Map();
    for (let k of Object.keys(obj)) {
        strMap.set(k, obj[k]);
    }
    return strMap;
  }

  /**
   * 
   * @param idClub metodo que permite armar key para los registros de las preguntas en Pouchdb
   * @param idFormulario 
   */
  armarIdentificadorPouchdb(idClub,idFormulario,idPregunta){
    try {
      let key =`${idClub.trim()}${idFormulario.trim()}${idPregunta.trim()}`
      return key;
    } catch (error) {
      alert("ocurrio algo armando key");
    }
  }
  
}
