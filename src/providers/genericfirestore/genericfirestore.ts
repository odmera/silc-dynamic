import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { ModeloFirebase } from '../../modelos/ModeloFirebase';
import { PreguntaResuelta } from '../../modelos/PreguntaResuelta';

@Injectable()
export class GenericFirestoreProvider {

  constructor(private afDB: AngularFirestore) {
  }

  private obtenerRef(id,objectDbName){
    return this.afDB.doc<any>(`${objectDbName}/${id}`);
  }

  /**
   * permite guarda un documento, luego se actualizada con el id generado por firebase
   * @param item
   * @param objectDbName 
   */
  async addRecordFirestore(item:any,objectDbName:string): Promise<string> {
    try {
       let collection:AngularFirestoreCollection<any> = this.afDB.collection(`/${objectDbName}`);
       let document = await collection.add(item);
       item.id = document.id;
       await this.updateRecordFirestore(item,objectDbName);
       return item.id;
    } catch (error) {
      throw new Error(error);
    }
  }

  async updateRecordFirestore(item:any,objectDbName:string): Promise<string> {
    try {

      /*let db = this.afDB.firestore;
      let batch = db.batch();

      let ref = db.doc(`${objectDbName}/${item.id}`);
      if(item.id != null){
          batch.update(ref, item);
      }
      await batch.commit();*/

      let refCriterio = this.afDB.doc(`${objectDbName}/${item.id}`);
      await refCriterio.update(item);
      //await this.obtenerRef(item.id,objectDbName).update(item);
       return item.id;
    } catch (error) {
      throw new Error(error);
    }
  }


  async addOrUpdateRecordFirestore(item:any,objectDbName:string): Promise<string> {
    try {
      let id:string = null;
      if (item.id == null) { //se guarda
        id = await this.addRecordFirestore(item,objectDbName);
      }
      else { //se actualiza
        id = await this.updateRecordFirestore(item,objectDbName);
      }
      return id;
    } catch (error) {
      throw new Error(error);
    }
  }


  /**
   * metodo que permite consultar todos los datos de una "tabla"
   * @param objectDbName 
   */
  findRecordFirestore(objectDbName:string): Promise<any[]> {
    return new Promise((resolve, reject) => {
      return this.afDB.collection(`${objectDbName}`).snapshotChanges().map(actions => {
          return actions.map(a => {
            const data = a.payload.doc.data() as any;
            data.id = a.payload.doc.id;
            return data;
          });
        }).subscribe(datos => {
          resolve(datos)
        });
    });
  }

 /**
   * metodo que permite consultar un documento por id
   * @param id 
   * @param objectDbName 
   */
  findRecordPropertyFirestore(campo:string,valor: string,objectDbName:string): Promise<any> {

    return new Promise((resolve, reject) => {
      return this.afDB.collection(objectDbName, ref =>
      
        ref.where(campo, '==', valor)).snapshotChanges().map(actions => {
          return actions.map(a => {
            const data = a.payload.doc.data() as any;
            data.id = a.payload.doc.id;
            return data;
          });
        }).subscribe(datos => {
          if(datos != null && datos.length > 0){
            resolve(datos[0])
          }
          else{
            resolve(null);
          }
        });
    });
  }

  /**
   * 
   * @param campo 
   * @param valor 
   * @param objectDbName 
   */
  findRecordPropertyFirestoreList(campo:string,valor: string,objectDbName:string): Promise<any> {
    
    return new Promise((resolve, reject) => {
      return this.afDB.collection(objectDbName, ref =>
      
        ref.where(campo, '==', valor)).snapshotChanges().map(actions => {
          return actions.map(a => {
            const data = a.payload.doc.data() as any;
            data.id = a.payload.doc.id;
            return data;
          });
        }).subscribe(datos => {
          if(datos != null && datos.length > 0){
            resolve(datos)
          }
          else{
            resolve(null);
          }
        });
    });
  }

  /**
   * metodo que permite encontrar los registros que han cambiados despuesta de la ultima sincronizacion
   * @param dateLastSync  --> fecha de la ultima sincronizacion.
   * @param objectDbName 
   */
  findRecordLastSync(dateLastSync:number,objectDbName:string): Promise<any[]> {
    return new Promise((resolve, reject) => {
      return this.afDB.collection(objectDbName, ref =>
        ref.where('fecha', '>=',dateLastSync)).snapshotChanges().map(actions => {
          return actions.map(a => {
            const data = a.payload.doc.data() as any;
            data.id = a.payload.doc.id;
            return data;
          });
        }).subscribe(datos => {
          resolve(datos)
        });
    });
  }


  /**
   *  permite guarda multiples documentos
   * @param items
   * @param objectDbName 
   */
  async addOrUpdateRecordFirestoreMultiple(items:any[],objectDbName:string){

    try {

     //referecias de la bd
     let db = this.afDB.firestore;
     let batch =db.batch()

     for (let item of items) {

       if(item.id == null){
        let collectionTransaccion = db.collection(`/${objectDbName}`);
        let documento = await collectionTransaccion.add(item);
        item.id= documento.id;
        let ref = db.doc(`${objectDbName}/${documento.id}`);
        batch.update(ref, item);
       }else{
        let ref = db.doc(`${objectDbName}/${item.id}`);
        batch.update(ref, item);
       }
 
     }

     //commit
     await batch.commit();
     return true;
 
    } catch (error) {
      alert(`metodo updateRecordFirestoreMultiple ${objectDbName} error: ${error}`);
      throw new Error();
    }
 }




 /******************************************* metodo especificos  ****************************************/

  ////////metodos FORMULARIORESPUESTA
  /**
   * 
   * @param idclub metodo que permite cargar las preguntas resueltas por club y formulario
   * @param idformulario 
   */
  public cargarPreguntasResueltas(idclub: string, idformulario:string): Promise<PreguntaResuelta[]> {
    return new Promise((resolve, reject) => {

      return this.afDB.collection(ModeloFirebase.FORMULARIORESPUESTA, ref =>
        ref.where('idclub', '==', idclub).where('idformulario', '==', idformulario)).snapshotChanges().map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as PreguntaResuelta;
          data.id = a.payload.doc.id;
          return data;
        });
      }).subscribe(datos => {
        resolve(datos)
      });
    });
  }

  /**
   * 
   * @param idclub metodo que permite cargar las preguntas resueltas por club
   */
  public cargarPreguntasResueltasPorClub(idclub: string): Promise<PreguntaResuelta[]> {
    return new Promise((resolve, reject) => {

      return this.afDB.collection(ModeloFirebase.FORMULARIORESPUESTA, ref =>
        ref.where('idclub', '==', idclub)).snapshotChanges().map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as PreguntaResuelta;
          data.id = a.payload.doc.id;
          return data;
        });
      }).subscribe(datos => {
        resolve(datos)
      });
    });
  }



   /*************************************Termina metodo especificos ****************************************/
}
