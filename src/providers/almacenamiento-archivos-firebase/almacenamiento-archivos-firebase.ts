
import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase';

@Injectable()
export class AlmacenamientoArchivosFirebaseProvider {

  private CARPETA_IMAGENES: string = "Imagenes";

  constructor(public afDB: AngularFireDatabase) {
  }

  guardarFotoFirebase(imagenFirebase: string, nombreArchivo: string,contentType: string = 'image/jpeg' ): Promise<string> {

    let promesa = new Promise<string>((resolve, reject) => {

      if (imagenFirebase == undefined || imagenFirebase == null || imagenFirebase == '') {
        resolve(null);
        return promesa;
      }
      console.log("Inicio de carga imagen");

      if(contentType === 'application/pdf'){ //
        this.CARPETA_IMAGENES = "pdf"
      }

      let storageRef = firebase.storage().ref();
      let uploadTask: firebase.storage.UploadTask = storageRef.child(`${this.CARPETA_IMAGENES}/${nombreArchivo}`)
        .putString(imagenFirebase, 'base64', { contentType: contentType });

      uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
        (snapshot) => { }, // saber el avance del archivo
        (error) => {  // Manejo de errores

          var errorCode = error['code_'];
          var mensajeError = error['message_'];
          switch (errorCode) {
            case 'storage/invalid-argument':
              mensajeError = "Imagen invalida, no se fue posible guardar la imagen";
              break;
            case 'storage/unauthorized':
              mensajeError = "El usuario no tiene permiso, no se fue posible guardar la imagen";
              break;
            case 'storage/canceled':
              mensajeError = "El usuario canceló la subida de la imagen";
              break;
            case 'storage/unknown':
              mensajeError = "Error desconocido subiendo la imagen";
              break;
          }
          console.log("error gradando archivo ", mensajeError);
          alert("metodo guardarFotoFirebase, error gradando archivo " +  mensajeError);
          reject(mensajeError);
        }, () => { // Termino el proceso
          let url: string = uploadTask.snapshot.downloadURL;
          resolve(url);
        }
      )
    });
    return promesa;
  }

}
