import { Injectable } from '@angular/core';
import { ModeloFirebase } from '../../modelos/ModeloFirebase';
import { AngularFirestore } from 'angularfire2/firestore';
import { UtilidadesProvider } from '../utilidades/utilidades';
import { Club } from '../../modelos/Club';
import { GenericpouchdbProvider } from '../genericpouchdb/genericpouchdb';
import { ModelNamesSqlLiteEnum } from '../../modelos/enum/ModelNamesSqlLiteEnum';
import { GenericFirestoreProvider } from '../genericfirestore/genericfirestore';
import { AlmacenamientoLocalProvider } from '../almacenamiento-local/almacenamiento-local';
import { datosAL } from '../../modelos/datosAL';
import { Sincronizacion } from '../../modelos/Sincronizacion';
import { FirebaseFirestore, WriteBatch, CollectionReference } from '@firebase/firestore-types';
import { CampoDeJuegoMedidas } from '../../modelos/CampoDeJuegoMedidas';
import { ApiProvider } from '../api/api';
import { SincronizacionApi } from '../../modelos/SincronizacionApi';
import { ImagePDBInterface } from '../../modelos/interface/imageinterface/ImagePDBInterface';
import { AlmacenamientoArchivosFirebaseProvider } from '../almacenamiento-archivos-firebase/almacenamiento-archivos-firebase';
import { Division } from '../../modelos/Division';
import { Formulario } from '../../modelos/Formulario';
import { PreguntaResuelta } from '../../modelos/PreguntaResuelta';

@Injectable()
export class SincronizarDatosProvider {

  private db:FirebaseFirestore;
  private batch:WriteBatch;
  private idClubFirestore:string;
  private idClubPouchd:string;

  constructor(public afDB: AngularFirestore,
              public _genericpouchdbProvider: GenericpouchdbProvider,
              public _genericFirestoreProvider: GenericFirestoreProvider,
              public _util: UtilidadesProvider,
              public _almacenamientoLocalProvider: AlmacenamientoLocalProvider,
              public _apiProvider:ApiProvider,
              public _almacenamientoArchivosFirebaseProvider: AlmacenamientoArchivosFirebaseProvider ) {

  }

  /** 
   * Metodo que permite sincronizar a firebase. 
   */
  async sincronizarDatos(idClubFirestore:string, idClubPouchd:string,sincronizacion:Sincronizacion,divisionClub:Division){

    try {

      let datosAl: datosAL = await this._almacenamientoLocalProvider.obtenerDatosSesion();
      if (datosAl && datosAl.idUsuario) {

        this.idClubFirestore = idClubFirestore;
        this.idClubPouchd = idClubPouchd;
        
        //referecias de la bd
        this.db = this.afDB.firestore;
        this.batch = this.db.batch();

        /////////////////club (solo se actualizan)////////////////////////////////////////////
        await this._genericpouchdbProvider.constructorToObjectDb(ModelNamesSqlLiteEnum.CLUB.toString());
        let itemClub :Club = await this._genericpouchdbProvider.getRecord(idClubPouchd) as Club;   
        let ref = this.db.doc(`${ModeloFirebase.CLUB}/${itemClub.id}`);
        if(itemClub.id != null){
          this.batch.update(ref, itemClub);
        }          

        1////////////////PREGUNTAS RESUELTAS ////////////////////////////////////////////
        let listPreguntasResueltasLocal:PreguntaResuelta[] = await this.enviarDatosFormulariosResueltosClub();
        //console.log(JSON.stringify(listPreguntasResueltasLocal));


        2////////////////CAMPO DE JUEGO MEDIDAS////////////////////////////////////////////
        // se limpia la imagen de las medidas para que no se guarde en firebase, pero si se envia a la api 
        await this._genericpouchdbProvider.constructorToObjectDb(ModelNamesSqlLiteEnum.CAMPODEJUEGOMEDIDA.toString());
        let itemLocalMedidasCampoJuego = await this._genericpouchdbProvider.getRecord(this.idClubFirestore);
        let base64ImagenCampoJuego:string = null;
        if(itemLocalMedidasCampoJuego != null){ 
          base64ImagenCampoJuego = itemLocalMedidasCampoJuego.imagencampojuego;
          itemLocalMedidasCampoJuego.imagencampojuego = null;
          //se actualizado el item localmente.
          let datorespuesta = await this._genericpouchdbProvider.putRecord(itemLocalMedidasCampoJuego);
        }

        let campoDeJuegoMedidas:CampoDeJuegoMedidas = await this.enviarDatosMedidasCampoJuego(ModeloFirebase.CAMPODEJUEGOMEDIDA,ModelNamesSqlLiteEnum.CAMPODEJUEGOMEDIDA.toString())
        
        //como se limpio la imagen para no enviar a firebase, entonces se vuelvo a llenar el item en el almacenamiento local
        await this._genericpouchdbProvider.constructorToObjectDb(ModelNamesSqlLiteEnum.CAMPODEJUEGOMEDIDA.toString());
        let itemLocalMedidasCampoJuegoActualizar = await this._genericpouchdbProvider.getRecord(this.idClubFirestore);
        if(itemLocalMedidasCampoJuegoActualizar != null){ 
          itemLocalMedidasCampoJuegoActualizar.imagencampojuego = base64ImagenCampoJuego;
          //se actualizado el item localmente.
          let datorespuesta = await this._genericpouchdbProvider.putRecord(itemLocalMedidasCampoJuegoActualizar);
        }

        //se pone la imagen para que viaje a la api
        if(campoDeJuegoMedidas != null){
           campoDeJuegoMedidas.imagencampojuego =  base64ImagenCampoJuego != null && base64ImagenCampoJuego.length > 0 ? base64ImagenCampoJuego.split(',')[1]: null; //quito la cadena data:image/jpg;base64,
        }
        //Commit (termina enviado datos a firebase)
        await this.batch.commit();

        ///////////////se guardo el nodo de sincronizacion///////////////////////////////////
        let collectionSincronizacion= this.db.collection(`/${ModeloFirebase.SINCRONIZACION}`);
        let documentoSincronizacion = await collectionSincronizacion.add(sincronizacion);
        sincronizacion.id = documentoSincronizacion.id;

        //se elimina el dato de sincronizacion localmente
        await this._genericpouchdbProvider.constructorToObjectDb(ModelNamesSqlLiteEnum.SINCRONIZACION.toString());
        await this._genericpouchdbProvider.deleteRecord(sincronizacion);

        ////quito la cadena data:image/jpg;base64, de la firma
        sincronizacion.firma = sincronizacion.firma.split(',')[1];

        let formulariosOrganizados = await this.organizarFormulario(divisionClub);
        
        //se consultan los formularios para enviar al pdf
        let dataFormulariosFinalesResueltos = await this.prepararFormulariosResueltosPDF(listPreguntasResueltasLocal,formulariosOrganizados);
 
        let sincronizacionApi :SincronizacionApi = {
          campoDeJuegoMedidas:campoDeJuegoMedidas,
          listFormularios:dataFormulariosFinalesResueltos,
          sincronizacion:sincronizacion
        }
        let respuesta = {valido:true,datosPdf:sincronizacionApi}
        return respuesta;
      }
    } catch (error) {
      throw error;
    }
  }

  async enviarDatosFormulariosResueltosClub(){

    let loading = this._util.presentarLoading(`Sincronizando...`);

    //console.log("idClubFirestore " , this.idClubFirestore);
    try {
      await loading.present();
  
      await this._genericpouchdbProvider.constructorToObjectDb(ModelNamesSqlLiteEnum.FORMULARIORESPUESTA.toString());
      //se crea el indice ['idclub']
      let indexformulariorespuestaidclub = await this._genericpouchdbProvider.createIndex('indexformulariorespuestaidclub',['idclub']);

      //se obtienen todos los  items locales
      let listPreguntasResueltasLocal = await this._genericpouchdbProvider.findRecordPropertys({idclub: this.idClubFirestore},'indexformulariorespuestaidclub') as  PreguntaResuelta[];

      //se consultas las preguntas resueltas en firebase (por idclub)
      let listPreguntasResueltasFirebase = await this._genericFirestoreProvider.cargarPreguntasResueltasPorClub(this.idClubFirestore);
      
      if(listPreguntasResueltasLocal != null && listPreguntasResueltasLocal.length > 0){
        for (let itemLocal of listPreguntasResueltasLocal) {
        
          let collectionEnvio = this.db.collection(`/${ModeloFirebase.FORMULARIORESPUESTA}`);
          if(itemLocal != null){ // hay datos almacenados localmente
            if(itemLocal.id != null){ //si existe en firebase y el usuario ha obtenido los datos de los formularios, se actualiza
              
              //se actualiza la pregunta que ya estaba guardada en firebase. CON ID
              let refCriterio = this.db.doc(`${ModeloFirebase.FORMULARIORESPUESTA}/${itemLocal.id}`);
              this.batch.update(refCriterio, itemLocal);
            } 
            else{
  
              //se busca el item local en los items de firebase para obtener el idfirebase y enviar a actualizar
              if(listPreguntasResueltasFirebase != null && listPreguntasResueltasFirebase.length > 0){
                let itemEncontrado = false;
                for (let itemFirebase of listPreguntasResueltasFirebase) {
                  //se actualiza la pregunta que ya estaba guardada en firebase. SIN idfirebase localmente
                  if(itemFirebase.idpregunta === itemLocal.idpregunta){
                    itemLocal.id = itemFirebase.id;
                    let refCriterio = this.db.doc(`${ModeloFirebase.FORMULARIORESPUESTA}/${itemLocal.id}`);
                    this.batch.update(refCriterio, itemLocal);
                    itemEncontrado = true;
                    break;
                  } 
                }
                //nunca se ha guardado la pregunta en firebase
                if(itemEncontrado ===false){
                  await this.guardarItemLocalEnFirebase(itemLocal,collectionEnvio);
                }
              }
              else{ //si no hay items en firebase
                //console.log('no hay  items en firebase');
                await this.guardarItemLocalEnFirebase(itemLocal,collectionEnvio);
              }
            } 
          }
        }
      }
      
      return listPreguntasResueltasLocal;
    } catch (error) {
      alert(`metodo enviarDatosFormulariosResueltosClub error: ${error}`);
      throw error;
    }
    finally{
      loading.dismiss();
    }
  }


  /**
   * metodo que permite organizar los formulario para enviar a la api
   * @param divisionClub
   */
  async organizarFormulario(divisionClub :Division){
    let formulariosOrganizados:Formulario[] = [];
    if(divisionClub != null && divisionClub.listaFormularios.length > 0){
      //se consulta todos los formulario para compararlos con los formularios de las diviciones
      await this._genericpouchdbProvider.constructorToObjectDb(ModelNamesSqlLiteEnum.FORMULARIO.toString());
      let dataFormularios = await this._genericpouchdbProvider.findRecord() as Formulario[];
      if (dataFormularios != null && dataFormularios.length > 0) {
        for (let formularioDivicion of divisionClub.listaFormularios) {
          if(formularioDivicion.checked ===true){
            let formularioEncontrado = this.encontrarFormulario(dataFormularios,formularioDivicion.id);
            if(formularioEncontrado != null){
              if(formularioEncontrado.agrupaFormulario == true){ //agrupa otros formularios, se buscan sus formularios
 
                let nuevosFormulariosAgrupado:Formulario[] = [];
                for (let formAgrupado of formularioEncontrado.formulariosAgrupacion){          
                  let formulaarioNuevoAgrupacion =  this.encontrarFormulario(dataFormularios,formAgrupado.id);
                  //verificar si el formulario esta chekeado en la lista de los formulario de diviciones alli se determinan los checkeados
                 
                  //busco los formulario de agrupacion en las diviciones
                  for (let formAgrupadoDivicion of divisionClub.listaFormularios) {
                    if(formAgrupadoDivicion.agrupaFormulario == true){ 
                       let forNuevoDivicion = this.encontrarFormulario(formAgrupadoDivicion.formulariosAgrupacion,formulaarioNuevoAgrupacion.id)
                        if(forNuevoDivicion  != null && forNuevoDivicion.checked ==true){
                          nuevosFormulariosAgrupado.push(formulaarioNuevoAgrupacion);
                        }
                    }
                  }
                }
                //se reemplazo los formularios por los nuevos
                formularioEncontrado.formulariosAgrupacion = null;
                formularioEncontrado.formulariosAgrupacion= nuevosFormulariosAgrupado;
                formulariosOrganizados.push(formularioEncontrado);
              }
              else{
                //se valida si el formulario esta agrupado en algun otro, si esta, no se agrega a la lista 
                let estaContenido:boolean = false
                for (let formularioDivicionValidacion of divisionClub.listaFormularios) {
                  if(formularioDivicionValidacion.agrupaFormulario == true && 
                    formularioDivicionValidacion.formulariosAgrupacion != null && 
                    formularioDivicionValidacion.formulariosAgrupacion.length > 0){
                    let formularioReEncontrado =  this.encontrarFormulario(formularioDivicionValidacion.formulariosAgrupacion,formularioEncontrado.id);
                    if(formularioReEncontrado != null){
                      estaContenido = true;
                      break;
                    }
                  }
                }
                if(estaContenido == false){
                  formulariosOrganizados.push(formularioEncontrado);
                }
              }
            }
          }
        }
      }
    }
    return formulariosOrganizados;
  }

  encontrarFormulario(dataFormularios:Formulario[],idformularioEncontrar:string){
    let formularioEncontrado:Formulario = null;
    if(dataFormularios != null && dataFormularios.length > 0){
      for (let item of dataFormularios) {
        if(item.id === idformularioEncontrar){
          formularioEncontrado = item
          break; 
        }
      }
    }
    return formularioEncontrado;
  }



  /**
   * metodo que prepara los formulario resueltos, toca cada preguntas y le busca su respuesta
   * @param listPreguntasResueltasLocal
   * @param formulariosOrganizados 
   */
  async prepararFormulariosResueltosPDF(listPreguntasResueltasLocal:PreguntaResuelta[],formulariosOrganizados:Formulario[]){

    try {
      if (formulariosOrganizados != null && formulariosOrganizados.length > 0) {

        //se ordena de manera alfabetica.
        formulariosOrganizados.sort(function(a,b) {return (a.nombreFormulario > b.nombreFormulario) ? 1 : ((b.nombreFormulario > a.nombreFormulario) ? -1 : 0);} );
        for (let itemFormulario of formulariosOrganizados) {

          if(itemFormulario.agrupaFormulario == true && itemFormulario.formulariosAgrupacion.length > 0){ //formulario agrupa otros
           //se llama el mismo metodo para que haga el proceso el los formularios dentro 
           await this.prepararFormulariosResueltosPDF(listPreguntasResueltasLocal,itemFormulario.formulariosAgrupacion);
          }
          else{
            if(itemFormulario.listaBloques != null && itemFormulario.listaBloques.length > 0) {
              //se ordenan los bloques
              itemFormulario.listaBloques.sort((a, b) => a.orden - b.orden);
              for (let itemBloque of itemFormulario.listaBloques) {
                if(itemBloque.habilitado === false){ //no se gestiona el bloque
                  continue;
                }
                if (itemBloque.listaPreguntas != null && itemBloque.listaPreguntas.length > 0) {
                  //se ordenan las preguntas
                  itemBloque.listaPreguntas.sort((a, b) => a.orden - b.orden);
                  for (let pregunta of itemBloque.listaPreguntas) {
                    if(pregunta.habilitado === false){ //no se gestiona la pregunta
                      continue;
                    }
                    
                    //se busca la respuesta de cada pregunta
                    pregunta.PreguntaResuelta = null;
                    if(listPreguntasResueltasLocal != null && listPreguntasResueltasLocal.length > 0){
                      for (let itemPreguntaResuelta of listPreguntasResueltasLocal) {
                        if(itemPreguntaResuelta.idpregunta === pregunta.id) {
                          pregunta.PreguntaResuelta = itemPreguntaResuelta;
                          break;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        } 
      }
      return formulariosOrganizados;
    } catch (error) {
      alert(`metodo prepararFormularioPDF  error: ${error}`);
      throw error;;
    }
  
  }


  async guardarItemLocalEnFirebase(itemLocal:PreguntaResuelta,collectionEnvio: CollectionReference){
    let documento = await collectionEnvio.add(itemLocal);
    itemLocal.id= documento.id;
    
    let ref = this.db.doc(`${ModeloFirebase.FORMULARIORESPUESTA}/${documento.id}`);
    this.batch.update(ref, itemLocal);
    //se actualizado el dato localmente con el id nuevo de firebase
    //(para simula que es un dato traido desde firebase)
    itemLocal._id = this._util.armarIdentificadorPouchdb(itemLocal.idclub,itemLocal.idformulario,itemLocal.idpregunta);
    let datorespuesta = await this._genericpouchdbProvider.putRecord(itemLocal);
  }


  /**
   * Metodo que envia datos de medidas del campo de juego
   * @param tablaFirebase
   * @param tablaPouchdb 
   */
  async enviarDatosMedidasCampoJuego(tablaFirebase:string,tablaPouchdb:string){

    let loading = this._util.presentarLoading(`Sincronizando: ${tablaFirebase}...`);

     try {
       await loading.present();
       await this._genericpouchdbProvider.constructorToObjectDb(tablaPouchdb);

       //se toman lo datos locales
       let itemLocal = await this._genericpouchdbProvider.getRecord(this.idClubFirestore);

       let collectionEnvio = this.db.collection(`/${tablaFirebase}`);

       if(itemLocal != null){ // hay datos almacenados localmente
         if(itemLocal.id != null){ //si existe en firebase, se actualiza
           let refCriterio = this.db.doc(`${tablaFirebase}/${itemLocal.id}`);
           this.batch.update(refCriterio, itemLocal);
         } 
         else{ // no existe en firebase se crea un documento nuevo
           let documento = await collectionEnvio.add(itemLocal);
           itemLocal.id= documento.id;
           let ref = this.db.doc(`${tablaFirebase}/${documento.id}`);
           this.batch.update(ref, itemLocal);
           //se actualizado el dato localmente con el id nuevo de firebase
           let datorespuesta = await this._genericpouchdbProvider.putRecord(itemLocal);
         } 
       }
       return itemLocal;
     } catch (error) {
       alert(`metodo enviarDatosFirebase ${tablaFirebase} error: ${error}`);
       throw error;
     }
     finally{
       loading.dismiss();
     }

 }


  /** 
   * Metodo que permite obtener los datos de firebase a (pouchb)sqlite 
   */
  async obtenerDatosFormularisoResueltosClub(idClubFirestore:string) {

    this.idClubFirestore= idClubFirestore;

    try {
        
        //se consultas las preguntas resueltas en firebase (por idclub)
        let listPreguntasResueltasFirebase = await this._genericFirestoreProvider.cargarPreguntasResueltasPorClub(idClubFirestore);
      
        await this._genericpouchdbProvider.constructorToObjectDb(ModelNamesSqlLiteEnum.FORMULARIORESPUESTA.toString());
        //se crea el indice ['idclub']
        let indexformulariorespuestaidclub = await this._genericpouchdbProvider.createIndex('indexformulariorespuestaidclub',['idclub']);

         //elimino todos los  items locales
        let listPreguntasResueltasLocal = 
               await this._genericpouchdbProvider.findRecordPropertys({idclub: idClubFirestore},'indexformulariorespuestaidclub') as  PreguntaResuelta[];
        for (let itemLocal of listPreguntasResueltasLocal) {
            let eliminado = await this._genericpouchdbProvider.deleteRecord(itemLocal);
        }

        if(listPreguntasResueltasFirebase != null && listPreguntasResueltasFirebase.length > 0){
          for (let itemFirebase of listPreguntasResueltasFirebase) {
            itemFirebase._rev = null; //se limpia a el rev para poder crear el nuevo registro
            itemFirebase._id = this._util.armarIdentificadorPouchdb(itemFirebase.idclub,itemFirebase.idformulario,itemFirebase.idpregunta);
            let datorespuesta = await this._genericpouchdbProvider.putRecord(itemFirebase);
          }
        }

        1////////////////OBTENER DATOS CAMPO DE JUEGO MEDIDAS////////////////////////////////////////////
        let campoDeJuegoMedidas:CampoDeJuegoMedidas = await this.obtenerDatosFormularioMedicasCampoJuego(ModeloFirebase.CAMPODEJUEGOMEDIDA,ModelNamesSqlLiteEnum.CAMPODEJUEGOMEDIDA.toString())
        //console.log(JSON.stringify(campoDeJuegoMedidas));

        return true;
    } catch (error) {
      alert(`metodo obtenerDatosFirebaseFormulario error: ${error}`);
      throw error;
    } finally{
      
    } 
  } 

  async obtenerDatosFormularioMedicasCampoJuego(tablaFirebase:string,tablaPouchdb:string){

    try {
       await this._genericpouchdbProvider.constructorToObjectDb(tablaPouchdb);
       let itemFirebase = await this._genericFirestoreProvider.findRecordPropertyFirestore("idclub",this.idClubFirestore,tablaFirebase);
       if(itemFirebase != null){
         let itemLocal = await this._genericpouchdbProvider.getRecord(itemFirebase.idclub);
         if(itemLocal != null){
           itemFirebase._rev = itemLocal._rev; // se le pasa la rev para poder actualizar
           let datorespuesta = await this._genericpouchdbProvider.putRecord(itemFirebase);
         }
         else{ //no hay datos localmente
           itemFirebase._rev = null; //se limpia a el rev para poder crear el nuevo registro
           itemFirebase._id = itemFirebase.idclub;
           let datorespuesta = await this._genericpouchdbProvider.putRecord(itemFirebase);
         }
       }
       return itemFirebase;
      
    } catch (error) {
     alert(`metodo obtenerDatosFirebaseFormularios ${tablaFirebase} error: ${error}`);
     throw error;
    }
 }


  async obtenerDatosDelServidor(){ 
    try {
      if(this._util.isConect() == true){
        await this.obtenerDatosDivisiones();
        await this.obtenerDatosFormularios();
        await this.obtenerDatosClubes();
      }
      else{
        this._util.doAlert("Mensaje", 'Debe estar conectado a una red wifi para obtener los clubles').present();
      }
    } catch (error) {
      throw error;
    }
  }

  /**
   * elimina las divisiones existentes de pouchdb y las crea nuevamente 
   * apartir de los datos de firebase 
   */
  async obtenerDatosDivisiones(){

    let loaderDivisiones= this._util.presentarLoading("Obteniendo datos de divisiones del servidor...");
    await loaderDivisiones.present();
    try {

    await this._genericpouchdbProvider.constructorToObjectDb(ModelNamesSqlLiteEnum.DIVISION.toString());

    //elimino todos las diviciones
    let listaDivicionesPB :Division[] = await this._genericpouchdbProvider.findRecord() as Division[];
    for (let itemDivicion of listaDivicionesPB) {
        let eliminada = await this._genericpouchdbProvider.deleteRecord(itemDivicion);
    }
      
    //se consultas las divisiones de firebase para crealas localmente
    let listaDivisionesFirebase :Division[]  = await this._genericFirestoreProvider.findRecordFirestore(ModeloFirebase.DIVISION) as Division[];
    for (let divisionFirebase of listaDivisionesFirebase) {
      let datos = await this._genericpouchdbProvider.addRecord(divisionFirebase);
    }

    } catch (error) {
      throw error;
    }
    finally{
      loaderDivisiones.dismiss();
    }
  }


  /**
   * elimina los formularios existentes de pouchdb y las crea nuevamente 
   * apartir de los datos de firebase 
   */
  async obtenerDatosFormularios(){

    let loaderFormularios= this._util.presentarLoading("Obteniendo datos de formularios del servidor...");
    await loaderFormularios.present();
    try {

    await this._genericpouchdbProvider.constructorToObjectDb(ModelNamesSqlLiteEnum.FORMULARIO.toString());

    //elimino todos los formularios
    let listaFormulariosPB :Formulario[] = await this._genericpouchdbProvider.findRecord() as Formulario[];
    for (let itemFormulario of listaFormulariosPB) {
        let eliminado = await this._genericpouchdbProvider.deleteRecord(itemFormulario);
    }
      
    //se consultas los formularios de firebase para crealas localmente
    let listaFormulariosFirebase :Formulario[]  = await this._genericFirestoreProvider.findRecordFirestore(ModeloFirebase.FORMULARIO) as Formulario[];
    for (let formularioFirebase of listaFormulariosFirebase) {
      let datos = await this._genericpouchdbProvider.addRecord(formularioFirebase);
    }

    } catch (error) {
      throw error;
    }
    finally{
      loaderFormularios.dismiss();
    }
  }



   /**
   * metodo que obtiene los datos de los clubes desde firebase, 
   * elimina los cubles de pouchdb y los crea nuevamente 
   */
  async obtenerDatosClubes(){

    let loaderClubes= this._util.presentarLoading("Obteniendo datos de clubes del servidor...");
    await loaderClubes.present();

    try {
      await this._genericpouchdbProvider.constructorToObjectDb(ModelNamesSqlLiteEnum.CLUB.toString());
      let listaClubesFirebase :Club[]  = await this._genericFirestoreProvider.findRecordFirestore(ModeloFirebase.CLUB) as Club[];
      let listaClubesPB :Club[] = await this._genericpouchdbProvider.findRecord() as Club[];
      for (let clubFirebase of listaClubesFirebase) {
        if(listaClubesPB != null && listaClubesPB.length > 0 ){
          for (let clubPB of listaClubesPB) {
            if(clubFirebase.id == clubPB.id){
              let eliminado = await this._genericpouchdbProvider.deleteRecord(clubPB);
            } 
          }
        }
        
        //se limpia para que pueda crear el registro 
        clubFirebase._rev= null; 
        clubFirebase._id= null;  

        if(clubFirebase._attachments != null && 
          clubFirebase._attachments["att.txt"] != null && 
          clubFirebase._attachments["att.txt"].data != null){
          clubFirebase._attachments["att.txt"]['digest']=null;
          clubFirebase._attachments["att.txt"]['revpos']=null;
        }
        let datos = await this._genericpouchdbProvider.addRecord(clubFirebase);
      }
      loaderClubes.dismiss();
    } catch (error) {
      loaderClubes.dismiss();
      alert(`Ocurrio algo obteniendo los cubles  ${error}`);
      throw error;
    }
    
  }


  async enviarImagenesFirebase(fotos:object[]){
    let fotosModificadas:object[] = []; //fotos con las url generadas por firebase
    if(fotos != null && fotos.length > 0){
      for (let itemFoto of fotos) {
           let itemFotoPB = itemFoto as ImagePDBInterface; 
           if(itemFotoPB._attachments != null && itemFotoPB._attachments.imageProperties != null){

             //es una url que ya esta en firebase
             if(this._util.isValidUrl(itemFotoPB._attachments.imageProperties.data) === true){ 
              //alert("es una url que ya esta en firebase");
              continue;
             }
             let urlFile = await this._almacenamientoArchivosFirebaseProvider.guardarFotoFirebase(itemFotoPB._attachments.imageProperties.data,itemFotoPB._attachments.name.toString());
             itemFotoPB._attachments.imageProperties.data = urlFile; //se cambia la cadena en base64 por la url generada por firebase
             fotosModificadas.push(itemFotoPB);
           }
      }
    }
    return fotosModificadas;
  }


  /**
   * metodo que permite eliminar todos los datos locales del club(FORMULARIORESPUESTA)
   * @param idClubFirestore
   */
  async eliminarFormularisoResueltosClub(idClubFirestore:string){

    try{
        ///////eliminando datos locales de formularios resueltos
        await this._genericpouchdbProvider.constructorToObjectDb(ModelNamesSqlLiteEnum.FORMULARIORESPUESTA.toString());
        //se crea el indice ['idclub']
        let indexformulariorespuestaidclub = await this._genericpouchdbProvider.createIndex('indexformulariorespuestaidclub',['idclub']);

        //se consulta las respuestas
        let listPreguntasResueltasLocal = 
                await this._genericpouchdbProvider.findRecordPropertys({idclub: idClubFirestore},'indexformulariorespuestaidclub') as  PreguntaResuelta[];

        if(listPreguntasResueltasLocal != null && listPreguntasResueltasLocal.length >0){
          for(let itemLocal of listPreguntasResueltasLocal) {
              let eliminado = await this._genericpouchdbProvider.deleteRecord(itemLocal);
          }
        }        

        ///////eliminano datos de las medidas del campo de juego
        await this._genericpouchdbProvider.constructorToObjectDb(ModelNamesSqlLiteEnum.CAMPODEJUEGOMEDIDA.toString());
        let itemLocalMedidasCampo = await this._genericpouchdbProvider.getRecord(idClubFirestore);
        if(itemLocalMedidasCampo != null){
          let eliminado = await this._genericpouchdbProvider.deleteRecord(itemLocalMedidasCampo);
        }
        return true;
    } catch (error) {
      alert(`metodo eliminarFormularisoResueltosClub error: ${error}`);
      throw error;
    }
  }

}
