import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from 'angularfire2/firestore';
import { Division } from '../../modelos/Division';
import { UtilidadesProvider } from '../utilidades/utilidades';
import { AlmacenamientoArchivosFirebaseProvider } from '../almacenamiento-archivos-firebase/almacenamiento-archivos-firebase';
import { ModeloFirebase } from '../../modelos/ModeloFirebase';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DivisionProvider {

  collectionDivision: AngularFirestoreCollection<Division>;


  constructor(public _util: UtilidadesProvider,
              public _almacenamientoFirebaseProvider: AlmacenamientoArchivosFirebaseProvider,
              public afDB: AngularFirestore) {
  
    this.collectionDivision = this.afDB.collection(`/${ModeloFirebase.DIVISION}`);
  }

  obtenerRef(id) {
    return this.afDB.doc<Division>(`${ModeloFirebase.DIVISION}/${id}`);
  }

  consultarDivisiones(): Promise<Division[]> {
    return new Promise((resolve, reject) => {

      return this.afDB.collection(`${ModeloFirebase.DIVISION}`).snapshotChanges().map(actions => {
          return actions.map(a => {
            return { id: a.payload.doc.id, ...a.payload.doc.data() } as Division;
          });
        }).subscribe(datos => {
          resolve(datos)
        });
    });
  }

  async crearActualizarDivision(Division: Division,imagenFirebase:string, imagenFondoFirebase:string): Promise<Division> {
    if (Division.id == null) { //se guarda
      let documento = await this.collectionDivision.add(Division);
      Division.id = documento.id;
      await this.obtenerRef(Division.id).update(Division);
    }
    else { //se actualiza
      await this.obtenerRef(Division.id).update(Division);
    }

    //se estable el nombre de la imagen
    let nombreArchivo = Division.id;
   
    return Division;
  }

  consultarDivisionPorid(id: string): Observable<Division[]> {
    return this.afDB.collection(`${ModeloFirebase.DIVISION}`, ref => 
     ref.where('id', '==', id)).snapshotChanges().map(actions => {
      return actions.map(a => {
        return { id: a.payload.doc.id, ...a.payload.doc.data() } as Division;
      });
    });
  }

}
