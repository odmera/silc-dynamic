import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb';
import PouchdbFind from 'pouchdb-find';
import { Platform } from 'ionic-angular/platform/platform';

@Injectable()
export class GenericpouchdbProvider {

  private _DBOBJECT: any;
  private dbName:string;

  constructor(public platform: Platform) {
    
  }

  async constructorToObjectDb(objectDbName:string) {
    console.info(objectDbName);
    this.dbName = objectDbName;
    await this.initialise_DBObject();
  }

  private initialise_DBObject() {
    return new Promise((resolve, reject) => {
      try {
        this.platform.ready().then((readySource) => {
          if (this.platform.is('cordova')) {
            var PouchAdapterCordovaSqlite = require('pouchdb-adapter-cordova-sqlite');
            PouchDB.plugin(PouchAdapterCordovaSqlite);
            PouchDB.plugin(PouchdbFind);
            this._DBOBJECT = new PouchDB(this.dbName, {adapter: 'cordova-sqlite'});
            //alert('SQLite plugin is installed?: ' + (!!window['sqlitePlugin']))
            resolve(true);
          }
          else{
            PouchDB.plugin(PouchdbFind);
            this._DBOBJECT = new PouchDB(this.dbName,{size: 1000});
            resolve(true);
          }
        });
      } catch (error) {
        console.error("No se logro obtener la base de dattos " + this.dbName);
        alert("No se logro obtener la base de datos " + this.dbName);
        reject(error);
      }
    });
  }

  addRecord(item) {
    return new Promise((resolve, reject) => {
      try {
        let guardo = this._DBOBJECT.post(item);
        resolve(guardo);
      } catch (error) {
        reject(error);
      }
    });
  }

  putRecord(item) {
    return new Promise(resolve => {
      let guardo = this._DBOBJECT.put(item);
      resolve(guardo);
    });
  }

  deleteRecord(item) {
    return new Promise(resolve => {
      let elimino = this._DBOBJECT.remove(item);
      resolve(elimino);
    });
  }

  async getRecord(id) {
    try {
      //attachments --> para poder traer las imagenes
      let dato = await this._DBOBJECT.get(id,{attachments: true}); 
      return dato
    } catch (error) {
      console.log("No existe el dato ", error)
      return null;
    }
  }

  findRecord(): Promise<any> {
    return new Promise(resolve => {
      this._DBOBJECT.allDocs({ include_docs: true, descending: true, attachments: true },
        function (err, doc) {
          let listObjets:any[] = [];

          if (err) {
            console.error("error", err);
            resolve(null);
          }
          else {
            let row = doc.rows;
            for (let k in row) {
              let item = row[k].doc as any;
              listObjets.push(item);
            }
            resolve(listObjets);
          }
        });
    });
  }

  deleteDatabase(objectDbName:string) {
    return new Promise((resolve, reject) => {
      try {
        new PouchDB(objectDbName).destroy().then(function () {
          resolve(true);
        }).catch(function (err) {
          alert("Ocurrio algo eliminando la bd de " + objectDbName + "  " +  JSON.stringify(err))
          reject(false);
        })
      } catch (error) {
        reject(error);
      }
    });
  }

  putRecordMultiple(items) {
    return new Promise((resolve, reject) => {
      try {
        let guardo = this._DBOBJECT.bulkDocs(items);
        resolve(guardo);
      } catch (error) {
        reject(error);
      }
    });
  }

  
  createIndex(name:string,fields:string[]){
    return new Promise((resolve, reject) => {
      try {
        this._DBOBJECT.createIndex({
          index: {
            fields: fields,
            name: name,
            ddoc: 'design'+name,
            type: 'json',//solo admite 'json', que también es el valor predeterminado
          }
        }).then(function (result) {
          resolve(result);
        }).catch(function (err) {
          console.log("createIndex error " , err);
          reject(err);
        });
      } catch (error) {
        console.log("createIndex error " , error);
        reject(error);
      }
    });
  }

  findRecordPropertys(selector:object,use_index:string): Promise<any[]> {

    return new Promise((resolve, reject) => {
      try {
        this._DBOBJECT.find({
          selector: selector,
          use_index:'design'+use_index
        }).then(function (result) {
          if(result != null){
            resolve(result['docs']);
          }
          else{
            resolve(null);
          }
          
        }).catch(function (err) {
          console.log("findRecordPropertys error " , err);
          reject(err);
        });
      } catch (error) {
        console.log("findRecordPropertys error " , error);
        reject(error);
      }
    });

  }


}
