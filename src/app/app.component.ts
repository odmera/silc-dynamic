import { Component, ViewChild } from "@angular/core";
import { Platform, Nav, MenuController, AlertController, Events } from "ionic-angular";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Keyboard } from '@ionic-native/keyboard';

import { AuntenticacionFirebaseProvider } from "../providers/auntenticacion-firebase/auntenticacion-firebase";
import { AlmacenamientoLocalProvider } from "../providers/almacenamiento-local/almacenamiento-local";
import { UtilidadesProvider } from "../providers/utilidades/utilidades";
import { UsuarioProvider } from "../providers/usuario/usuario";
import { datosAL } from "../modelos/datosAL";
import { PerfilUsuario } from "../modelos/UsuarioDTO";
import { FirstRunPage } from '../modelos/constants/constants';
import { SincronizarDatosProvider } from "../providers/sincronizar-datos/sincronizar-datos";

export interface MenuItem {
    title: string;
    component: any;
    icon: string;
    codigomenu?:string
}

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;

  CONFFORMULARIOS = '01'
  CONFDIVICIONES = '02'
  CONFCLUBES = '03'
  CONFTODO = '04'

  paginasAdmin: MenuItem[] = [
    { title: 'Usuarios', component: 'ListUsuariosPage', icon: 'people' },
    { title: 'Divisiones', component: 'ListDivisionPage', icon: 'md-browsers' },
    { title: 'Clubes', component: 'ListClubesPage', icon: 'list-box' },
    { title: 'Configuración Formularios', component: 'ListFormularioPage', icon: 'paper' },
    { title: 'Ver divisiones', component: 'TipoclubPage', icon: 'ios-football' },
  ]

  paginasUsuario: MenuItem[] = [
    { title: 'Ver Divisiones', component: 'TipoclubPage', icon: 'ios-football' },
    { title: 'Actualizar configuración de formularios', component: "", codigomenu:this.CONFFORMULARIOS, icon: 'cloud-download'},
    { title: 'Actualizar divisiones', component: "",codigomenu:this.CONFDIVICIONES, icon: 'cloud-download'},
    { title: 'Actualizar clubes', component: "", codigomenu:this.CONFCLUBES,icon: 'cloud-download'},
    { title: 'Actualizar todo', component: "", codigomenu:this.CONFTODO,icon: 'cloud-download'},

    //{ title: 'Actualizar configuración de formularios,divisiones y clubes', component: "obtenerDatos", icon: 'cloud-download'},
  ]

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public keyboard: Keyboard,
    public _auntenticacionFirebaseProvider: AuntenticacionFirebaseProvider,
    public _almacenamientoLocalProvider: AlmacenamientoLocalProvider,
    public _usuarioProvider: UsuarioProvider,
    public _util: UtilidadesProvider,
    public menu: MenuController,
    private alertCtrl: AlertController,
    private _sincronizarDatosProvider: SincronizarDatosProvider,
    public events: Events){
    
      this.initializeApp();
   }

  async initializeApp() {

    let datosAl: datosAL = await this._almacenamientoLocalProvider.obtenerDatosSesion();

    if (datosAl && datosAl.idUsuario) {
      this._almacenamientoLocalProvider.correoUsuario = datosAl.correoUsuario;

      //verficio el perfil de usuario
      if(datosAl.perfilUsuario) {
        this._almacenamientoLocalProvider.perfilUsuario = datosAl.perfilUsuario;
      }

      if(datosAl.perfilUsuario == PerfilUsuario._pefil_usuario.toString()){
        this.rootPage = 'TipoclubPage';
      }
      else if (datosAl.perfilUsuario == PerfilUsuario._pefil_admin.toString()) {
        this.rootPage = 'TipoclubPage';
        //this.rootPage = 'CampodejuegomedidasPage';
      }
    
    } else {
      this.rootPage = FirstRunPage;
    }

    this.platformReady()

  }


  platformReady(): void {
    // Call any initial plugins when ready
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.statusBar.overlaysWebView(false);
      this.splashScreen.hide();
      this.keyboard.disableScroll(true);
    });
  }

  openPage(page) {
    if(page.codigomenu === this.CONFFORMULARIOS || page.codigomenu === this.CONFDIVICIONES || page.codigomenu === this.CONFCLUBES || page.codigomenu === this.CONFTODO){
      this.presentConfirm(page.codigomenu);
    }
    else{
      this.nav.setRoot(page.component);
    }  
  }

  async logout() {
    this._almacenamientoLocalProvider.limpiarTodoAL();
    let loader = this._util.presentarLoading("Saliendo...");
    await loader.present()
    await this._auntenticacionFirebaseProvider.cerrarSesion();
    await this.nav.setRoot('LoginPage');
    loader.dismiss();
  }  

  presentConfirm(codigomenu:string) {
    let alert = this.alertCtrl.create({
      title: 'Esta seguro de realizar esta accion.',
      message: 'Presione OK para realizar la operación',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Ok',
          handler: () => {
            try {
              if(this._util.isConect() == true){
                this.actualizarDatos(codigomenu);
              }
              else{
                this._util.doAlert("Mensaje", 'Debe estar conectado a una red wifi para obtener los clubles').present();
              } 
            } catch (error) {
              this._util.doAlert('','No se obtuvieron los datos del servidor' + error).present();;
            }
          }
        }
      ]
    });
    alert.present();
  }

  async actualizarDatos(codigomenu:string){
      if(codigomenu === this.CONFTODO){ //actualizar todo
        await this._sincronizarDatosProvider.obtenerDatosDelServidor();
      }
      if(codigomenu === this.CONFDIVICIONES){ //actualizar diviciones
        await this._sincronizarDatosProvider.obtenerDatosDivisiones();
      }
      if(codigomenu === this.CONFCLUBES){ //actualizar clubes y diviciones (se actulizan tambien las diviciones ya que los clubles dependen de ellas)
        await this._sincronizarDatosProvider.obtenerDatosClubes();
        await this._sincronizarDatosProvider.obtenerDatosDivisiones();
      }
      if(codigomenu === this.CONFFORMULARIOS){
        await this._sincronizarDatosProvider.obtenerDatosFormularios();
      }
      //se emite el evento para que se realice la consulta justo despues de tener las diviciones listas en PB
      this.events.publish('division:consulta',true);
  }

}
