import {NgModule} from "@angular/core";
import {IonicApp, IonicModule} from "ionic-angular";
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {IonicStorageModule} from '@ionic/storage';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Keyboard} from '@ionic-native/keyboard';

import {ActivityService} from "../services/activity-service";
import {TripService} from "../services/trip-service";
import {WeatherProvider} from "../services/weather";

import {MyApp} from "./app.component";
import {HomePage} from "../pages/home/home";


// Firebase
import { firebaseConfig } from "../configuracion/firebase.config";
import { AngularFireModule } from "angularfire2";
import { AngularFirestoreModule } from "angularfire2/firestore";
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';

///
import { UtilidadesProvider } from '../providers/utilidades/utilidades';
import { ClubProvider } from '../providers/club/club';
import { AlmacenamientoArchivosFirebaseProvider } from '../providers/almacenamiento-archivos-firebase/almacenamiento-archivos-firebase';
import { AuntenticacionFirebaseProvider } from '../providers/auntenticacion-firebase/auntenticacion-firebase';
import { UsuarioProvider } from '../providers/usuario/usuario';
import { AlmacenamientoLocalProvider } from '../providers/almacenamiento-local/almacenamiento-local';
import { GenericpouchdbProvider } from '../providers/genericpouchdb/genericpouchdb';
import { GenericFirestoreProvider } from "../providers/genericfirestore/genericfirestore";
import { Network } from "@ionic-native/network";
import { SincronizarDatosProvider } from '../providers/sincronizar-datos/sincronizar-datos';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { Camera } from '@ionic-native/camera';
import { SignaturePadModule } from "angular2-signaturepad";
import { ApiProvider } from "../providers/api/api";
import { FormdynamicProvider } from "../providers/formdynamic/formdynamic";
import { DivisionProvider } from "../providers/division/division";

import { Ng2ImgMaxModule } from 'ng2-img-max';

@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp, {
      scrollPadding: false,
      scrollAssist: true,
      autoFocusAssist: false,
      backButtonText: 'Atrás',
      monthNames: ['Enero','Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
      monthShortNames: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
      dayNames: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
      dayShortNames: ['Dom','Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
    }),
    IonicStorageModule.forRoot({
      name: '__ionic3_start_theme',
        driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule,
    AngularFireAuthModule,
    IonicImageViewerModule,
    Ng2ImgMaxModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Keyboard,
    ActivityService,
    TripService,
    WeatherProvider,
    AngularFireDatabase,
    UtilidadesProvider,
    ClubProvider,
    AlmacenamientoArchivosFirebaseProvider,
    AuntenticacionFirebaseProvider,
    UsuarioProvider,
    AlmacenamientoLocalProvider,
    Network,
    SincronizarDatosProvider,
    GenericpouchdbProvider,
    GenericFirestoreProvider,
    Camera,
    ApiProvider,
    FormdynamicProvider,
    DivisionProvider,
  ]
})

export class AppModule {
}
