import { NgModule } from '@angular/core';
import { FotoformularioComponent } from './fotoformulario/fotoformulario';
import { IonicModule } from 'ionic-angular';
import { ResourceTree } from './tree/ResourceTree';

@NgModule({
	declarations: [FotoformularioComponent,ResourceTree],
	imports: [IonicModule],
	exports: [FotoformularioComponent,ResourceTree]
})
export class ComponentsModule {}

