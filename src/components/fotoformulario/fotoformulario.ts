import { Component, ViewChild, EventEmitter, Output, Input } from '@angular/core';
import { UtilidadesProvider } from '../../providers/utilidades/utilidades';
import { Platform, NavController, ModalController, ActionSheetController } from 'ionic-angular';
import { Foto } from '../../modelos/Foto';
import { CameraOptions, Camera } from '@ionic-native/camera';


@Component({
  selector: 'fotoformulario',
  templateUrl: 'fotoformulario.html'
})
export class FotoformularioComponent {


  //fileInput para la web
  @ViewChild('fileInput') fileInput;

  @Output("obtenerImagenBase64") 
  obtenerImagenBase64: EventEmitter<string> = new EventEmitter<string>();
  
  @Input() 
  imagenBase64: string=null;

  @Input()
  listImagen: Array<Foto> = [];

  @Input()
  nombrePregunta: string='Tomar foto';

  constructor(public _util: UtilidadesProvider,
              public platform :Platform,
              public navCtrl: NavController,
              public modalCtrl: ModalController,
              private camera: Camera,
              public actionSheetCtrl: ActionSheetController) {

  }


  presentActionSheet() {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Operación a realizar',
      buttons: [
        {
          text: 'Galería',
          handler: () => {
            this.tomarFotoDesdeDispositvo(true);
          }
        },{
          text: 'Cámara',
          handler: () => {
            this.tomarFotoDesdeDispositvo(false);
          }
        },{
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  gestionarFoto() {
    if(this.platform.is('cordova')) {
       this.presentActionSheet();
    }
    else{
      this.fileInput.nativeElement.click();
    }
  }

  verFotos(){
    let modal = this.modalCtrl.create('GaleriafotosPage',
    { 'imagenes': this.listImagen,
      'nombrePregunta': this.nombrePregunta });

    modal.present();

    //esta funcion se ejecuta cuando cierro el modal
    modal.onDidDismiss(parametros => {
      if (parametros != null) {
        console.log("item eliminado ", JSON.stringify(parametros));
      }
    })
  }

  async processWebImage(event) {
    if(event.target.files != null && event.target.files[0] != null){
      let reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (readerEvent) => {
        let loader = this._util.presentarLoading('');
        loader.present().then(() => {
          this.imagenBase64 = reader.result.split(',')[1]; //quito la cadena data:image/jpg;base64,

          let base64data = 'data:image/jpeg;base64,' + this.imagenBase64;
          let bigSize = this._util.getImageSize(base64data);
          this._util.doAlert("Detalle de imagen ","tamaño imagen " + bigSize + "MB").present();

          this.obtenerImagenBase64.emit(this.imagenBase64);
          loader.dismiss();
        });
      };
    }
  }

  tomarFotoDesdeDispositvo(useAlbum:boolean){
    let options: CameraOptions = {
      sourceType: useAlbum?  this.camera.PictureSourceType.SAVEDPHOTOALBUM : this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 400,
      targetHeight: 400,
      quality: 60,
      allowEdit:false
    }
     if (Camera['installed']()) {
      let loadercamara = this._util.presentarLoading('');
      loadercamara.present().then(() => {
        this.camera.getPicture(options).then((data) => {
          this.imagenBase64 = data;

          let base64data = 'data:image/jpeg;base64,' + data;
          let bigSize = this._util.getImageSize(base64data);
          this._util.doAlert("Detalle de imagen ","tamaño imagen " + bigSize + "MB").present();
          
          this.obtenerImagenBase64.emit(this.imagenBase64);
          loadercamara.dismiss();
        }).catch((err) => {
          loadercamara.dismiss();
          alert(err)
          console.log("Error ", err);
        });
      });
    }
  }



}
